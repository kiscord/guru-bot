ktor {
    deployment {
        port = 8080
        port = ${?BOT_PORT}
        watch = [ classes, resources ]
        shutdown {
            url = "/ktor/application/shutdown"
        }
    }
    application {
        modules = [kitty.backend.KittyKt.kitty]
    }
}

kitty {
    clientId = ${?BOT_CLIENT_ID}
    clientSecret = ${?BOT_CLIENT_SECRET}
    token = ${?BOT_TOKEN}
    mode = "server"
    mode = ${?BOT_MODE}
    hmacSecret = "change-me-please"
    hmacSecret = ${?BOT_HMAC_SECRET}
    publicPath = "http://localhost:8080"
    publicPath = ${?BOT_PUBLIC_PATH}

    database {
        url = ${?BOT_DATABASE_URL}
        username = ${?BOT_DATABASE_USERNAME}
        password = ${?BOT_DATABASE_PASSWORD}
        baselineVersion = ${?BOT_DATABASE_BASELINE_VERSION}
    }

    kubernetes {
        enabled = false
        enabled = ${?BOT_KUBERNETES_ENABLED}
        kubeConfig = ${?BOT_KUBERNETES_KUBECONFIG}
        namespace = ${?BOT_KUBERNETES_NAMESPACE}
        leaseName = ${?BOT_KUBERNETES_LEASE_NAME}
        identity = ${?BOT_KUBERNETES_IDENTITY}
        leaseDuration = ${?BOT_KUBERNETES_LEASE_DURATION}
        renewDeadline = ${?BOT_KUBERNETES_RENEW_DEADLINE}
        retryPeriod = ${?BOT_KUBERNETES_RETRY_PERIOD}
    }
}

jwt {
    privateKey = """
    -----BEGIN EC PRIVATE KEY-----
    MHcCAQEEIImtNkqTKipyme3NTrp601SEWA24IEUlux1/Gk3w/gkdoAoGCCqGSM49
    AwEHoUQDQgAECaQ0aalKJskIkLcnIjmO0vmVuzIb7gTbTwze9woSechW0b+TYj4w
    ggG5q2zz4rSxTTLA+5/yy+5K4mKps+vz6w==
    -----END EC PRIVATE KEY-----
    """
    privateKey = ${?BOT_JWT_PRIVATE_KEY}
    issuer = "http://localhost:8080"
    issuer = ${?BOT_JWT_ISSUER}
    audience = "http://localhost:8080/"
    audience = ${?BOT_JWT_AUDIENCE}
    realm = "Kitty"
    realm = ${?BOT_JWT_REALM}

    signWith = "db41a32d-d23e-4e8a-8d87-a628fd35c7ec"
    signWith = ${?BOT_JWT_SIGN_WITH}

    publicKeys = [
        {
            key = """
            -----BEGIN PUBLIC KEY-----
            MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAECaQ0aalKJskIkLcnIjmO0vmVuzIb
            7gTbTwze9woSechW0b+TYj4wggG5q2zz4rSxTTLA+5/yy+5K4mKps+vz6w==
            -----END PUBLIC KEY-----
            """
            kid = "db41a32d-d23e-4e8a-8d87-a628fd35c7ec"
            use = "sig"
            alg = "ES256"
            key_ops = ["verify"]
        }, {
            key = """
            -----BEGIN PUBLIC KEY-----
            MIGbMBAGByqGSM49AgEGBSuBBAAjA4GGAAQBl+oS1UltO9wbXxc+fou7Pk4psS1C
            jbYKU2no4RI3XMQZfEyfe+aXktC/5FdRNRIkRyPqobyRqYRz1oVNrDGXZF0BC/8N
            I48wLWfzhPGBFOGmCUA5xBsV7MLzwjn+flrCWtu476clIge4c9JSAly8YBVetXYP
            byXpkF8g9fgE1TMvM8U=
            -----END PUBLIC KEY-----
            """
            kid = "888d7521-2724-4495-928c-ecffc41363cd"
            use = "sig"
            alg = "ES512"
            key_ops = ["verify"]
        }
    ]
}
