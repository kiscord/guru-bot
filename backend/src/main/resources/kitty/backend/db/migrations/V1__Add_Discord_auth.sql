CREATE TABLE DISCORDUSERS
(
    ID            ${datatype:ulong}                                          NOT NULL PRIMARY KEY,
    USERNAME      VARCHAR(32)                                                NOT NULL,
    DISCRIMINATOR VARCHAR(4)                                                 NOT NULL,
    AVATAR        VARCHAR(32),
    MFA_ENABLED   ${datatype:boolean},
    BANNER        VARCHAR(32),
    ACCENT_COLOR  ${datatype:integer},
    LOCALE        VARCHAR(12),
    VERIFIED      ${datatype:boolean},
    EMAIL         VARCHAR(36),
    FLAGS         ${datatype:integer},
    PUBLIC_FLAGS  ${datatype:integer},
    UPDATED_AT    ${datatype:dateTime} DEFAULT ${datatype:current_timestamp} NOT NULL,
    BOT           ${datatype:boolean}
);

CREATE TABLE DISCORDGUILDS
(
    ID      ${datatype:ulong}                             NOT NULL PRIMARY KEY,
    NAME    VARCHAR(100)                                  NOT NULL,
    ICON    VARCHAR(32),
    MANAGED ${datatype:boolean} DEFAULT ${datatype:false} NOT NULL
);

CREATE TABLE DISCORDGUILDMEMBERS
(
    ${q}USER${q} ${datatype:ulong}   NOT NULL,
    GUILD        ${datatype:ulong}   NOT NULL,
    OWNER        ${datatype:boolean} NOT NULL,
    PERMISSIONS  ${datatype:ulong}   NOT NULL,
    CONSTRAINT DISCORDGUILDMEMBERS_USER_GUILD_UNIQUE UNIQUE (${q}USER${q}, GUILD),
    CONSTRAINT FK_DISCORDGUILDMEMBERS_GUILD__ID
        FOREIGN KEY (GUILD) REFERENCES DISCORDGUILDS (ID)
            ON DELETE CASCADE,
    CONSTRAINT FK_DISCORDGUILDMEMBERS_USER__ID
        FOREIGN KEY (${q}USER${q}) REFERENCES DISCORDUSERS (ID)
            ON DELETE CASCADE
);
