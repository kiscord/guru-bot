CREATE TABLE PASTAFILES
(
    ID            CHAR(16)                                                   NOT NULL PRIMARY KEY,
    ${q}DATA${q}  ${datatype:blob}                                           NOT NULL,
    SHA3_256_HASH ${datatype:binary(32)}                                     NOT NULL,
    AUTHOR        ${datatype:ulong} NULL,
    CREATED_AT    ${datatype:dateTime} DEFAULT ${datatype:current_timestamp} NOT NULL,

    CONSTRAINT FK_PASTAFILES_AUTHOR__ID
        FOREIGN KEY (AUTHOR) REFERENCES DISCORDUSERS (ID)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE INDEX PASTAFILES_SHA3_256_HASH ON PASTAFILES (SHA3_256_HASH);
