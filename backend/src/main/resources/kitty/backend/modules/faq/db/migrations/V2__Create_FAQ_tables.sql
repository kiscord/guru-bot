CREATE TABLE FAQBOOKS
(
    ID        ${datatype:longAutoinc} ${PK_FOR_AUTOINC},
    NAME      VARCHAR(256)                                               NOT NULL,
    GUILD_ID  ${datatype:ulong}                                          NOT NULL,
    AUTHOR    ${datatype:ulong}                                          NOT NULL,
    CREATEDAT ${datatype:dateTime} DEFAULT ${datatype:current_timestamp} NOT NULL,
    CONSTRAINT FAQBOOKS_NAME_GUILD_ID_UNIQUE
        UNIQUE (NAME, GUILD_ID)
);

CREATE INDEX FAQBOOKS_NAME ON FAQBOOKS (NAME);

CREATE TABLE FAQPAGES
(
    ID         ${datatype:longAutoinc} ${PK_FOR_AUTOINC},
    BOOK       ${datatype:long}            NOT NULL,
    EMBED_DATA ${datatype:text}            NOT NULL,
    TEXT       ${datatype:text} DEFAULT '' NOT NULL,
    CONSTRAINT FK_FAQPAGES_BOOK__ID
        FOREIGN KEY (BOOK) REFERENCES FAQBOOKS (ID)
            ON DELETE CASCADE
);

CREATE INDEX FAQPAGES_BOOK ON FAQPAGES (BOOK);

CREATE TABLE FAQPAGEALIASES
(
    ID            ${datatype:longAutoinc} ${PK_FOR_AUTOINC},
    PAGE          ${datatype:long}    NOT NULL,
    ALIAS         VARCHAR(128)        NOT NULL,
    ${q}ORDER${q} ${datatype:integer} NOT NULL,
    CONSTRAINT FAQPAGEALIASES_PAGE_ALIAS_UNIQUE
        UNIQUE (PAGE, ALIAS),
    CONSTRAINT FK_FAQPAGEALIASES_PAGE__ID
        FOREIGN KEY (PAGE) REFERENCES FAQPAGES (ID)
            ON UPDATE CASCADE ON DELETE CASCADE
);
