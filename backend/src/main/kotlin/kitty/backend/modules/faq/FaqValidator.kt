package kitty.backend.modules.faq

import kitty.backend.*
import kitty.modules.faq.*
import io.ktor.server.plugins.requestvalidation.*
import kiscord.api.*
import org.kodein.di.*

class FaqValidator(di: DI) : AbstractValidationController(di) {
    override fun RequestValidationConfig.install() {
        validateEntity<FaqPageDTO> { page ->
            if (page.aliases.isEmpty()) invalid("faq_page_empty_aliases", "Page aliases cannot be empty")
            if (page.aliases.distinct() != page.aliases) invalid(
                "faq_page_aliases_not_unique",
                "Page aliases should contain only unique values"
            )
            try {
                page.embed.validate()
            } catch (e: EmbedValidationException) {
                invalid("faq_page_embed_invalid", "Embed invalid: ${e.message}")
            }
            if (page.embed.type != null && page.embed.type != "rich") invalid("faq_page_embed_invalid", "Embed has invalid type")
        }
    }
}