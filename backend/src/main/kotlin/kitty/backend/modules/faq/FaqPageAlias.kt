package kitty.backend.modules.faq

import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.dao.id.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.inList

object FaqPageAliases : LongIdTable() {
    val page = reference("page", FaqPages, onDelete = ReferenceOption.CASCADE, onUpdate = ReferenceOption.CASCADE)
    val alias = varchar("alias", 128)
    val order = integer("order")

    init {
        uniqueIndex(page, alias)
    }
}

class FaqPageAlias(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<FaqPageAlias>(FaqPageAliases) {
        fun deleteAliases(entry: FaqPage, aliases: Iterable<String>? = null): Int {
            return FaqPageAliases.deleteWhere {
                buildList {
                    this += page eq entry.id
                    if (aliases != null) {
                        this += alias inList aliases
                    }
                }.compoundAnd()
            }
        }
    }

    var pageId by FaqPageAliases.page
    var page by FaqPage referencedOn FaqPageAliases.page
    var alias by FaqPageAliases.alias
    var order by FaqPageAliases.order
}