package kitty.backend.modules.faq

import kitty.backend.db.*
import kitty.modules.faq.*
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.dao.id.*
import org.jetbrains.exposed.sql.*

object FaqPages : LongIdTable() {
    val book = reference("book", FaqBooks, onDelete = ReferenceOption.CASCADE).index()
    val text = text("text").default("")
    val embedData = text("embed_data")
}

class FaqPage(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<FaqPage>(FaqPages) {
        fun syncAliases(page: FaqPage, aliases: Collection<String>) {
            FaqPageAlias.deleteAliases(page)
            FaqPageAliases.batchInsert(aliases) { alias ->
                this[FaqPageAliases.page] = page.id
                this[FaqPageAliases.alias] = alias
                this[FaqPageAliases.order] = aliases.indexOf(alias)
            }
        }
    }

    var bookId by FaqPages.book
    var book by FaqBook referencedOn FaqPages.book
    var text by FaqPages.text
    var embed by FaqPages.embedData.transform(EmbedTransformer)

    val aliases by FaqPageAlias referrersOn FaqPageAliases.page
}

val FaqPage.mainAlias: FaqPageAlias?
    get() = aliases.orderBy(FaqPageAliases.order to SortOrder.ASC).limit(1).firstOrNull()

fun FaqPage.asDTO(withId: Boolean = true) = FaqPageDTO(
    id = id.value.takeIf { withId },
    text = text,
    embed = embed,
    aliases = aliases.map { it.alias },
)