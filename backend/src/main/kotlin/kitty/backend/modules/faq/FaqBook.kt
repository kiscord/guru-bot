package kitty.backend.modules.faq

import kiscord.api.*
import kitty.backend.db.*
import kitty.modules.faq.*
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.dao.id.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.kotlin.datetime.*

object FaqBooks : LongIdTable() {
    val name = varchar("name", 256)
    val guildId = snowflake("guild_id")
    val author = snowflake("author")
    val createdAt = timestamp("created_at").defaultExpression(CurrentTimestamp())

    init {
        uniqueIndex(name, guildId)
        index(isUnique = false, name)
    }
}

class FaqBook(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<FaqBook>(FaqBooks) {
        operator fun get(
            bookId: Long,
            guildId: Snowflake,
            author: Snowflake? = null
        ): FaqBook? = find {
            buildList {
                this += FaqBooks.id eq bookId
                this += FaqBooks.guildId eq guildId
                if (author != null) {
                    this += FaqBooks.author eq author
                }
            }.compoundAnd()
        }.firstOrNull()

        fun findByGuildId(guildId: Snowflake): SizedIterable<FaqBook> = find {
            FaqBooks.guildId eq guildId
        }
    }

    var name by FaqBooks.name
    var guildId by FaqBooks.guildId
    var author by FaqBooks.author
    val pages by FaqPage referrersOn FaqPages.book
    var createdAt by FaqBooks.createdAt
}

fun FaqBook.asDTO(withId: Boolean = true): FaqBookDTO {
    val pagesCache = FaqPage.find { FaqPages.book eq id }

    return FaqBookDTO(
        id = id.value.takeIf { withId },
        name = name,
        guildId = guildId,
        author = author,
        pages = pagesCache.map { it.asDTO(withId = withId) },
        createdAt = createdAt.takeIf { withId },
    )
}
