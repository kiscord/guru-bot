package kitty.backend.modules.permissions

import kitty.modules.permissions.*
import org.kodein.di.*
import org.kodein.di.ktor.controller.*

val PermissionsModule = DI.Module(PermissionsModuleName.name) {
    inBindSet<DIController>(tag = "global-routes") { add { singleton { PermissionsRoutes(instance()) } } }
}
