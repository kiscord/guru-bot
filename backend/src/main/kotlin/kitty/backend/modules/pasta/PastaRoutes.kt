package kitty.backend.modules.pasta

import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.Route
import io.ktor.util.*
import io.ktor.utils.io.*
import io.ktor.utils.io.core.*
import io.ktor.utils.io.jvm.javaio.*
import kitty.backend.auth.*
import kitty.backend.db.*
import kitty.backend.routes.*
import kitty.modules.pasta.*
import kotlinx.coroutines.*
import org.bouncycastle.crypto.digests.*
import org.jetbrains.exposed.sql.statements.api.*

class PastaRoutes(application: Application) : AbstractKittyController(application) {
    override fun Route.getRoutes() {
        authenticate("auth-kitty", optional = true) {
            post<PastaResource.Upload> {
                val principal = call.principal<KittyPrincipal>()

                val multipart = call.receiveMultipart()
                var contents: ByteArray? = null

                while (true) {
                    val part = multipart.readPart() ?: break

                    when (part.name) {
                        "contents" -> when (part) {
                            is PartData.FormItem -> {
                                contents = part.value.encodeToByteArray()
                            }

                            is PartData.FileItem -> {
                                contents = part.provider().readBytes()
                            }

                            else -> {
                                return@post call.respond(HttpStatusCode.NotAcceptable)
                            }
                        }

                    }

                    part.dispose()
                }

                if (contents == null) {
                    return@post call.respond(HttpStatusCode.BadRequest)
                }

                val digest = SHA3Digest(256).run {
                    update(contents, 0, contents.size)
                    val result = ByteArray(digestSize)
                    doFinal(result, 0)
                    result
                }

                val gzipped = async(Dispatchers.IO, start = CoroutineStart.LAZY) {
                    with(GZip) {
                        encode(ByteReadChannel(contents)).toByteArray()
                    }
                }

                val pastaFileDTO = withTransaction {
                    val pastaFile = PastaFile.find { PastaFiles.sha3_256_hash eq digest }.singleOrNull()
                    if (pastaFile != null) return@withTransaction pastaFile.asDTO()
                    val dataBlob = ExposedBlob(gzipped.await().inputStream())
                    PastaFile.new {
                        data = dataBlob
                        sha3_256_hash = digest
                        if (principal != null) {
                            author = DiscordUser[principal.discordUserId]
                        }
                    }.asDTO()
                }

                call.respond(HttpStatusCode.Created, pastaFileDTO)
            }
        }

        get<PastaResource.Id.Raw> { resource ->
            val pastaFile = withTransaction {
                PastaFile.findById(resource.id)
            } ?: return@get call.respond(HttpStatusCode.NotFound)

            val gzippedContent = async(Dispatchers.IO) {
                pastaFile.data.inputStream.toByteReadChannel()
            }

            val respondGzipped = call.request.acceptEncodingItems().any { it.value == "gzip" }

            val content = if (respondGzipped) {
                GzipPreCompressedContent(gzippedContent.await())
            } else {
                GzipDecompressedContent(gzippedContent.await(), this)
            }

            call.response.header(HttpHeaders.ContentType, ContentType.Text.Plain.toString())
            call.respond(content)
        }

        get<PastaResource.Id> { resource ->
            val pastaFile = withTransaction {
                PastaFile.findById(resource.id)?.asDTO()
            } ?: return@get call.respond(HttpStatusCode.NotFound)

            call.respond(pastaFile)
        }
    }
}

private class GzipPreCompressedContent(
    val data: ByteReadChannel
) : OutgoingContent.ReadChannelContent() {
    override val headers: Headers by lazy(LazyThreadSafetyMode.NONE) {
        Headers.build {
            append(HttpHeaders.ContentEncoding, "gzip")
        }
    }

    override fun readFrom(): ByteReadChannel = data
}

private class GzipDecompressedContent(
    val data: ByteReadChannel,
    val scope: CoroutineScope,
) : OutgoingContent.ReadChannelContent() {
    override fun readFrom(): ByteReadChannel {
        return with(scope) {
            with(GZip) {
                decode(data)
            }
        }
    }
}
