package kitty.backend.modules.pasta

import kitty.*
import kitty.backend.db.*
import kitty.modules.pasta.*
import okio.ByteString.Companion.toByteString
import org.jetbrains.exposed.dao.id.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.kotlin.datetime.*

object PastaFiles : HashIdTable() {
    val data = blob("data")
    val sha3_256_hash = binary("sha3_256_hash", 32).index()
    val author = reference(
        "author", DiscordUsers,
        onDelete = ReferenceOption.CASCADE,
        onUpdate = ReferenceOption.CASCADE
    ).nullable()
    val createdAt = timestamp("created_at").defaultExpression(CurrentTimestamp())
}

class PastaFile(id: EntityID<HashId>) : HashIdEntity(id) {
    var data by PastaFiles.data
    var sha3_256_hash by PastaFiles.sha3_256_hash
    var author by DiscordUser optionalReferencedOn PastaFiles.author
    var createdAt by PastaFiles.createdAt

    companion object : HashIdEntityClass<PastaFile>(PastaFiles)
}

fun PastaFile.asDTO(): PastaFileDTO = PastaFileDTO(
    id = id.value,
    sha3_256_hash = sha3_256_hash.toByteString().hex(),
    author = author?.asProfile()
)
