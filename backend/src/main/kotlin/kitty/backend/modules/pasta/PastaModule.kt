package kitty.backend.modules.pasta

import kitty.modules.pasta.*
import org.flywaydb.core.api.*
import org.jetbrains.exposed.sql.*
import org.kodein.di.*
import org.kodein.di.ktor.controller.*

val PastaModule = DI.Module(name = PastaModuleName.name) {
    inBindSet(tag = "flyway-migrations") { add { singleton { Location("classpath:kitty/backend/modules/pasta/db/migrations") } } }

    inBindSet<Table>(tag = "tables-in-use") {
        add { provider { PastaFiles } }
    }

    inBindSet<DIController>(tag = "global-routes") { add { singleton { new(::PastaRoutes) } } }
}
