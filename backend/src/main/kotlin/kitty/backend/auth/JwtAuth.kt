package kitty.backend.auth

import com.auth0.jwk.*
import com.auth0.jwt.algorithms.*
import com.auth0.jwt.interfaces.*
import com.typesafe.config.*
import io.ktor.http.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.config.*
import io.ktor.server.response.*
import org.bouncycastle.asn1.pkcs.*
import org.bouncycastle.asn1.x509.*
import org.bouncycastle.crypto.params.*
import org.bouncycastle.crypto.util.*
import org.bouncycastle.math.ec.custom.sec.*
import org.bouncycastle.openssl.*
import org.bouncycastle.openssl.jcajce.*
import org.kodein.di.*
import java.io.*
import java.security.*
import java.security.interfaces.*
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.util.*

data class JwtConfig(
    val audience: String,
    val issuer: String,
    val keys: Jwks,
    val provider: JwkProvider,
    val realm: String,
    val signAlgorithm: Algorithm,
)

@Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
fun ApplicationConfig.jesusChristStopAnnoyingMeWithFuckingBugs(path: String): List<ApplicationConfig> =
    if (this is MergedApplicationConfig) {
        when (path) {
            in first.keys() -> first.jesusChristStopAnnoyingMeWithFuckingBugs(path)
            else -> second.jesusChristStopAnnoyingMeWithFuckingBugs(path)
        }
    } else {
        configList(path)
    }

fun jwtConfig(config: ApplicationConfig): JwtConfig {
    lateinit var privateKey: PrivateKey
    val publicKeys: MutableList<Jwk> = mutableListOf()

    fun processKey(key: String, props: Map<String, Any> = emptyMap()) {
        val parser = PEMParser(StringReader(key))
        var obj = parser.readObject()
        while (obj != null) {
            when (obj) {
                is PEMKeyPair -> {
                    privateKey = obj.privateKeyInfo.toPrivateKey()
                }

                is PrivateKeyInfo -> {
                    privateKey = obj.toPrivateKey()
                }

                is SubjectPublicKeyInfo -> {
                    publicKeys += obj.toJwk(props)
                }

                else -> throw IllegalArgumentException("Unsupported key $obj while parsing $key")
            }
            obj = parser.readObject()
        }
        parser.close()
    }

    processKey(config.property("privateKey").getString().trimIndent())

    config.jesusChristStopAnnoyingMeWithFuckingBugs("publicKeys").forEach { keyConfig ->
        val props = mutableMapOf<String, Any>()
        for (key in keyConfig.keys()) {
            val prop = keyConfig.property(key)
            try {
                props[key] = prop.getString()
                continue
            } catch (e: ConfigException.WrongType) {
                props[key] = prop.getList()
            }
        }
        val key = props.remove("key") as? String ?: return@forEach
        processKey(key.trimIndent(), props)
    }

    val issuer = config.property("issuer").getString()
    val audience = config.property("audience").getString()
    val realm = config.property("realm").getString()
    val provider = JwkProvider { keyId ->
        publicKeys.firstOrNull { it.id == keyId }
            ?: throw SigningKeyNotFoundException("No key found with kid $keyId", null)
    }

    val signKey = provider[config.property("signWith").getString()]

    fun rsaProvider(): RSAKeyProvider = object : RSAKeyProvider {
        override fun getPublicKeyById(keyId: String): RSAPublicKey = provider[keyId].publicKey as RSAPublicKey
        override fun getPrivateKey(): RSAPrivateKey = privateKey as RSAPrivateKey
        override fun getPrivateKeyId(): String = signKey.id
    }

    fun ecdsaProvider(): ECDSAKeyProvider = object : ECDSAKeyProvider {
        override fun getPublicKeyById(keyId: String): ECPublicKey = provider[keyId].publicKey as ECPublicKey
        override fun getPrivateKey(): ECPrivateKey = privateKey as ECPrivateKey
        override fun getPrivateKeyId(): String = signKey.id
    }

    val jwtAlgorithm = when (val alg = signKey.algorithm) {
        "RS256" -> Algorithm.RSA256(rsaProvider())
        "RS384" -> Algorithm.RSA384(rsaProvider())
        "RS512" -> Algorithm.RSA512(rsaProvider())
        "ES256" -> Algorithm.ECDSA256(ecdsaProvider())
        "ES384" -> Algorithm.ECDSA384(ecdsaProvider())
        "ES512" -> Algorithm.ECDSA512(ecdsaProvider())
        else -> throw IllegalArgumentException("Algorithm not supported: $alg")
    }

    return JwtConfig(
        audience = audience,
        issuer = issuer,
        keys = Jwks(publicKeys),
        provider = provider,
        realm = realm,
        signAlgorithm = jwtAlgorithm,
    )
}

fun AuthenticationConfig.jwt(di: DI, name: String = "auth-jwt") {
    val jwtConfig: JwtConfig by di.instance()

    jwt(name) {
        realm = jwtConfig.realm
        verifier(jwtConfig.provider, jwtConfig.issuer)
        validate { credential ->
            KittyPrincipal(credential.payload)
        }
        challenge { _, _ ->
            call.respond(HttpStatusCode.Unauthorized, "Token is not valid or has expired")
        }
    }
}

private fun PrivateKeyInfo.toPrivateKey(): PrivateKey = JcaPEMKeyConverter().getPrivateKey(this)

private fun SubjectPublicKeyInfo.toJwk(props: Map<String, Any>): Jwk {
    val p = props.toMutableMap()
    when (val key = PublicKeyFactory.createKey(this)) {
        is RSAKeyParameters -> {
            p.putIfAbsent("kty", "RSA")
            p.putIfAbsent("e", Base64.getUrlEncoder().encodeToString(key.exponent.toByteArray()))
            p.putIfAbsent("n", Base64.getUrlEncoder().encodeToString(key.modulus.toByteArray()))
        }

        is Ed25519PublicKeyParameters -> {
            p.putIfAbsent("kty", "OKP")
            p.putIfAbsent("alg", "EdDSA")
            p.putIfAbsent("crv", "Ed25519")
            p.putIfAbsent("x", Base64.getUrlEncoder().encodeToString(key.encoded))
        }

        is ECPublicKeyParameters -> {
            p.putIfAbsent("kty", "EC")
            when (key.q.curve) {
                is SecP256R1Curve -> p.putIfAbsent("crv", "P-256")
                is SecP384R1Curve -> p.putIfAbsent("crv", "P-384")
                is SecP521R1Curve -> p.putIfAbsent("crv", "P-521")
                else -> throw IllegalArgumentException("Unsupported curve: ${key.q.curve}")
            }
            p.putIfAbsent("x", Base64.getUrlEncoder().encodeToString(key.q.xCoord.encoded))
            p.putIfAbsent("y", Base64.getUrlEncoder().encodeToString(key.q.yCoord.encoded))
        }

        else -> throw IllegalArgumentException("Unsupported key type $key")
    }
    return Jwk.fromValues(p)
}