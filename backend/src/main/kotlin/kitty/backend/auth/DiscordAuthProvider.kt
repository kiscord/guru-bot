package kitty.backend.auth

import com.auth0.jwt.*
import kitty.backend.*
import kitty.backend.db.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.server.auth.*
import io.ktor.server.plugins.*
import io.ktor.util.*
import kiscord.*
import kiscord.api.*
import kiscord.ktor.*
import kotlinx.coroutines.*
import kotlinx.datetime.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.*
import org.kodein.di.*
import java.util.*
import kotlin.time.*
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

class DiscordAuthProvider(
    override val di: DI,
    override val id: String,
    private val accessTokenDuration: Duration = 1.hours,
    private val refreshTokenDuration: Duration = 14.days,
) : AuthProvider {
    private val jwtConfig: JwtConfig by instance()
    private val kiscord: Kiscord by instance()
    private val database: Database by instance()
    private val httpClient: HttpClient by instance()
    private val darkKittyConfig: KittyConfig by instance()
    private val json: Json by instance()

    override suspend fun auth(principal: Principal): BearerTokens {
        if (principal !is OAuthAccessTokenResponse.OAuth2) throw IllegalStateException("Invalid principal type: $principal")

        principal.extraParameters["guild"]?.let { guildRaw ->
            val guild = json.decodeFromString(ExtendedAuthGuild.serializer(), guildRaw)

            newSuspendedTransaction(context = Dispatchers.IO, db = database) {
                fun DiscordGuild.takeFrom(guild: ExtendedAuthGuild) {
                    name = guild.name
                    icon = guild.icon
                    managed = true
                }

                DiscordGuild.findById(guild.id)?.apply { takeFrom(guild) }
                    ?: DiscordGuild.new(guild.id) { takeFrom(guild) }
            }
        }



        return discordAuth(
            tokens = BearerTokens(
                accessToken = principal.accessToken,
                refreshToken = principal.refreshToken ?: throw IllegalStateException("Missing refresh token")
            ),
            duration = principal.expiresIn.seconds,
        )
    }

    override suspend fun refresh(principal: Principal): BearerTokens {
        if (principal !is KittyPrincipal) throw IllegalStateException("Invalid principal type: $principal")

        val discordRefreshToken = principal.discordRefreshToken ?: throw IllegalStateException("Missing refresh token")

        val response = httpClient.post("https://discord.com/api/oauth2/token") {
            accept(ContentType.Application.Json)

            setBody(FormDataContent(Parameters.build {
                set("client_id", darkKittyConfig.clientId.toString())
                set("client_secret", darkKittyConfig.clientSecret)
                set("grant_type", "refresh_token")
                set("refresh_token", discordRefreshToken)
            }))
        }

        if (response.status != HttpStatusCode.OK) throw UnauthorizedException()

        val body = response.body<DiscordAccessTokenResponse>()

        return discordAuth(
            tokens = BearerTokens(
                accessToken = body.accessToken,
                refreshToken = body.refreshToken,
            ),
            duration = body.expiresIn.seconds
        )
    }

    private suspend fun discordAuth(tokens: BearerTokens, duration: Duration): BearerTokens {
        val auth = kiscord.withMixin({
            token(Token.bearer(tokens.accessToken))
        }) {
            oAuth2.getCurrentAuthorizationInformation()
        }

        val discordUser = auth.user ?: throw IllegalStateException("Missing identify scope")

        fun issueToken(block: JWTCreator.Builder.() -> Unit): String {
            val builder = JWT.create()
            builder.withIssuer(jwtConfig.issuer)
                .withAudience(jwtConfig.audience)
                .withHeader(mapOf("jku" to jwtConfig.issuer + KittyPrincipal.JWKS_PATH))
            builder.apply(block)
            return builder.sign(jwtConfig.signAlgorithm)
        }

        newSuspendedTransaction(context = Dispatchers.IO, db = database) {
            DiscordUser.updateUser(discordUser)
        }

        val now = Clock.System.now()

        val accessToken = issueToken {
            withIssuedAt(now)
            withExpiresAt(now + accessTokenDuration.coerceAtMost(duration))
            withClaim(KittyPrincipal.CLAIM_PROVIDER, id)
            withClaim(
                KittyPrincipal.CLAIM_DISCORD, mapOf(
                    "user_id" to discordUser.id.toString(),
                    "access_token" to tokens.accessToken,
                )
            )
        }

        val refreshToken = issueToken {
            withIssuedAt(now)
            withExpiresAt(now + refreshTokenDuration.coerceAtMost(duration))
            withClaim(KittyPrincipal.CLAIM_PROVIDER, id)
            withClaim(
                KittyPrincipal.CLAIM_DISCORD, mapOf(
                    "user_id" to discordUser.id.toString(),
                    "refresh_token" to tokens.refreshToken,
                )
            )
        }

        return BearerTokens(
            accessToken = accessToken,
            refreshToken = refreshToken,
        )
    }

    @Serializable
    private data class ExtendedAuthGuild(
        val id: Snowflake,
        val name: String,
        val icon: String?,
    )

    override fun AuthenticationConfig.install() {
        oauth("auth-$id") {
            urlProvider = { settings ->
                val origin = request.origin
                val protocol = URLProtocol.createOrDefault(origin.scheme)
                URLBuilder(
                    protocol = protocol,
                    port = if (origin.serverPort == protocol.defaultPort) DEFAULT_PORT else origin.serverPort,
                    host = origin.serverHost,
                    pathSegments = listOf("api", "oauth2", "callback", settings.name)
                ).buildString()
            }
            providerLookup = {
                val scopes = mutableListOf("identify", "guilds", "guilds.members.read")
                val guildId = request.queryParameters["guildId"]?.let { Snowflake(it) }
                if (guildId != null || "guild" in request.queryParameters) {
                    scopes += "applications.commands"
                    if (darkKittyConfig.mode.isBot) {
                        scopes += "bot"
                    }
                }
                val extraAuthParameters = mutableListOf<Pair<String, String>>()
                extraAuthParameters += "prompt" to "none"
                if (guildId != null) {
                    extraAuthParameters += "guild_id" to guildId.toString()
                    extraAuthParameters += "disable_guild_select" to "true"
                }
                OAuthServerSettings.OAuth2ServerSettings(
                    name = "discord",
                    authorizeUrl = "https://discord.com/api/oauth2/authorize",
                    accessTokenUrl = "https://discord.com/api/oauth2/token",
                    requestMethod = HttpMethod.Post,
                    clientId = darkKittyConfig.clientId.toString(),
                    clientSecret = darkKittyConfig.clientSecret,
                    defaultScopes = scopes,
                    nonceManager = StatelessHmacNonceManager(
                        key = darkKittyConfig.hmacSecret.toByteArray(),
                        timeoutMillis = 10.minutes.inWholeMilliseconds
                    ),
                    extraAuthParameters = extraAuthParameters,
                )
            }
            client = httpClient
        }
    }

    @Serializable
    private data class DiscordAccessTokenResponse(
        @SerialName("access_token")
        val accessToken: String,
        @SerialName("refresh_token")
        val refreshToken: String,
        @SerialName("token_type")
        val tokenType: String,
        @SerialName("expires_in")
        val expiresIn: Long,
        val scope: String,
    )
}

private fun JWTCreator.Builder.withExpiresAt(expiresAt: Instant): JWTCreator.Builder =
    withExpiresAt(Date.from(expiresAt.toJavaInstant()))

private fun JWTCreator.Builder.withIssuedAt(issuedAt: Instant): JWTCreator.Builder =
    withIssuedAt(Date.from(issuedAt.toJavaInstant()))
