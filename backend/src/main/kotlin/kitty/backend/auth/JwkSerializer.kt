package kitty.backend.auth

import com.auth0.jwk.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*

object JwkSerializer : KSerializer<Jwk> {
    private val mapSerializer = MapSerializer(String.serializer(), String.serializer())
    override val descriptor: SerialDescriptor get() = mapSerializer.descriptor

    override fun deserialize(decoder: Decoder): Jwk {
        check(decoder is JsonDecoder)
        val props = decoder.decodeJsonElement().jsonObject.asRaw()
        return Jwk.fromValues(props)
    }

    override fun serialize(encoder: Encoder, value: Jwk) {
        check(encoder is JsonEncoder)
        val map = mutableMapOf<String, JsonElement>()
        map.putAll(value.additionalAttributes.mapValues { (_, value) -> value.toRaw() })
        value.id?.let { map.put("kid", it.toRaw()) }
        value.type?.let { map.put("kty", it.toRaw()) }
        value.algorithm?.let { map.put("alg", it.toRaw()) }
        value.usage?.let { map.put("use", it.toRaw()) }
        value.operationsAsList?.let { map.put("key_ops", it.toRaw()) }
        value.certificateUrl?.let { map.put("x5u", it.toRaw()) }
        value.certificateChain?.let { map.put("x5c", it.toRaw()) }
        value.certificateThumbprint?.let { map.put("x5t", it.toRaw()) }
        encoder.encodeJsonElement(JsonObject(map))
    }

    private fun JsonElement.asRaw(): Any = when (this) {
        is JsonObject -> asRaw()
        is JsonArray -> asRaw()
        is JsonPrimitive -> when {
            isString -> content
            else -> throw IllegalArgumentException("Not supported json primitive: $this")
        }
        else -> throw IllegalArgumentException("Not supported json element: $this")
    }

    private fun JsonObject.asRaw(): Map<String, Any?> {
        return mapValues { (_, value) -> value.asRaw() }
    }

    private fun JsonArray.asRaw(): List<Any?> = map { it.asRaw() }

    @Suppress("UNCHECKED_CAST")
    private fun Any?.toRaw(): JsonElement = when (this) {
        null -> JsonNull
        is String -> JsonPrimitive(this)
        is List<*> -> JsonArray(map { it.toRaw() })
        is Map<*, *> -> JsonObject((this as Map<String, Any?>).mapValues { it.toRaw() })
        else -> throw IllegalArgumentException("Not supported value element: ${this::class} $this")
    }
}

