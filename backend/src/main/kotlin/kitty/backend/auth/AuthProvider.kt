package kitty.backend.auth

import io.ktor.client.plugins.auth.providers.*
import io.ktor.server.auth.*
import org.kodein.di.*

interface AuthProvider : DIAware {
    val id: String

    suspend fun auth(principal: Principal): BearerTokens
    suspend fun refresh(principal: Principal): BearerTokens

    fun AuthenticationConfig.install()
}