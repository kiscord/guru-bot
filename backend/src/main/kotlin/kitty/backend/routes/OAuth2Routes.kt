package kitty.backend.routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kitty.backend.*
import kitty.backend.auth.*
import kitty.resources.*
import org.kodein.di.*
import io.ktor.server.resources.post as postResource

class OAuth2Routes(application: Application) : AbstractKittyController(application) {
    private val authProviders: Set<AuthProvider> by instance()

    override fun Route.getRoutes() {
        val resourceFormat = application.plugin(Resources).resourcesFormat
        val loginPattern = resourceFormat.encodeToPathPattern(OAuth2Resource.Login.serializer())
        val callbackPattern = resourceFormat.encodeToPathPattern(OAuth2Resource.Callback.serializer())

        authProviders.forEach { authProvider ->
            authenticate("auth-${authProvider.id}") {
                get(loginPattern.replace("{provider}", authProvider.id)) {

                }

                get(callbackPattern.replace("{provider}", authProvider.id)) {
                    val principal = authorize<Principal>()

                    val tokens = authProvider.auth(principal)

                    call.respondRedirect {
                        path("/login")
                        parameters.clear()
                        parameters["access_token"] = tokens.accessToken
                        parameters["refresh_token"] = tokens.refreshToken
                    }
                }
            }
        }

        authenticate("auth-kitty") {
            get<OAuth2Resource.Check> {
                call.respond(HttpStatusCode.NoContent)
            }

            postResource<OAuth2Resource.Refresh> {
                val principal = authorize<KittyPrincipal>()

                val providerId = principal.provider
                val authProvider = authProviders.find { it.id == providerId }
                    ?: throw UnauthorizedException()

                val tokens = authProvider.refresh(principal)

                call.respond(tokens)
            }
        }
    }
}

