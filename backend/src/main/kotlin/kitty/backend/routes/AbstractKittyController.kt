package kitty.backend.routes

import io.ktor.server.application.*
import kotlinx.coroutines.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.*
import org.kodein.di.*
import org.kodein.di.ktor.controller.*

abstract class AbstractKittyController(application: Application) : AbstractDIController(application) {
    private val database: Database by instance()

    suspend fun <T> withTransaction(block: suspend Transaction.() -> T): T {
        return newSuspendedTransaction(context = Dispatchers.IO, db = database, statement = block)
    }
}