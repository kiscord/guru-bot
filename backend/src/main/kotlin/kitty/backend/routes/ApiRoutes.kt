package kitty.backend.routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.application.Application
import io.ktor.server.auth.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kiscord.*
import kiscord.api.*
import kiscord.ktor.*
import kitty.*
import kitty.backend.*
import kitty.backend.auth.*
import kitty.backend.db.*
import kitty.resources.*
import kotlinx.datetime.*
import org.kodein.di.*
import kotlin.time.Duration.Companion.minutes

class ApiRoutes(application: Application) : AbstractKittyController(application) {
    private val kiscord: Kiscord by instance()

    override fun Route.getRoutes() {
        authenticate("auth-kitty") {
            get<ApiResource.Users.Me> {
                val principal = authorize<KittyPrincipal>()

                val discordAccessToken = principal.discordAccessToken

                val userView = withTransaction {
                    when {
                        discordAccessToken != null -> {
                            val userId = principal.discordUserId
                            val discordUser = DiscordUser[userId]
                            if (discordUser.updatedAt + 5.minutes <= Clock.System.now()) {
                                val user = kiscord.withMixin({
                                    token(Token.bearer(discordAccessToken))
                                }) {
                                    user.getCurrentUser()
                                }
                                discordUser.takeFrom(user)
                                discordUser.updatedAt = Clock.System.now()
                            }
                            discordUser
                        }

                        else -> null
                    }?.asProfile()
                }

                if (userView != null) {
                    call.respond(userView)
                } else {
                    call.respond(HttpStatusCode.Unauthorized)
                }
            }

            get<ApiResource.Users.Me.Guilds> {
                val principal = authorize<KittyPrincipal>()

                val accessToken = principal.discordAccessToken
                    ?: throw IllegalStateException("Missing discord access token")

                val guilds = kiscord.withMixin({
                    token(Token.bearer(accessToken))
                }) {
                    user.getCurrentUserGuilds()
                }

                val guildsDTO = withTransaction {
                    DiscordGuild.syncGuilds(principal.discordUserId, guilds)
                }

                call.respond(guildsDTO)
            }

            get<ApiResource.Users.Info> { resource ->
                authorize<KittyPrincipal>()

                val profile = withTransaction {
                    DiscordUser.findById(resource.userId)?.asProfile()
                }

                when (profile) {
                    null -> call.respond(HttpStatusCode.NotFound)
                    else -> call.respond(profile)
                }
            }
        }

        get<ApiResource.Stats> {
            withTransaction {
                val guilds = DiscordGuild.find { DiscordGuilds.managed eq true }.count()
                val users = DiscordUser.count()

                call.respond(
                    StatsResponse(
                        guilds = guilds,
                        users = users,
                    )
                )
            }
        }

        get<ApiResource.Users.Bot> {
            val user = kiscord.user.getCurrentUser()
            call.response.cacheControl(CacheControl.MaxAge(
                maxAgeSeconds = 3600,
                visibility = CacheControl.Visibility.Public,
            ))
            call.respond(
                UserProfile(
                    id = user.id,
                    username = user.username,
                    discriminator = user.discriminator,
                    avatar = user.avatar,
                    flags = user.flags,
                    bot = user.bot,
                )
            )
        }
    }
}
