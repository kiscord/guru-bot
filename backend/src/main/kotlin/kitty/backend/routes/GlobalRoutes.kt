package kitty.backend.routes

import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.application.Application
import io.ktor.server.http.content.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kiscord.*
import kiscord.api.*
import kitty.backend.*
import kitty.backend.auth.*
import kitty.resources.*
import kotlinx.coroutines.*
import org.kodein.di.*
import java.awt.*
import java.awt.image.*
import java.io.*
import java.util.concurrent.*
import javax.imageio.*
import kotlin.time.*
import kotlin.time.Duration.Companion.days
import java.awt.Image as AwtImage

class GlobalRoutes(application: Application) : AbstractKittyController(application) {
    private val jwtConfig: JwtConfig by instance()
    private val kiscord: Kiscord by instance()

    override fun Route.getRoutes() {
        val staticRootDir = System.getenv("STATIC_ROOT_DIR")?.let { File(it) }
        if (staticRootDir != null) {
            staticRootFolder = staticRootDir
            preCompressed {
                singlePageApplication {
                    filesPath = staticRootDir.canonicalPath
                }
            }
        }

        get("/healthz") {
            call.respondText("Healthy")
        }

        get<WellKnownResource.JwksJson> {
            call.acceptOnly(ContentType.Application.Json)
            call.response.cacheControl(CacheControl.MaxAge(1.days.toInt(DurationUnit.SECONDS)))
            call.respond(jwtConfig.keys)
        }

        get("/favicon.png") {
            val size = (call.request.queryParameters["size"]?.toIntOrNull() ?: 64).coerceIn(16, 256)

            val result = faviconCache.getOrPut(size) {
                async(Dispatchers.IO) {
                    val user = kiscord.user.getCurrentUser()
                    val avatar = user.avatarImage
                    val avatarStream = kiscord.httpClient.get(avatar.url).body<InputStream>()
                    val avatarImage = withContext(Dispatchers.IO) {
                        ImageIO.read(avatarStream)
                    }
                    val avatarScaledImage = avatarImage.scale(size, size, AwtImage.SCALE_SMOOTH)
                    val outputStream = ByteArrayOutputStream()
                    ImageIO.write(avatarScaledImage, "png", outputStream)
                    outputStream.toByteArray()
                }
            }

            val cacheDuration = 1.days.inWholeSeconds.toInt()
            call.response.cacheControl(CacheControl.MaxAge(
                maxAgeSeconds = cacheDuration,
                proxyMaxAgeSeconds = cacheDuration,
                visibility = CacheControl.Visibility.Public,
            ))
            call.respondBytes(ContentType.Image.PNG) {
                result.await()
            }
        }
    }

    private val faviconCache = ConcurrentHashMap<Int, Deferred<ByteArray>>()
}

private fun AwtImage.scale(width: Int, height: Int, hints: Int): BufferedImage {
    val scaled = getScaledInstance(width, height, hints)
    val buffer = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
    val graphics = buffer.graphics
    if (graphics is Graphics2D) {
        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC)
        graphics.setRenderingHint(
            RenderingHints.KEY_ALPHA_INTERPOLATION,
            RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY
        )
    }
    graphics.drawImage(scaled, 0, 0, width, height, null)
    graphics.dispose()
    return buffer
}
