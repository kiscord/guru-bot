package kitty.backend

import io.kubernetes.client.extended.leaderelection.*
import io.kubernetes.client.extended.leaderelection.Lock
import io.kubernetes.client.extended.leaderelection.resourcelock.*
import io.kubernetes.client.openapi.*
import io.kubernetes.client.util.*
import kitty.*
import kitty.backend.leader.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import okio.*
import org.kodein.di.*
import org.kodein.log.*
import java.util.*
import kotlin.time.*
import kotlin.time.Duration.Companion.seconds

val KubernetesModule = DI.Module("kitty-backend-kubernetes") {
    bindSingleton<ApiClient> {
        val config = instance<KittyConfig>()
        val kubeConfig = config.kubernetes.kubeConfig
        val clientBuilder = if (kubeConfig != null) {
            FileSystem.SYSTEM.read(kubeConfig) {
                ClientBuilder.kubeconfig(KubeConfig.loadKubeConfig(inputStream().reader()))
            }
        } else {
            ClientBuilder.cluster()
        }

        clientBuilder.build()
    }

    bindSingleton {
        fun error(reason: String): Nothing {
            throw IllegalStateException("Failed to create leader elector: $reason")
        }

        val config = instance<KittyConfig>()
        val namespace = config.kubernetes.namespace ?: error("missing namespace")
        val leaseName = config.kubernetes.leaseName ?: error("missing lease name")
        val identity = config.kubernetes.identity ?: UUID.randomUUID().toString()
        val leaseDuration = config.kubernetes.leaseDuration ?: 10.seconds
        val renewDeadline = config.kubernetes.renewDeadline ?: 8.seconds
        val retryPeriod = config.kubernetes.retryPeriod ?: 2.seconds
        KubernetesLeaderElector(
            apiClient = instance(),
            logger = instance<LoggerFactory>().newLogger<KubernetesLeaderElector>(),
            namespace = namespace,
            leaseName = leaseName,
            identity = identity,
            leaseDuration = leaseDuration,
            renewDeadline = renewDeadline,
            retryPeriod = retryPeriod,
        )
    }
    delegate<LeaderElection>(overrides = true).to<KubernetesLeaderElector>()
    inBindSet<AutoCloseable>(tag = "auto-closeable") { add { provider { instance<KubernetesLeaderElector>() } } }

    onReady {
        val elector: KubernetesLeaderElector = instance()
        val scope: CoroutineScope = instance(tag = "background-scope")

        scope.launch {
            elector.start()
        }
    }
}

private class KubernetesLeaderElector(
    val apiClient: ApiClient,
    val logger: Logger,
    namespace: String,
    leaseName: String,
    identity: String,
    leaseDuration: Duration,
    renewDeadline: Duration,
    retryPeriod: Duration,
) : LeaderElection, AutoCloseable {
    private val _status = MutableStateFlow(LeaderElection.Status.Follower)
    override val status: StateFlow<LeaderElection.Status> = _status.asStateFlow()

    private val lock: Lock
    private val elector: LeaderElector

    init {
        lock = LeaseLock(namespace, leaseName, identity, apiClient)
        val config = LeaderElectionConfig(
            lock,
            leaseDuration.toJavaDuration(),
            renewDeadline.toJavaDuration(),
            retryPeriod.toJavaDuration(),
        )
        elector = LeaderElector(config)
    }

    private lateinit var job: Job

    suspend fun start() {
        coroutineScope {
            job = coroutineContext.job

            launch {
                _status.collectLatest { status ->
                    logger.info { "Leadership status changed to $status" }
                }
            }

            while (job.isActive) {
                elector.run({
                    _status.value = LeaderElection.Status.Leader
                }, {
                    _status.value = LeaderElection.Status.Follower
                })

                yield()
            }
        }
    }

    override fun close() {
        if (::job.isInitialized) {
            job.cancel("elector is closing")
        }
        elector.close()
    }
}
