package kitty.backend.db

import org.flywaydb.core.api.logging.*
import org.kodein.log.*

class FlywayLogCreator : LogCreator {
    private val loggerFactory = loggerFactoryLocal.get()

    companion object {
        val loggerFactoryLocal: ThreadLocal<LoggerFactory> = ThreadLocal.withInitial { LoggerFactory.default }
    }

    override fun createLogger(clazz: Class<*>): Log {
        val tag = Logger.Tag(clazz.`package`.name, clazz.simpleName)
        val logger = loggerFactory.newLogger(tag)
        return KodeinLog(logger)
    }

    private class KodeinLog(val logger: Logger) : Log {
        override fun isDebugEnabled(): Boolean = true

        override fun debug(message: String) {
            logger.debug { message }
        }

        override fun info(message: String) {
            logger.info { message }
        }

        override fun warn(message: String) {
            logger.warning { message }
        }

        override fun error(message: String) {
            logger.error { message }
        }

        override fun error(message: String, e: Exception?) {
            logger.error(e) { message }
        }

        override fun notice(message: String) {}
    }
}
