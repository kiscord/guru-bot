package kitty.backend.db

import kiscord.api.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.sql.*
import java.util.*
import kiscord.api.User as DiscordUser

interface ColumnTransformer<TColumn, TReal> {
    fun toColumn(value: TReal): TColumn
    fun toReal(value: TColumn): TReal
}

fun <TColumn, TReal> Column<TColumn>.transform(transformer: ColumnTransformer<TColumn, TReal>): ColumnWithTransform<TColumn, TReal> {
    return ColumnWithTransform(this, transformer::toColumn, transformer::toReal, false)
}

@JvmName("transformNullable")
fun <TColumn : Any, TReal : Any> Column<TColumn?>.transform(transformer: ColumnTransformer<TColumn, TReal>): ColumnWithTransform<TColumn?, TReal?> {
    return transform(transformer.nullable)
}

fun <TColumn, TReal> Column<TColumn>.memoizedTransform(transformer: ColumnTransformer<TColumn, TReal>): ColumnWithTransform<TColumn, TReal> {
    return ColumnWithTransform(this, transformer::toColumn, transformer::toReal, true)
}

open class JsonTransformer<T : Any>(private val serializer: KSerializer<T>) : ColumnTransformer<String, T> {
    override fun toColumn(value: T): String = Json.encodeToString(serializer, value)
    override fun toReal(value: String): T = Json.decodeFromString(serializer, value)
}

object EmbedTransformer : JsonTransformer<Embed>(Embed.serializer())

@OptIn(KiscordUnstableAPI::class)
object UserFlagsTransformer : ColumnTransformer<Int, Set<DiscordUser.Flag>> {
    override fun toColumn(value: Set<DiscordUser.Flag>): Int {
        return value.fold(0) { acc, flag ->
            acc or DiscordUser.Flag.enumToValue(flag)
        }
    }

    override fun toReal(value: Int): Set<DiscordUser.Flag> {
        if (value == 0) return emptySet()
        var mask = value
        val set = EnumSet.noneOf(DiscordUser.Flag::class.java)
        while (mask != 0) {
            val flag = mask.takeLowestOneBit()
            mask = mask and flag.inv()
            val enum = DiscordUser.Flag.valueToEnum(flag)
            if (enum != null) {
                set += enum
            }
        }
        return set
    }
}

@OptIn(KiscordUnstableAPI::class)
object PermissionSetTransformer : ColumnTransformer<ULong, PermissionSet> {
    override fun toColumn(value: PermissionSet): ULong {
        return value.fold(0UL) { acc, flag ->
            acc or Permission.enumToValue(flag)
        }
    }

    override fun toReal(value: ULong): PermissionSet {
        if (value == 0UL) return PermissionSet.Empty
        var mask = value
        val set = EnumSet.noneOf(Permission::class.java)
        while (mask != 0UL) {
            val flag = mask.takeLowestOneBit()
            mask = mask and flag.inv()
            val enum = Permission.valueToEnum(flag)
            if (enum != null) {
                set += enum
            }
        }
        return PermissionSet(set)
    }
}

val <TColumn : Any, TReal : Any> ColumnTransformer<TColumn, TReal>.nullable: ColumnTransformer<TColumn?, TReal?>
    get() = NullableTransformer(this)

private class NullableTransformer<TColumn : Any, TReal : Any>(
    private val transformer: ColumnTransformer<TColumn, TReal>,
) : ColumnTransformer<TColumn?, TReal?> {
    override fun toColumn(value: TReal?): TColumn? = value?.let { transformer.toColumn(it) }
    override fun toReal(value: TColumn?): TReal? = value?.let { transformer.toReal(it) }
}
