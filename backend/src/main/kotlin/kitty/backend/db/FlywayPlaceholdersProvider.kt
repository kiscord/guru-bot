package kitty.backend.db

interface FlywayPlaceholdersProvider {
    fun providePlaceholders(): Map<String, String>
}
