package kitty.backend.db

import kitty.*
import kiscord.api.*
import org.jetbrains.exposed.dao.id.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.inList
import org.jetbrains.exposed.sql.statements.*

object DiscordGuilds : SnowflakeIdTable() {
    val name = varchar("name", 100)
    val icon = varchar("icon", 34).nullable()
    val managed = bool("managed").default(false)
}

class DiscordGuild(id: EntityID<Snowflake>) : SnowflakeEntity(id) {
    var name by DiscordGuilds.name
    var icon by DiscordGuilds.icon
    var managed by DiscordGuilds.managed

    companion object : SnowflakeEntityClass<DiscordGuild>(DiscordGuilds) {
        fun syncGuilds(userId: Snowflake, guilds: Collection<PartialGuild>): List<DiscordGuildDTO> {
            fun DiscordGuild.takeFrom(guild: PartialGuild) {
                name = guild.name ?: throw IllegalArgumentException("Guild should have name")
                icon = guild.icon
            }

            fun UpdateBuilder<*>.takeGuildMember(guild: PartialGuild) {
                this[DiscordGuildMembers.owner] = guild.owner ?: false
                this[DiscordGuildMembers.permissions] =
                    PermissionSetTransformer.toColumn(guild.permissions ?: PermissionSet.Empty)
            }

            val userSelector = DiscordGuildMembers.user eq userId

            val obsoleteGuilds = DiscordGuildMembers.slice(DiscordGuildMembers.guild).select { userSelector }
                .map { it[DiscordGuildMembers.guild].value } - guilds.mapNotNull { it.id }.toSet()

            if (obsoleteGuilds.isNotEmpty()) {
                DiscordGuildMembers.deleteWhere {
                    userSelector and (guild inList obsoleteGuilds)
                }
            }

            return guilds.map { partialGuild ->
                val guildId = partialGuild.id ?: throw IllegalArgumentException("Guild should have id")
                val discordGuild =
                    findById(guildId)?.apply { takeFrom(partialGuild) } ?: new(guildId) { takeFrom(partialGuild) }

                val guildMemberSelector =
                    userSelector and (DiscordGuildMembers.guild eq guildId)
                val hasGuildMember = DiscordGuildMembers.select { guildMemberSelector }.limit(1).any()

                if (hasGuildMember) {
                    DiscordGuildMembers.update(where = {
                        guildMemberSelector
                    }) {
                        it.takeGuildMember(partialGuild)
                    }
                } else {
                    DiscordGuildMembers.insert {
                        it[user] = userId
                        it[guild] = guildId
                        it.takeGuildMember(partialGuild)
                    }
                }

                val allowedToManage =
                    partialGuild.owner == true || Permission.ManageGuild in partialGuild.permissions.orEmpty()

                DiscordGuildDTO(
                    id = guildId,
                    name = discordGuild.name,
                    icon = discordGuild.icon,
                    owner = partialGuild.owner ?: false,
                    permissions = partialGuild.permissions ?: PermissionSet.Empty,
                    managed = discordGuild.managed,
                    allowedToManage = allowedToManage,
                )
            }
        }
    }
}
