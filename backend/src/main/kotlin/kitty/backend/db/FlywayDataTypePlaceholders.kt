package kitty.backend.db

import kotlinx.datetime.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.kotlin.datetime.*
import org.jetbrains.exposed.sql.statements.jdbc.*
import org.jetbrains.exposed.sql.transactions.*
import org.jetbrains.exposed.sql.vendors.*
import java.util.*

object FlywayDataTypePlaceholders : FlywayPlaceholdersProvider {
    override fun providePlaceholders(): Map<String, String> {
        val manager = TransactionManager.current()
        val dialect = manager.db.dialect
        val dataTypeProvider = dialect.dataTypeProvider

        val map = mapping.entries.associateTo(mutableMapOf()) { (name, mapper) ->
            "$PREFIX:$name" to mapper(dataTypeProvider)
        }

        expressionMapping.entries.associateTo(map) { (name, expression) ->
            val queryBuilder = QueryBuilder(false)
            expression.toQueryBuilder(queryBuilder)
            "$PREFIX:$name" to queryBuilder.toString()
        }

        (manager.connection as? JdbcConnectionImpl)?.let { connection ->
            map["q"] = connection.connection.metaData.identifierQuoteString
        }

        map["PK_FOR_AUTOINC"] = if (dialect is SQLiteDialect) "" else "PRIMARY KEY"

        return Collections.unmodifiableMap(map)
    }

    private const val PREFIX = "datatype"

    @Suppress("INVISIBLE_REFERENCE")
    private val mapping = mapOf(
        "blob" to DataTypeProvider::blobType,
        "boolean" to DataTypeProvider::booleanType,
        "byte" to DataTypeProvider::byteType,
        "dateTime" to DataTypeProvider::dateTimeType,
        "double" to DataTypeProvider::doubleType,
        "false" to { it.booleanToStatementString(false) },
        "float" to DataTypeProvider::floatType,
        "integer" to DataTypeProvider::integerType,
        "integerAutoinc" to DataTypeProvider::integerAutoincType,
        "long" to DataTypeProvider::longType,
        "longAutoinc" to DataTypeProvider::longAutoincType,
        "short" to DataTypeProvider::shortType,
        "text" to DataTypeProvider::textType,
        "time" to DataTypeProvider::timeType,
        "true" to { it.booleanToStatementString(true) },
        "ubyte" to DataTypeProvider::ubyteType,
        "uinteger" to DataTypeProvider::uintegerType,
        "ulong" to DataTypeProvider::ulongType,
        "ushort" to DataTypeProvider::ushortType,
    ) + (1..256).associate { length ->
        "binary($length)" to { provider ->
            if (provider is PostgreSQLDataTypeProvider) {
                provider.binaryType()
            } else {
                provider.binaryType(length)
            }
        }
    }

    private val expressionMapping = mapOf<String, Expression<*>>(
        "current_timestamp" to CurrentTimestamp<Instant>(),
    )
}
