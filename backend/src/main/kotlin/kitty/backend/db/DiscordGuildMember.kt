@file:OptIn(ExperimentalUnsignedTypes::class)

package kitty.backend.db

import kitty.resources.*
import kiscord.api.*
import org.jetbrains.exposed.sql.*

object DiscordGuildMembers : Table() {
    val user = reference("user", DiscordUsers, onDelete = ReferenceOption.CASCADE)
    val guild = reference("guild", DiscordGuilds, onDelete = ReferenceOption.CASCADE)
    val owner = bool("owner")
    val permissions = ulong("permissions")

    init {
        uniqueIndex(user, guild)
    }
}
