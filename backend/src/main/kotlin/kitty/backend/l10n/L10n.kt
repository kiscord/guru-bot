@file:OptIn(KiscordUnstableAPI::class)

package kitty.backend.l10n

import kiscord.api.*
import kiscord.builder.*
import java.util.*
import kiscord.api.ApplicationCommandOption.Builder as ApplicationCommandOptionBuilder
import kiscord.api.CommandApi.ApplicationCommandInfo.Builder as ApplicationCommandBuilder
import kiscord.api.Locale as DiscordLocale

interface L10n {
    operator fun get(key: String): String
    operator fun get(key: String, locale: DiscordLocale): String

    fun find(key: String, locale: DiscordLocale): String?

    fun getLocalized(key: String, default: String = get(key)): Map<DiscordLocale, String>


    class Bundle(
        basename: String,
        control: ResourceBundle.Control = FlexibleControl.Default
    ) : L10n {
        private val root = ResourceBundle.getBundle(basename, Locale.ROOT, control)
        private val locales: Map<DiscordLocale, ResourceBundle> = DiscordLocale.values().associateWith {
            val locale = Locale.forLanguageTag(DiscordLocale.enumToValue(it))
            ResourceBundle.getBundle(basename, locale, control)
        }.filterValues { it.locale != Locale.ROOT }

        override operator fun get(key: String): String = root.getString(key)

        override fun get(key: String, locale: DiscordLocale): String {
            return find(key, locale) ?: get(key)
        }

        override fun find(key: String, locale: DiscordLocale): String? {
            val bundle = locales[locale] ?: return null
            return bundle.getString(key)
        }

        override fun getLocalized(key: String, default: String): Map<DiscordLocale, String> {
            return locales.mapValues { (_, bundle) ->
                bundle.getString(key)
            }.filterValues { it != default }
        }
    }
}

context(L10n) @BuilderDsl
fun ApplicationCommandBuilder.localize(nameKey: String, descriptionKey: String) {
    val name = get(nameKey)
    name(name)
    nameLocalizations(getLocalized(nameKey, name))
    val description = get(descriptionKey)
    description(description)
    descriptionLocalizations(getLocalized(descriptionKey, description))
}

context(L10n) @BuilderDsl
fun ApplicationCommandOptionBuilder.localize(nameKey: String, descriptionKey: String) {
    val name = get(nameKey)
    name(name)
    nameLocalizations(getLocalized(nameKey, name))
    val description = get(descriptionKey)
    description(description)
    descriptionLocalizations(getLocalized(descriptionKey, description))
}

private class L10NWithOrderedLocales(val l10n: L10n, val locales: Collection<DiscordLocale>) : L10n by l10n {
    override fun get(key: String): String {
        return locales.firstNotNullOfOrNull { locale ->
            l10n.find(key, locale)
        } ?: l10n[key]
    }
}

fun L10n.withOrderedLocales(locales: Collection<DiscordLocale>): L10n = when (this) {
    is L10NWithOrderedLocales -> L10NWithOrderedLocales(l10n, locales)
    else -> L10NWithOrderedLocales(this, locales)
}

context (L10n)
fun Interaction.l10n(takeUserLocale: Boolean = true, takeGuildLocale: Boolean = true): L10n {
    val locales = LinkedHashSet<DiscordLocale>(2)
    if (takeUserLocale) locale?.let(locales::add)
    if (takeGuildLocale) guildLocale?.let(locales::add)

    if (locales.isEmpty()) return this@L10n

    return withOrderedLocales(locales)
}