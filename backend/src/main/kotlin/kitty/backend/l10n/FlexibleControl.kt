package kitty.backend.l10n

import io.ktor.utils.io.charsets.*
import java.io.*
import java.util.*
import kotlin.collections.HashMap

class FlexibleControl(
    private val charset: Charset = Charsets.UTF_8
) : ResourceBundle.Control() {
    override fun getFormats(baseName: String): List<String> = listOf("properties")
    override fun getFallbackLocale(baseName: String, locale: Locale): Locale? = when (locale) {
        Locale.ROOT -> null
        else -> Locale.ROOT
    }

    override fun newBundle(
        baseName: String,
        locale: Locale,
        format: String,
        loader: ClassLoader,
        reload: Boolean
    ): ResourceBundle? {
        if (format != "properties") return null
        val bundleName = toBundleName(baseName, locale)
        val resourceName = toResourceName(bundleName, format)
        var stream: InputStream? = null
        if (reload) {
            val url = loader.getResource(resourceName)
            if (url != null) {
                val urlConnection = url.openConnection()
                if (urlConnection != null) {
                    urlConnection.useCaches = false
                    stream = urlConnection.getInputStream()
                }
            }
        } else {
            stream = loader.getResourceAsStream(resourceName)
        }
        return stream?.use {
            PropertyResourceBundle(it, charset)
        }
    }

    companion object {
        val Default = FlexibleControl()
    }
}

private class PropertyResourceBundle : ResourceBundle {
    private val lookup: Map<String, Any?>
    constructor(stream: InputStream, charset: Charset = Charsets.UTF_8) : this(InputStreamReader(stream, charset))
    constructor(reader: Reader) {
        val props = Properties()
        props.load(reader)
        @Suppress("UNCHECKED_CAST")
        lookup = HashMap(props as Map<String, Any?>)
    }
    constructor() {
        lookup = HashMap()
    }

    override fun hashCode(): Int {
        return lookup.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return other is PropertyResourceBundle && lookup == other.lookup
    }

    override fun handleGetObject(key: String): Any? = lookup[key]
    override fun handleKeySet(): Set<String> = lookup.keys
    override fun getKeys(): Enumeration<String> = Collections.enumeration(handleKeySet())
}