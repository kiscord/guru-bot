package kitty.backend

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.plugins.callloging.*
import io.ktor.server.plugins.compression.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.defaultheaders.*
import io.ktor.server.plugins.forwardedheaders.*
import io.ktor.server.plugins.partialcontent.*
import io.ktor.server.plugins.requestvalidation.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kitty.*
import kitty.backend.auth.*
import kotlinx.serialization.modules.*
import org.kodein.di.*
import org.kodein.di.ktor.*
import org.kodein.di.ktor.controller.*
import org.slf4j.event.*

class CommonKtorConfigurator(di: DI) : AbstractKtorConfigurator(di) {
    private val customSerializerModule by instance<SerializersModule>()
    private val authProviders: Set<AuthProvider> by instance()
    private val validationControllers: Set<ValidationController> by instance()
    private val globalRoutes: Set<DIController> by instance(tag = "global-routes")
    private val contentNegotiationConfigurers: Set<ContentNegotiationConfigurer> by instance()

    override fun Application.configure() {
        install(DefaultHeaders)
        install(PartialContent)
        install(ForwardedHeaders)
        install(XForwardedHeaders)
        install(CallLogging) {
            level = Level.INFO
            filter { it.request.path() != "/healthz" }
        }
        install(Resources) {
            serializersModule += customSerializerModule
        }
        install(Authentication) {
            jwt(di, "auth-kitty")

            authProviders.forEach {
                with(it) {
                    install()
                }
            }
        }
        install(StatusPages) {
            exception<UnauthorizedException> { call, _ ->
                call.respond(HttpStatusCode.Unauthorized)
            }
            exception<ForbiddenException> { call, _ ->
                call.respond(HttpStatusCode.Forbidden)
            }
            exception<RequestValidationException> { call, cause ->
                val errors = cause.reasons.map { reason ->
                    val i = reason.indexOf(EntityValidator.CODE_DELIMITER)
                    if (i < 0) return@map Error(message = reason)
                    val code = reason.substring(0, i)
                    val message = reason.substring(i + EntityValidator.CODE_DELIMITER.length)
                    Error(code = code, message = message)
                }
                call.respond(HttpStatusCode.BadRequest, ErrorResponse(errors = errors))
            }
        }
        install(RequestValidation) {
            validationControllers.forEach { controller ->
                with(controller) {
                    install()
                }
            }
        }

        install(Compression) {
            gzip {
                priority = 0.9
            }
            deflate {
                priority = 1.0
                minimumSize(1024)
            }
        }

        environment.monitor.subscribe(ApplicationStopping) {
            closestDI().close()
        }

        environment.monitor.subscribe(ApplicationStopped) {
            closestDI().closeLastResort()
        }

        routing {
            install(ContentNegotiation) {
                accept(AcceptOnlyPlugin)
                checkAcceptHeaderCompliance = true

                contentNegotiationConfigurers.forEach { configurer ->
                    with(configurer) {
                        configure()
                    }
                }
            }

            globalRoutes.forEach { controller ->
                controller { controller }
            }
        }
    }
}
