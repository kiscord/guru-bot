package kitty.backend

import kitty.*
import kitty.backend.l10n.*
import kiscord.*
import kiscord.api.*
import kiscord.interactions.*
import org.kodein.di.*
import kitty.BuildConfig as DarkKittyBuildConfig

class InfoInteractions(di: DI) : AbstractInteractionsController(di) {
    private val config: KittyConfig by instance()

    override fun InteractionsHost.Builder.install() {
        with(L10n.Bundle("kitty/backend/l10n/info")) {
            info()
        }
    }

    context(L10n) private fun InteractionsHost.Builder.info() {
        command {
            localize("command.info.name", "command.info.description")
            dmPermission(true)
        } handle {
            val l10n = interaction.l10n()

            message {
                embed {
                    title(KittyBuildConfig.NAME)
                    url(config.publicPath)
                    color(0x7289da)

                    field(l10n["command.info.fields.kitty_version"], DarkKittyBuildConfig.VERSION, inline = true)
                    field(l10n["command.info.fields.kiscord_version"], Kiscord.VERSION, inline = true)
                    field(l10n["command.info.fields.kotlin_version"], KotlinVersion.CURRENT.toString(), inline = true)

                    author {
                        name("Prototik")
                        url(Kiscord.URL)
                    }
                }
            }
        }
    }
}
