package kitty.backend

import kitty.backend.auth.*
import kitty.backend.db.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.util.pipeline.*
import kiscord.api.*
import kotlinx.coroutines.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.*
import org.kodein.di.*
import org.kodein.di.ktor.*

inline fun <reified T : Principal> PipelineContext<Unit, ApplicationCall>.authorize(
    block: PipelineContext<Unit, ApplicationCall>.(T) -> Unit = {}
): T {
    val principal = call.principal<T>() ?: throw UnauthorizedException()
    block.invoke(this, principal)
    return principal
}

inline fun PipelineContext<Unit, ApplicationCall>.require(block: DIAware.() -> Boolean) {
    if (!block(call.closestDI())) throw UnauthorizedException()
}

context(DIAware)
@OptIn(KiscordUnstableAPI::class)
suspend fun KittyPrincipal.hasGuildPermissions(guildId: Snowflake, vararg permissions: Permission): Boolean {
    val database: Database by instance()
    val permissionMask = permissions.fold(0UL) { acc, permission ->
        acc or Permission.enumToValue(permission)
    }
    return newSuspendedTransaction(Dispatchers.IO, database) {
        DiscordGuildMembers.select {
            (DiscordGuildMembers.user eq discordUserId) and
                    (DiscordGuildMembers.guild eq guildId) and
                    (DiscordGuildMembers.owner or DiscordGuildMembers.permissions.bitwiseAnd(permissionMask).neq(0UL))
        }.count() > 0
    }
}

context(DIAware)
suspend fun KittyPrincipal.hasGuildManagementPermission(guildId: Snowflake): Boolean {
    return hasGuildPermissions(guildId, Permission.ManageGuild) || hasGuildPermissions(guildId, Permission.BanMembers)
}
