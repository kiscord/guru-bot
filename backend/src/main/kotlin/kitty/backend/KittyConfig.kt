package kitty.backend

import io.ktor.http.*
import io.ktor.server.config.*
import kiscord.api.*
import okio.*
import okio.Path.Companion.toPath
import kotlin.time.*

data class KittyConfig(
    val clientId: Snowflake,
    val clientSecret: String,
    val token: String?,
    val mode: Mode,
    val hmacSecret: String,
    val publicPath: Url,
    val database: Database,
    val kubernetes: Kubernetes,
) {
    constructor(config: ApplicationConfig) : this(
        clientId = Snowflake(config.property("clientId").getString()),
        clientSecret = config.property("clientSecret").getString(),
        token = config.propertyOrNull("token")?.getString(),
        mode = when (val mode = config.property("mode").getString()) {
            "bot" -> Mode.Bot
            "server" -> Mode.Server
            "mixed" -> Mode.Mixed
            else -> throw IllegalArgumentException("Unknown mode $mode, should be either server ot bot")
        },
        hmacSecret = config.property("hmacSecret").getString(),
        publicPath = Url(config.property("publicPath").getString()),
        database = Database(config.config("database")),
        kubernetes = Kubernetes(config.config("kubernetes")),
    )

    data class Database(
        val url: String,
        val username: String,
        val password: String,
        val baselineVersion: String? = null,
    ) {
        constructor(config: ApplicationConfig) : this(
            url = config.property("url").getString(),
            username = config.propertyOrNull("username")?.getString() ?: "",
            password = config.propertyOrNull("password")?.getString() ?: "",
            baselineVersion = config.propertyOrNull("baselineVersion")?.getString(),
        )
    }

    data class Kubernetes(
        val enabled: Boolean,
        val kubeConfig: Path?,
        val namespace: String?,
        val leaseName: String?,
        val identity: String?,
        val leaseDuration: Duration?,
        val renewDeadline: Duration?,
        val retryPeriod: Duration?,
    ) {
        constructor(config: ApplicationConfig) : this(
            enabled = config.propertyOrNull("enabled")?.getString()?.toBoolean() ?: false,
            kubeConfig = config.propertyOrNull("kubeConfig")?.getString()?.toPath(),
            namespace = config.propertyOrNull("namespace")?.getString(),
            leaseName = config.propertyOrNull("leaseName")?.getString(),
            identity = config.propertyOrNull("identity")?.getString(),
            leaseDuration = config.propertyOrNull("leaseDuration")?.getString()?.let(Duration::parse),
            renewDeadline = config.propertyOrNull("renewDeadline")?.getString()?.let(Duration::parse),
            retryPeriod = config.propertyOrNull("retryPeriod")?.getString()?.let(Duration::parse),
        )
    }

    enum class Mode(val isBot: Boolean = false, val isServer: Boolean = false) {
        Bot(isBot = true),
        Server(isServer = true),
        Mixed(isBot = true, isServer = true),
    }
}
