package kitty.backend

import com.zaxxer.hikari.*
import io.ktor.client.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.config.*
import kiscord.*
import kiscord.api.*
import kiscord.api.gateway.*
import kiscord.builder.*
import kiscord.gateway.*
import kiscord.gateway.interactions.*
import kiscord.interactions.*
import kiscord.ktor.*
import kitty.*
import kitty.backend.auth.*
import kitty.backend.db.*
import kitty.backend.leader.*
import kitty.backend.modules.faq.*
import kitty.backend.modules.pasta.*
import kitty.backend.modules.permissions.*
import kitty.backend.routes.*
import kotlinx.coroutines.*
import kotlinx.serialization.json.*
import org.flywaydb.core.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.*
import org.kodein.di.*
import org.kodein.di.ktor.controller.*
import org.kodein.log.*
import javax.sql.*
import io.ktor.server.application.Application as KtorApplication
import kitty.BuildConfig as DarkKittyBuildConfig

val KittyModule = DI.Module("kitty-backend") {
    importOnce(KittyCommonModule)
    importOnce(DatabaseModule)

    bindSingleton {
        val contentNegotiationConfigurers: Set<ContentNegotiationConfigurer> = instance()

        HttpClient {
            followRedirects = false

            install(KiscordBranding)
            install(ContentNegotiation) {
                contentNegotiationConfigurers.forEach { configurer ->
                    with(configurer) {
                        configure()
                    }
                }
            }
        }
    }

    bindProvider {
        instance<KtorApplication>().environment
    }

    bindProvider {
        instance<ApplicationEnvironment>().config
    }

    bindSingleton {
        jwtConfig(instance<ApplicationConfig>().config("jwt"))
    }

    bindSingleton(tag = "background-scope") { instance<KtorApplication>() + SupervisorJob() + Dispatchers.Default }
    inBindSet<AutoCloseable>(tag = "auto-closeable") {
        add {
            singleton {
                val scope: CoroutineScope = instance(tag = "background-scope")
                AutoCloseable { scope.cancel() }
            }
        }
    }

    bindSingleton {
        val config = instance<KittyConfig>()

        HikariConfig().apply {
            jdbcUrl = config.database.url
            username = config.database.username
            password = config.database.password
        }
    }

    bindSingleton<DataSource> {
        HikariDataSource(instance())
    }

    bindSet<InteractionsController>()
    inBindSet<InteractionsController> { add { singleton { InfoInteractions(di) } } }

    bindSet<DIController>(tag = "global-routes")
    inBindSet<DIController>(tag = "global-routes") {
        add { singleton { GlobalRoutes(instance()) } }
        add { singleton { InteractionsRoutes(instance()) } }
        add { singleton { ApiRoutes(instance()) } }
        add { singleton { OAuth2Routes(instance()) } }
    }

    bindSet<AuthProvider>()
    inBindSet<AuthProvider> {
        add { singleton { DiscordAuthProvider(di, "discord") } }
    }

    bindSet<KtorConfigurator>()
    inBindSet<KtorConfigurator> { add { singleton { CommonKtorConfigurator(di) } } }

    bindSet<ValidationController>()
    bindProvider<LeaderElection> { LeaderElection.Single }

    @Suppress("RemoveExplicitTypeArguments")
    inBindSet<Table>(tag = "tables-in-use") {
        add { provider { DiscordUsers } }
        add { provider { DiscordGuilds } }
        add { provider { DiscordGuildMembers } }
    }

    if (DarkKittyBuildConfig.MODULE_FAQ) importOnce(FaqModule)
    if (DarkKittyBuildConfig.MODULE_PASTA) importOnce(PastaModule)
    if (DarkKittyBuildConfig.MODULE_PERMISSIONS) importOnce(PermissionsModule)

    bindSingleton {
        val darkKittyConfig: KittyConfig = instance()
        Kiscord {
            fun commonGateway() {
                maybeInstall(Gateway)

                presence {
                    activity {
                        listening("${KittyBuildConfig.NAME} / ${KittyBuildConfig.VERSION}")
                    }
                }
            }

            when (darkKittyConfig.mode) {
                KittyConfig.Mode.Bot -> {
                    token(
                        Token.bot(
                            darkKittyConfig.token
                                ?: throw IllegalArgumentException("Bot token required to launch in bot mode")
                        )
                    )

                    gatewayInteractions {
                        kitty(di)
                    }

                    commonGateway()
                }

                KittyConfig.Mode.Server -> {
                    token(
                        Token.clientCredentials(
                            darkKittyConfig.clientId,
                            darkKittyConfig.clientSecret,
                            "applications.commands.update"
                        )
                    )
                }

                KittyConfig.Mode.Mixed -> {
                    token(
                        Token.bot(
                            darkKittyConfig.token
                                ?: throw IllegalArgumentException("Bot token required to launch in bot mode")
                        )
                    )

                    commonGateway()
                }
            }
        }
    }

    onReady {
        instance<Flyway>().migrate()
        val database = instance<Database>()

        val tablesInUse = instance<Set<Table>>(tag = "tables-in-use")
        val logger = instance<LoggerFactory>().newLogger("kitty.backend", "ActualizeDB")

        val statements = transaction(db = database) {
            SchemaUtils.statementsRequiredToActualizeScheme(*tablesInUse.toTypedArray(), withLogs = false)
        }

        if (statements.isNotEmpty()) {
            logger.warning { "SQL required to actualize a schema!" }
            statements.forEach { statement ->
                logger.warning { statement }
            }
        }
    }

    onReady {
        val kiscord: Kiscord = instance()
        val gateway = kiscord.featureOrNull(Gateway)
        if (gateway != null) {
            val leaderElection: LeaderElection = instance()
            val scope: CoroutineScope = instance(tag = "background-scope")
            val logger = instance<LoggerFactory>().newLogger("kitty.backend", "KittyModule")
            scope.launch {
                leaderElection.status.collect { status ->
                    when (status) {
                        LeaderElection.Status.Leader -> {
                            logger.debug { "Connecting to the gateway in progress..." }
                            gateway.connect(waitForReady = true, join = false)
                            logger.debug { "Connection to the gateway successfully established!" }
                        }

                        LeaderElection.Status.Follower -> {
                            logger.debug { "Closing gateway connection" }
                            gateway.close()
                            logger.debug { "Gateway connection closed" }
                        }
                    }
                }
            }
        }
    }
}

fun InteractionsHost.Builder.kitty(di: DI) {
    val controllers: Set<InteractionsController> by di.instance()

    controllers.forEach { controller ->
        with(controller) {
            install()
        }
    }
}
