package kitty.backend

import io.ktor.server.application.*
import io.ktor.server.cio.*
import io.ktor.server.config.*
import io.ktor.server.engine.*
import kotlinx.coroutines.*
import org.kodein.di.*
import org.kodein.di.ktor.*
import org.slf4j.bridge.*
import io.ktor.server.application.Application as KtorApplication

object Kitty {
    init {
        SLF4JBridgeHandler.removeHandlersForRootLogger()
        SLF4JBridgeHandler.install()
    }

    @JvmStatic
    fun main(args: Array<String>) {
        EngineMain.main(args)
    }
}

@Suppress("unused")
fun KtorApplication.kitty() {
    val kittyConfig = KittyConfig(environment.config.config("kitty"))

    di {
        importOnce(KittyModule)

        bindProvider { kittyConfig }
        if (kittyConfig.kubernetes.enabled) {
            importOnce(KubernetesModule, allowOverride = true)
        }
    }

    val configurators: Set<KtorConfigurator> by closestDI().instance()

    configurators.forEach { configurator ->
        with(configurator) {
            configure()
        }
    }
}
