package kitty.backend

import io.ktor.server.plugins.requestvalidation.*
import org.kodein.di.*
import kotlin.reflect.*

interface ValidationController : DIAware {
    fun RequestValidationConfig.install()
}

abstract class AbstractValidationController(override val di: DI) : ValidationController

interface EntityValidator {
    val result: ValidationResult

    fun invalid(code: String, message: String) = invalid("${code}${CODE_DELIMITER}${message}")
    fun invalid(reason: String)
    fun invalid(vararg reasons: String)
    fun invalid(reasons: Collection<String>)

    companion object {
        const val CODE_DELIMITER = "|||"
    }
}

@PublishedApi
internal class EntityValidatorImpl : EntityValidator {
    private val reasons: MutableList<String> by lazy { mutableListOf() }

    override val result: ValidationResult
        get() = when {
            reasons.isNotEmpty() -> ValidationResult.Invalid(reasons)
            else -> ValidationResult.Valid
        }

    override fun invalid(reason: String) {
        this.reasons += reason
    }

    override fun invalid(reasons: Collection<String>) {
        this.reasons += reasons
    }

    override fun invalid(vararg reasons: String) {
        this.reasons += reasons
    }
}

inline fun <T : Any> RequestValidationConfig.validateEntity(
    kClass: KClass<T>,
    crossinline block: suspend EntityValidator.(T) -> Unit
) = validate(object : Validator {
    override fun filter(value: Any) = kClass.isInstance(value)

    @Suppress("UNCHECKED_CAST")
    override suspend fun validate(value: Any): ValidationResult {
        val entityValidator = EntityValidatorImpl()
        block(entityValidator, value as T)
        return entityValidator.result
    }
})

inline fun <reified T : Any> RequestValidationConfig.validateEntity(crossinline block: suspend EntityValidator.(T) -> Unit) {
    validateEntity(T::class, block)
}