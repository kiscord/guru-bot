package kitty.backend

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.util.*

object AcceptOnlyPlugin : AcceptHeaderContributor {
    override fun invoke(
        call: ApplicationCall,
        acceptedContentTypes: List<ContentTypeWithQuality>
    ): List<ContentTypeWithQuality> {
        val valid = call.attributes.takeOrNull(Key) ?: return acceptedContentTypes
        return acceptedContentTypes.flatMap { candidate ->
            if (valid.none { it.match(candidate.contentType) }) return@flatMap emptyList()
            if (candidate.contentType.contentSubtype == "*" || candidate.contentType.contentType == "*") {
                return@flatMap valid.filter {
                    it.match(candidate.contentType) && it.contentType != "*" && it.contentSubtype != "*"
                }.map {
                    ContentTypeWithQuality(it, candidate.quality)
                }
            }
            listOf(candidate)
        }
    }

    internal val Key = AttributeKey<Collection<ContentType>>("AcceptOnlyPlugin")
}

fun ApplicationCall.acceptOnly(contentTypes: Collection<ContentType>) {
    attributes.put(AcceptOnlyPlugin.Key, contentTypes)
}

fun ApplicationCall.acceptOnly(contentType: ContentType) = acceptOnly(listOf(contentType))
fun ApplicationCall.acceptOnly(vararg contentTypes: ContentType) = acceptOnly(contentTypes.asList())