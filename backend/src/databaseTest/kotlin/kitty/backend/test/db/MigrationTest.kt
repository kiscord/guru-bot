package kitty.backend.test.db

import org.flywaydb.core.*
import org.kodein.di.*

class MigrationTest : DatabaseTest() {
    @TestAllDBs
    fun `migrations are applying on fresh database`(di: DI) {
        val flyway by di.instance<Flyway>()
        val result = flyway.migrate()
        assert(result.success) { "Migrations cannot be applied" }
    }
}
