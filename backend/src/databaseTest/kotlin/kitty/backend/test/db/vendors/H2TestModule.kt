package kitty.backend.test.db.vendors

import com.zaxxer.hikari.*
import kitty.backend.test.db.*
import org.kodein.di.*
import javax.sql.*

val H2TestModule = DI.Module("kitty-database-test-h2") {
    importOnce(DatabaseTestModule)
    bindSingleton {
        val config = HikariConfig()
        config.driverClassName = "org.h2.Driver"
        config.jdbcUrl = "jdbc:h2:mem:"
        HikariDataSource(config)
    }
    delegate<DataSource>().to<HikariDataSource>()

    inBindSet<AutoCloseable>(tag = "auto-closeable") {
        add { provider { instance<HikariDataSource>() } }
    }
}
