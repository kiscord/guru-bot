package kitty.backend.test.db

import kitty.*
import kitty.backend.test.db.vendors.*
import kitty.test.*
import org.junit.jupiter.api.*
import org.junit.jupiter.params.*
import org.junit.jupiter.params.aggregator.*
import org.junit.jupiter.params.provider.*
import org.kodein.di.*
import java.util.stream.*
import kotlin.streams.*

abstract class DatabaseTest {
    @ParameterizedTest(name = "{displayName} [{0}]")
    @MethodSource("dbs")
    @Retention(AnnotationRetention.RUNTIME)
    @Target(AnnotationTarget.FUNCTION)
    annotation class TestAllDBs

    companion object {
        private val diModules = sequenceOf(
            // Do in-memory DBs first
            H2TestModule to "h2db",
            // Then testcontainers
            PostgresTestModule to "postgres",
        )

        @JvmStatic
        @Suppress("unused")
        fun dbs(): Iterable<DI> = diModules.map { (module, name) ->
            AutoCloseableDI {
                importOnce(module)
            } named name
        }.asIterable()
    }
}
