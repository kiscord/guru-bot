package kitty.backend.test.db.vendors

import com.zaxxer.hikari.*
import kitty.*
import kitty.backend.test.db.*
import org.kodein.di.*
import javax.sql.*

val PostgresTestModule = DI.Module("kitty-database-test-postgres") {
    importOnce(DatabaseTestModule)
    bindConstant(tag = "kitty-database-test-postgres-version") { "14-alpine" }
    bindSingleton {
        val version = instance<String>(tag = "kitty-database-test-postgres-version")
        val databaseName = instance<String>(tag = "kitty-database-test-database-name")
        val config = HikariConfig()
        config.driverClassName = "org.testcontainers.jdbc.ContainerDatabaseDriver"
        config.jdbcUrl = "jdbc:tc:postgresql:${version}:///${databaseName}"
        HikariDataSource(config)
    }
    delegate<DataSource>().to<HikariDataSource>()

    inBindSet<AutoCloseable>(tag = "auto-closeable") {
        add { provider { instance<HikariDataSource>() } }
    }
}
