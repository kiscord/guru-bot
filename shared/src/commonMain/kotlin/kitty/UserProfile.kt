package kitty

import kiscord.api.*
import kotlinx.serialization.*

@Serializable
@OptIn(KiscordUnstableAPI::class)
data class UserProfile(
    val id: Snowflake,
    val username: String,
    val discriminator: String,
    val avatar: String? = null,
    val mfaEnabled: Boolean? = null,
    val verified: Boolean? = null,
    @Serializable(with = User.Flag.BitmaskSerializer::class)
    val flags: Set<User.Flag>? = null,
    val bot: Boolean? = null,
)

fun UserProfile.avatarUrl(size: Int = 32): String = if (avatar != null) {
    val format = if (avatar.startsWith("a_")) "gif" else "png"
    "https://cdn.discordapp.com/avatars/${id}/${avatar}.${format}?size=$size"
} else {
    val modulo = discriminator.trimStart('0').toInt() % 5
    "https://cdn.discordapp.com/embed/avatars/${modulo}.png"
}