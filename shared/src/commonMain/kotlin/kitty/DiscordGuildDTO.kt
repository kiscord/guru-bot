package kitty

import io.ktor.http.*
import kiscord.api.*
import kotlinx.serialization.*

@Serializable
data class DiscordGuildDTO(
    val id: Snowflake,
    val name: String,
    val icon: String?,
    val owner: Boolean,
    val permissions: PermissionSet,
    val managed: Boolean,
    val allowedToManage: Boolean,
)

val DiscordGuildDTO.iconUrl: Url?
    get() {
        if (icon == null) return null
        return Url("https://cdn.discordapp.com/icons/$id/$icon.png")
    }