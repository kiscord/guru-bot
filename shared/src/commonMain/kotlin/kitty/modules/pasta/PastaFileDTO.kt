package kitty.modules.pasta

import kitty.*
import kotlinx.serialization.*

@Serializable
data class PastaFileDTO(
    val id: HashId,
    val sha3_256_hash: String,
    val author: UserProfile?,
)
