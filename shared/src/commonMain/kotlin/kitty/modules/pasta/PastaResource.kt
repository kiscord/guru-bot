@file:OptIn(KiscordUnstableAPI::class)
@file:UseSerializers(UrlSerializer::class)

package kitty.modules.pasta

import io.ktor.http.*
import io.ktor.resources.*
import kiscord.api.*
import kiscord.serialization.*
import kitty.*
import kitty.resources.*
import kotlinx.serialization.*

@Serializable
@Resource("/pasta")
class PastaResource(
    override val parent: ApiResource = ApiResource(),
) : HasParentResource {
    @Serializable
    @Resource("/upload")
    class Upload(
        override val parent: PastaResource = PastaResource(),
    ) : HasParentResource

    @Serializable
    @Resource("/{id}")
    class Id(
        override val parent: PastaResource = PastaResource(),
        val id: HashId
    ) : HasParentResource {
        @Serializable
        @Resource("/raw")
        class Raw(
            override val parent: Id,
        ) : HasParentResource {
            constructor(id: HashId) : this(Id(id = id))

            val id: HashId get() = parent.id
        }
    }
}
