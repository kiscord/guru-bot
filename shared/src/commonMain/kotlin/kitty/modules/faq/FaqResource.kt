package kitty.modules.faq

import kitty.resources.*
import io.ktor.resources.*
import kiscord.api.*
import kotlinx.serialization.*
import org.kodein.di.*

@Serializable
@Resource("/faq")
data class FaqResource(override val parent: GuildScopedResource) : HasParentResource, ModuleScoped {
    constructor(guildId: Snowflake) : this(GuildScopedResource(guildId = guildId))

    override val moduleName: ModuleScoped.Name get() = FaqModuleName

    interface BookScoped {
        val bookId: Long
    }

    interface PageScoped {
        val pageId: Long
    }

    @Serializable
    @Resource("/books")
    data class Books(
        override val parent: FaqResource
    ) : HasParentResource {
        constructor(guildId: Snowflake) : this(FaqResource(guildId = guildId))

        @Serializable
        @Resource("/{bookId}")
        data class Id(
            override val parent: Books,
            override val bookId: Long,
        ) : HasParentResource, BookScoped {
            constructor(guildId: Snowflake, bookId: Long) : this(Books(guildId = guildId), bookId = bookId)

            @Serializable
            @Resource("/pages")
            data class Pages(
                override val parent: Id,
            ) : HasParentResource {
                constructor(guildId: Snowflake, bookId: Long) : this(Id(guildId = guildId, bookId = bookId))
            }
        }
    }

    @Serializable
    @Resource("/pages")
    data class Pages(
        override val parent: FaqResource
    ) : HasParentResource {
        constructor(guildId: Snowflake) : this(FaqResource(guildId = guildId))

        @Serializable
        @Resource("/{pageId}")
        data class Id(
            override val parent: Pages,
            override val pageId: Long,
        ) : HasParentResource, PageScoped {
            constructor(guildId: Snowflake, pageId: Long) : this(Pages(guildId = guildId), pageId = pageId)
        }
    }
}