package kitty.modules.faq

import kiscord.api.*
import kotlinx.datetime.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class FaqBookDTO(
    @EncodeDefault(EncodeDefault.Mode.NEVER)
    val id: Long? = null,
    val name: String,
    @EncodeDefault(EncodeDefault.Mode.NEVER)
    val guildId: Snowflake = Snowflake(0UL),
    @EncodeDefault(EncodeDefault.Mode.NEVER)
    val author: Snowflake = Snowflake(0UL),
    @JsonNames("entries")
    @EncodeDefault(EncodeDefault.Mode.NEVER)
    val pages: Collection<FaqPageDTO> = emptyList(),
    @EncodeDefault(EncodeDefault.Mode.NEVER)
    val createdAt: Instant? = null,
)

fun FaqBookDTO.sanitize(): FaqBookDTO = copy(
    id = null,
    name = name.trim(),
    guildId = Snowflake(0UL),
    pages = pages.map { it.sanitize() }
)