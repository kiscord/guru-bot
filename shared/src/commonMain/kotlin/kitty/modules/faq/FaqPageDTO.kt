package kitty.modules.faq

import kiscord.api.*
import kotlinx.serialization.*

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class FaqPageDTO(
    @EncodeDefault(EncodeDefault.Mode.NEVER)
    val id: Long? = null,
    val text: String,
    val embed: Embed,
    val aliases: Collection<String>,
)

fun FaqPageDTO.sanitize(): FaqPageDTO = copy(
    id = null,
    text = text.trim(),
    aliases = aliases.distinct(),
)