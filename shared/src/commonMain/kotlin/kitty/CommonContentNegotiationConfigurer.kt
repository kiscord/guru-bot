package kitty

import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.*
import io.ktor.serialization.kotlinx.cbor.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.*
import kotlinx.serialization.cbor.*
import kotlinx.serialization.json.*
import org.kodein.di.*

@OptIn(ExperimentalSerializationApi::class)
class CommonContentNegotiationConfigurer(di: DI) : AbstractContentNegotiationConfigurer(di) {
    private val cbor: Cbor by instance()
    private val json: Json by instance()

    override fun Configuration.configure() {
        if (!BuildConfig.DISABLE_CBOR) {
            cbor(cbor)
        }
        json(json)
    }
}
