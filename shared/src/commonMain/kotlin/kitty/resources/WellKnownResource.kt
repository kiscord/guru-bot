package kitty.resources

import io.ktor.resources.*
import kotlinx.serialization.*

@Serializable
@Resource("/.well-known")
object WellKnownResource {
    @Serializable
    @Resource("/jwks.json")
    data class JwksJson(
        override val parent: WellKnownResource = WellKnownResource
    ) : HasParentResource
}