package kitty.rich_text

import io.ktor.utils.io.*
import kotlinx.coroutines.flow.*

class RichTextSequencer<Base : Any>(private val emitter: RichTextEmitter<Base>) {
    fun parse(channel: ByteReadChannel) = flow {
        var state = State.Empty
        val textBuffer = StringBuilder()
        var elements: MutableList<Base> = ArrayList(1)
        loop@ while (true) {
            val line = channel.readUTF8Line() ?: break
            var lineStart = 0
            var escStart = line.indexOf(ESC)
            parser@ do {
                if (escStart < 0) {
                    if (elements.isEmpty() && lineStart == 0) {
                        // short-circuit if line isn't contains any ESC sequences
                        emit(produce(state, line))
                        continue@loop
                    }
                    textBuffer.appendRange(line, lineStart, line.length)
                    break
                }

                if (escStart > lineStart) {
                    textBuffer.appendRange(line, lineStart, escStart)
                    elements.add(produce(state, textBuffer.toString()))
                    textBuffer.setLength(0)
                }

                when (line[escStart + 1]) {
                    '[' -> {
                        // CSI
                        var idx = escStart + 2
                        val parametersBuffer = StringBuilder()

                        while (true) {
                            if (idx >= line.length) {
                                lineStart = escStart
                                escStart = line.indexOf(ESC, escStart + 1)
                                continue@parser
                            }
                            val c = line[idx]
                            if (c !in CSI_PARAMETER_RANGE) break
                            parametersBuffer.append(c)
                            idx++
                        }

                        val intermediateBuffer = StringBuilder()

                        while (true) {
                            if (idx >= line.length) {
                                lineStart = escStart
                                escStart = line.indexOf(ESC, escStart + 1)
                                continue@parser
                            }
                            val c = line[idx]
                            if (c !in CSI_INTERMEDIATE_RANGE) break
                            intermediateBuffer.append(c)
                            idx++
                        }

                        val terminal: Char = line[idx]

                        if (terminal !in CSI_TERMINAL_RANGE) {
                            lineStart = escStart
                            escStart = line.indexOf(ESC, escStart + 1)
                            continue@parser
                        }

                        lineStart = idx + 1
                        escStart = line.indexOf(ESC, lineStart)

                        when (terminal) {
                            'm' -> {
                                // SGR
                                val parameters = parametersBuffer.split(';')
                                if (parameters.isEmpty()) {
                                    state = State.Empty
                                    continue@parser
                                }
                                val firstParam = parameters[0].toIntOrNull() ?: continue@parser
                                when (firstParam) {
                                    0 -> {
                                        // SGR reset
                                        state = State.Empty
                                    }

                                    1 -> {
                                        // SGR bold
                                        state = state.copy(intensity = State.Intensity.Bold)
                                    }

                                    2 -> {
                                        // SGR faint
                                        state = state.copy(intensity = State.Intensity.Faint)
                                    }

                                    3 -> {
                                        // SGR italic
                                        state = state.copy(italic = true)
                                    }

                                    4 -> {
                                        // SGR underline
                                        state = state.copy(underline = true)
                                    }

                                    7 -> {
                                        // SGR reverse video
                                        state = state.copy(
                                            foregroundColor = state.backgroundColor,
                                            backgroundColor = state.foregroundColor,
                                        )
                                    }

                                    22 -> {
                                        // SGR normal intensity
                                        state = state.copy(intensity = State.Intensity.Normal)
                                    }

                                    23 -> {
                                        state = state.copy(italic = false)
                                    }

                                    24 -> {
                                        state = state.copy(underline = false)
                                    }

                                    in 30..37 -> {
                                        // SGR set foreground color
                                        state = state.copy(
                                            foregroundColor = RichTextColor.Palette((firstParam - 30).toUByte())
                                        )
                                    }

                                    38 -> {
                                        // SGR set foreground color
                                        state = state.copy(
                                            foregroundColor = parseSGRColor(parameters)
                                        )
                                    }

                                    39 -> {
                                        // SGR set default foreground color
                                        state = state.copy(
                                            foregroundColor = RichTextColor.Default
                                        )
                                    }

                                    in 40..47 -> {
                                        // SGR set background color
                                        state = state.copy(
                                            backgroundColor = RichTextColor.Palette((firstParam - 30).toUByte())
                                        )
                                    }

                                    48 -> {
                                        // SGR set background color
                                        state = state.copy(
                                            backgroundColor = parseSGRColor(parameters)
                                        )
                                    }

                                    49 -> {
                                        // SGR set default background color
                                        state = state.copy(
                                            backgroundColor = RichTextColor.Default
                                        )
                                    }

                                    58 -> {
                                        // SGR set underline color
                                        state = state.copy(
                                            underlineColor = parseSGRColor(parameters)
                                        )
                                    }

                                    59 -> {
                                        // SGR default underline color
                                        state = state.copy(
                                            underlineColor = RichTextColor.Default
                                        )
                                    }

                                    in 90..97 -> {
                                        // SGR set bright foreground color
                                        state = state.copy(
                                            foregroundColor = RichTextColor.Palette((firstParam - 90 + 8).toUByte())
                                        )
                                    }

                                    in 100..107 -> {
                                        // SGR set bright background color
                                        state = state.copy(
                                            backgroundColor = RichTextColor.Palette((firstParam - 100 + 8).toUByte())
                                        )
                                    }

                                    else -> {
                                        // unknown SGR sequence
                                    }
                                }
                            }

                            else -> {
                                // unknown CSI sequence
                            }
                        }

                        continue@parser
                    }

                    else -> {
                        // unknown ESC sequence, don't touch it at all
                    }
                }

                lineStart = escStart
                escStart = line.indexOf(ESC, escStart + 1)
            } while (true)
            if (textBuffer.isNotEmpty()) {
                elements += produce(state, textBuffer.toString())
                textBuffer.setLength(0)
            }
            when (elements.size) {
                0 -> emit(emitter.empty)
                1 -> {
                    emit(elements[0])
                    elements.clear()
                }

                else -> {
                    emit(emitter.join(elements))
                    elements = ArrayList(1)
                }
            }
        }
    }

    private fun produce(state: State, text: String): Base {

        var node = emitter.text(text)

        if (state.foregroundColor != RichTextColor.Default) {
            node = emitter.foregroundColor(state.foregroundColor, node)
        }
        if (state.backgroundColor != RichTextColor.Default) {
            node = emitter.backgroundColor(state.backgroundColor, node)
        }
        when (state.intensity) {
            State.Intensity.Bold -> {
                node = emitter.bold(node)
            }

            State.Intensity.Faint -> {
                node = emitter.faint(node)
            }

            State.Intensity.Normal -> {
                // noop
            }
        }
        if (state.italic) {
            node = emitter.italic(node)
        }
        if (state.underline) {
            node = emitter.underline(node)
        }
        if (state.underlineColor != RichTextColor.Default) {
            node = emitter.underlineColor(state.underlineColor, node)
        }

        return node
    }

    companion object {
        private const val ESC: Char = 0x1B.toChar()
        private val CSI_PARAMETER_RANGE = '\u0030'..'\u003f'
        private val CSI_INTERMEDIATE_RANGE = '\u0020'..'\u002f'
        private val CSI_TERMINAL_RANGE = '\u0040'..'\u007E'
    }

    private data class State(
        val foregroundColor: RichTextColor = RichTextColor.Default,
        val backgroundColor: RichTextColor = RichTextColor.Default,
        val intensity: Intensity = Intensity.Normal,
        val italic: Boolean = false,
        val underline: Boolean = false,
        val underlineColor: RichTextColor = RichTextColor.Default,
    ) {
        enum class Intensity {
            Faint,
            Normal,
            Bold
            ;
        }

        companion object {
            val Empty = State()
        }
    }

    private fun parseSGRColor(parameters: List<String>): RichTextColor {
        return when (parameters[1].toIntOrNull()) {
            2 -> {
                // RGB color
                val red = parameters.getOrNull(2)?.toUByteOrNull() ?: 0.toUByte()
                val green = parameters.getOrNull(3)?.toUByteOrNull() ?: 0.toUByte()
                val blue = parameters.getOrNull(4)?.toUByteOrNull() ?: 0.toUByte()
                RichTextColor.RGB(red, green, blue)
            }

            5 -> {
                // Palette color
                val color = parameters.getOrNull(2)?.toUByteOrNull() ?: 0.toUByte()
                RichTextColor.Palette(color)
            }

            else -> RichTextColor.Default
        }
    }
}
