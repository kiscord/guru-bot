package kitty.rich_text

import kotlin.jvm.*

sealed interface RichTextColor {
    object Default : RichTextColor {
        override fun toString() = "Default"
    }

    @JvmInline
    value class Palette(val index: UByte) : RichTextColor

    data class RGB(val color: UInt) : RichTextColor {
        constructor(red: UByte, green: UByte, blue: UByte) : this(
            (red.toUInt() shl 16) or (green.toUInt() shl 8) or blue.toUInt()
        )

        val red: UByte get() = (color shr 16 and 255u).toUByte()
        val green: UByte get() = (color shr 8 and 255u).toUByte()
        val blue: UByte get() = (color and 255u).toUByte()

        override fun toString() = "RGB(color=0x${color.toString(16).padStart(6, '0')})"
    }
}
