package kitty.rich_text

interface RichTextEmitter<Base : Any> {
    val empty: Base
    fun text(text: String): Base
    fun backgroundColor(color: RichTextColor, content: Base): Base
    fun foregroundColor(color: RichTextColor, content: Base): Base

    fun bold(content: Base): Base
    fun faint(content: Base): Base
    fun italic(content: Base): Base
    fun underline(content: Base): Base
    fun underlineColor(color: RichTextColor, content: Base): Base
    fun join(parts: Collection<Base>): Base

    operator fun Base.plus(another: Base): Base = join(listOf(this, another))
    operator fun Base.plus(another: String): Base = this + text(another)
}

fun <Base : Any> RichTextEmitter<Base>.join(vararg parts: Base): Base = join(parts.asList())
