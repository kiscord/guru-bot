package kitty

import io.ktor.serialization.*
import org.kodein.di.*

interface ContentNegotiationConfigurer : DIAware {
    fun Configuration.configure()
}

abstract class AbstractContentNegotiationConfigurer(override val di: DI) : ContentNegotiationConfigurer