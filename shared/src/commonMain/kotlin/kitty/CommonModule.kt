package kitty

import io.ktor.resources.serialization.*
import io.ktor.utils.io.core.*
import kiscord.*
import kotlinx.serialization.*
import kotlinx.serialization.cbor.*
import kotlinx.serialization.json.*
import kotlinx.serialization.modules.*
import org.kodein.di.*
import org.kodein.log.*

val KittyCommonModule = DI.Module("kitty-common") {
    bindSet<SerializersModule>()
    inBindSet<SerializersModule> { add { provider { KittySerializersModule } } }

    bindProvider {
        SerializersModule {
            instance<Set<SerializersModule>>().forEach {
                include(it)
            }
        }
    }

    bindSingleton {
        Json {
            encodeDefaults = false
            ignoreUnknownKeys = true
            useAlternativeNames = true
            serializersModule += instance()
        }
    }

    if (!BuildConfig.DISABLE_CBOR)
        @OptIn(ExperimentalSerializationApi::class)
        bindSingleton {
            Cbor {
                encodeDefaults = false
                ignoreUnknownKeys = true
                serializersModule += instance()
            }
        }

    bindSingleton(tag = "pretty-json") {
        Json(instance()) {
            prettyPrint = true
        }
    }

    bindSingleton {
        ResourcesFormat(instance())
    }

    bindSet<LogFrontend>()
    bindSet<LogFilter>()
    bindSet<LogMapper>()
    bindSingleton {
        val frontends = instance<Set<LogFrontend>>()
        val filters = instance<Set<LogFilter>>().toList()
        val mappers = instance<Set<LogMapper>>().toList()
        LoggerFactory(frontends, filters, mappers)
    }

    bindSet<ContentNegotiationConfigurer>()
    inBindSet<ContentNegotiationConfigurer> { add { singleton { CommonContentNegotiationConfigurer(di) } } }

    bindSet<AutoCloseable>("auto-closeable")
    bindSet<AutoCloseable>("auto-closeable-last-resort")

    commonModule()

    onReady {
        Kiscord.DefaultLoggerFactory = instance()
    }
}

internal expect fun DI.Builder.commonModule()
