package kitty

expect fun interface AutoCloseable {
    fun close()
}
