package kitty

object KittyBuildConfig {
    const val NAME: String = BuildConfig.NAME
    const val VERSION: String = BuildConfig.VERSION
    const val VCS_URL: String = BuildConfig.VCS_URL
}
