package kitty

import kotlinx.serialization.*
import kotlin.jvm.*

@Serializable
data class ErrorResponse(
    val errors: List<Error>
)

@Serializable
@OptIn(ExperimentalSerializationApi::class)
data class Error(
    @EncodeDefault(EncodeDefault.Mode.NEVER)
    val code: String? = null,
    val message: String,
)

