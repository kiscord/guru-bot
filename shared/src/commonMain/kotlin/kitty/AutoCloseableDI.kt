package kitty

import org.kodein.di.*

fun DI.close() {
    val closeables by instanceOrNull<Set<AutoCloseable>>(tag = "auto-closeable")
    closeables?.forEach(AutoCloseable::close)
}

fun DI.closeLastResort() {
    val closeables by instanceOrNull<Set<AutoCloseable>>(tag = "auto-closeable-last-resort")
    closeables?.forEach(AutoCloseable::close)
}
