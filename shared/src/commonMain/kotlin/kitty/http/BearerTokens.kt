package kitty.http

import io.ktor.client.plugins.auth.providers.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

object BearerTokensSerializer : KSerializer<BearerTokens> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("kiscord.darkkitty.BearerTokens") {
        element("access_token", String.serializer().descriptor)
        element("refresh_token", String.serializer().descriptor)
    }

    @OptIn(ExperimentalSerializationApi::class)
    @Suppress("INVISIBLE_MEMBER") // to be able to throw `UnknownFieldException`
    override fun deserialize(decoder: Decoder): BearerTokens =
        decoder.decodeStructure(descriptor) {
            var accessToken: String? = null
            var refreshToken: String? = null
            loop@ while (true) {
                when (val index = decodeElementIndex(descriptor)) {
                    CompositeDecoder.DECODE_DONE -> break@loop
                    0 -> accessToken = decodeStringElement(descriptor, index)
                    1 -> refreshToken = decodeStringElement(descriptor, index)
                    else -> throw UnknownFieldException(index)
                }
            }
            val missingFields = mutableListOf<String>()
            if (accessToken == null) missingFields += "access_token"
            if (refreshToken == null) missingFields += "refresh_token"
            if (missingFields.isNotEmpty()) throw MissingFieldException(missingFields, "darkkitty.BearerTokens")
            BearerTokens(
                accessToken = accessToken!!,
                refreshToken = refreshToken!!,
            )
        }

    override fun serialize(encoder: Encoder, value: BearerTokens) = encoder.encodeStructure(descriptor) {
        encodeStringElement(descriptor, 0, value.accessToken)
        encodeStringElement(descriptor, 1, value.refreshToken)
    }
}