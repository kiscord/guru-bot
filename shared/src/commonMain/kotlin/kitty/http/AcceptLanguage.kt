package kitty.http

import io.ktor.http.*

class AcceptLanguage(
    language: String, parameters: List<HeaderValueParam>
) : HeaderValueWithParameters(language, parameters) {
    constructor(language: String, quality: Float = 1.0f) : this(
        language,
        if (quality == 1.0f) emptyList() else listOf(HeaderValueParam("q", quality.toString()))
    )

    val quality: Float get() = parameter("q")?.toFloatOrNull() ?: 1.0f
}