package kitty

import kitty.http.*
import io.ktor.client.plugins.auth.providers.BearerTokens
import kotlinx.serialization.modules.*

val KittySerializersModule = SerializersModule {
    contextual(BearerTokens::class, BearerTokensSerializer)
}