package kitty

import ch.qos.logback.classic.*
import org.kodein.di.*
import org.kodein.log.*
import org.kodein.log.frontend.*
import org.slf4j.LoggerFactory as Slf4jLoggerFactory

internal actual fun DI.Builder.commonModule() {
    inBindSet<LogFrontend> { add { provider { slf4jFrontend } } }
    inBindSet<AutoCloseable>(tag = "auto-closeable-last-resort") {
        add {
            singleton {
                AutoCloseable {
                    val factory = Slf4jLoggerFactory.getILoggerFactory()
                    if (factory is LoggerContext) {
                        factory.stop()
                    }
                }
            }
        }
    }
}
