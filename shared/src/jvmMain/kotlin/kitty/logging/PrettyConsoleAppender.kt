package kitty.logging

import ch.qos.logback.core.*
import ch.qos.logback.core.joran.spi.*
import ch.qos.logback.core.status.*
import org.fusesource.jansi.*

class PrettyConsoleAppender<E> : OutputStreamAppender<E>() {
    private var _target = ConsoleTarget.SystemOut
    var target
        get() = _target.name
        set(value) {
            val t = ConsoleTarget.findByName(value.trim { it <= ' ' })
            if (t == null) {
                targetWarn(value)
            } else {
                _target = t
            }
        }
    var withJansi = false

    private fun targetWarn(`val`: String) {
        val status: Status = WarnStatus(
            "[" + `val` + "] should be one of " + ConsoleTarget.values().contentToString(),
            this
        )
        status.add(WarnStatus("Using previously set target, System.out by default.", this))
        addStatus(status)
    }

    override fun start() {
        outputStream = if (withJansi) {
            when (_target) {
                ConsoleTarget.SystemOut -> AnsiConsole.out()
                ConsoleTarget.SystemErr -> AnsiConsole.err()
            }
        } else _target.stream
        super.start()
    }
}
