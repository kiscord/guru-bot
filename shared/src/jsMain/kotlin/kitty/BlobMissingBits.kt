package kitty

import kotlinx.coroutines.*
import kotlinx.serialization.*
import org.khronos.webgl.*
import org.w3c.files.*
import kotlin.js.Promise

private external interface BlobMissingBits {
    fun text(): Promise<String>
    fun arrayBuffer(): Promise<ArrayBuffer>
}

suspend fun <T> StringFormat.decodeFromBlob(deserializer: DeserializationStrategy<T>, blob: Blob): T {
    val text = blob.unsafeCast<BlobMissingBits>().text().await()
    return decodeFromString(deserializer, text)
}

suspend inline fun <reified T> StringFormat.decodeFromBlob(blob: Blob): T = decodeFromBlob(serializer(), blob)

suspend fun <T> BinaryFormat.decodeFromBlob(deserializer: DeserializationStrategy<T>, blob: Blob): T {
    val arrayBuffer = blob.unsafeCast<BlobMissingBits>().arrayBuffer().await()
    val array = Int8Array(arrayBuffer).unsafeCast<ByteArray>()
    return decodeFromByteArray(deserializer, array)
}

suspend inline fun <reified T> BinaryFormat.decodeFromBlob(blob: Blob): T = decodeFromBlob(serializer(), blob)

suspend fun Blob.readByteArray(): ByteArray {
    val arrayBuffer = this.unsafeCast<BlobMissingBits>().arrayBuffer().await()
    return Int8Array(arrayBuffer).unsafeCast<ByteArray>()
}
