package kitty

import org.kodein.log.*

object LoggerFilter : LogFilter {
    private val isDevelopment = js("process.env.NODE_ENV === 'development'").unsafeCast<Boolean>()

    override fun filter(tag: Logger.Tag, entry: Logger.Entry): Logger.Entry? =
        if (!isDevelopment && entry.level <= Logger.Level.DEBUG) null else entry
}