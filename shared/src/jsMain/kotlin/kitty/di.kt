package kitty

import org.kodein.di.*
import org.kodein.log.*

internal actual fun DI.Builder.commonModule() {
    inBindSet<LogFilter> { add { provider { LoggerFilter } } }
    inBindSet<LogFrontend> { add { provider { LoggerFrontend } } }
}
