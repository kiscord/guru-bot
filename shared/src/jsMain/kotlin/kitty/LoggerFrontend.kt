package kitty

import org.kodein.log.*

object LoggerFrontend : LogFrontend {

    override fun getReceiverFor(tag: Logger.Tag) = LogReceiver { entry, message ->
        val logMsg = "[${tag.name}]: $message"

        val log = when (entry.level) {
            Logger.Level.DEBUG -> console::debug
            Logger.Level.INFO -> console::info
            Logger.Level.WARNING -> console::warn
            Logger.Level.ERROR -> console::error
        }

        if (entry.meta.isNotEmpty()) {
            console.groupCollapsed(logMsg)
            if (entry.ex != null) log(arrayOf(entry.ex))
            entry.meta.forEach { log(arrayOf("${it.key}: ${it.value}")) }
            console.groupEnd()
        } else if (entry.ex != null) log(arrayOf(logMsg, entry.ex))
        else log(arrayOf(logMsg))
    }
}

private external interface Console {
    fun dir(o: Any)
    fun error(vararg o: Any?)
    fun info(vararg o: Any?)
    fun warn(vararg o: Any?)
    fun debug(vararg o: Any?)
    fun group(label: String? = definedExternally)
    fun groupCollapsed(label: String? = definedExternally)
    fun groupEnd()
}

private external val console: Console
