plugins {
    kotlin("multiplatform")
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

kotlin {
    jvm()
    js {
        useCommonJs()
        browser()
    }
}

dependencies {
    commonMainApi(kiscord.jetbrains.markdown)
}
