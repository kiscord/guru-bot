package kiscord.markdown.lexer;

import kiscord.markdown.DiscordTokenTypes;
import org.intellij.markdown.MarkdownTokenTypes;
import org.intellij.markdown.IElementType;
import org.intellij.markdown.lexer.GeneratedLexer;

/* Auto generated File */
%%

%class _DiscordLexer
%unicode
%public

%function advance
%type IElementType

%implements GeneratedLexer

%{
  private List<Integer> stateStack = new ArrayList<Integer>();

  private boolean isHeader = false;

  private ParseDelimited parseDelimited = new ParseDelimited();

  private static class ParseDelimited {
    char exitChar = 0;
    IElementType returnType = null;
    boolean inlinesAllowed = true;
  }

  private static IElementType getDelimiterTokenType(char c) {
    switch (c) {
      case '"': return MarkdownTokenTypes.DOUBLE_QUOTE;
      case '\'': return MarkdownTokenTypes.SINGLE_QUOTE;
      case '(': return MarkdownTokenTypes.LPAREN;
      case ')': return MarkdownTokenTypes.RPAREN;
      case '[': return MarkdownTokenTypes.LBRACKET;
      case ']': return MarkdownTokenTypes.RBRACKET;
      case '<': return MarkdownTokenTypes.LT;
      case '>': return MarkdownTokenTypes.GT;
      default: return MarkdownTokenTypes.BAD_CHARACTER;
    }
  }

  private IElementType parseDelimited(IElementType contentsType, boolean allowInlines) {
    char first = yycharat(0);
    char last = yycharat(yylength() - 1);

    stateStack.push(yystate());

    parseDelimited.exitChar = last;
    parseDelimited.returnType = contentsType;
//    parseDelimited.inlinesAllowed = allowInlines;
    parseDelimited.inlinesAllowed = true;

    yybegin(PARSE_DELIMITED);

    yypushback(yylength() - 1);
    return getDelimiterTokenType(first);
  }

  private void processEol() {
    int newlinePos = 1;
    while (newlinePos < yylength() && yycharat(newlinePos) != '\n') {
      newlinePos++;
    }

    // there is always one at 0 so that means there are two at least
    if (newlinePos != yylength()) {
      yypushback(yylength() - newlinePos);
      return;
    }

    yybegin(YYINITIAL);
    yypushback(yylength() - 1);

    isHeader = false;
  }

  private void popState() {
    if (stateStack.isEmpty()) {
      yybegin(AFTER_LINE_START);
    }
    else {
      yybegin(stateStack.pop());
    }
  }

  private void resetState() {
    yypushback(yylength());

    popState();
  }

  private boolean canInline() {
    return yystate() == AFTER_LINE_START || yystate() == PARSE_DELIMITED && parseDelimited.inlinesAllowed;
  }

  private IElementType getReturnGeneralized(IElementType defaultType) {
    if (canInline()) {
      return defaultType;
    }
    return parseDelimited.returnType;
  }

  private int countChars(CharSequence s, char c) {
    int result = 0;
    for (int i = 0; i < s.length(); ++i) {
      if (s.charAt(i) == c)
        result++;
    }
    return result;
  }

  private int calcBalance(int startPos) {
    int balance = 0;
    for (int i = startPos; i >= 0; --i) {
      char c = yycharat(i);
      if (c == ')') {
        balance++;
      } else if (c == '(') {
        balance--;
        if (balance <= 0) break;
      }
    }
    return balance;
  }

  private void pushbackAutolink() {
    int length = yylength();
    if (yycharat(length - 1) == '/') {
      while (yycharat(length - 2) == '/') length--;
      yypushback(yylength() - length);
      return;
    }

    int balance = -1;

    // See DISCORD_AUTOLINK rule
    String badEnding = ".,:;!?\"'*_~]`";

    for (int i = length - 1; i >= 0; --i) {
      char c = yycharat(i);
      if (c == ')') {
        if (balance == -1) {
          balance = calcBalance(i);
        }

        // If there are not enough opening brackets to match this closing one, drop this bracket
        if (balance > 0) {
          balance--;
        } else {
            break;
        }
      } else if (badEnding.indexOf(c) == -1) {
        break;
      }

      length--;
    }

    yypushback(yylength() - length);
  }

%}

ALPHANUM = [\p{Letter}\p{Number}]
WHITE_SPACE = [ \t\f]
EOL = \R
ANY_CHAR = [^]

SCHEME = "http" "s"?
AUTOLINK = "<" {SCHEME} ":" [^ \t\f\n<>]+ ">"

HOST_PART={ALPHANUM}([a-zA-Z0-9_-]*{ALPHANUM})?
PATH_PART=[\S&&[^\]()]]|\][^\[(]
PATH=({PATH_PART}+ | ("(" {PATH_PART}* ")"? {PATH_PART}*)) ("(" {PATH_PART}* ")"? {PATH_PART}*)*
// See pushbackAutolink method
DISCORD_AUTOLINK = ({SCHEME} "://") {HOST_PART} ("." {HOST_PART})* (":" [0-9]+)? ("/" {PATH})? "/"?

%state TAG_START, AFTER_LINE_START, PARSE_DELIMITED, CODE

%%



<YYINITIAL> {

  // blockquote
  ^ ">" {WHITE_SPACE}+ {
    return MarkdownTokenTypes.BLOCK_QUOTE;
  }

  {ANY_CHAR} {
    resetState();
  }

  {WHITE_SPACE}* {EOL} {
    resetState();
  }
}

<AFTER_LINE_START, PARSE_DELIMITED> {
  // The special case of a backtick
  \\"`"+ {
    return getReturnGeneralized(MarkdownTokenTypes.ESCAPED_BACKTICKS);
  }

  // Escaping
  \\[\\\"'`*_{}\[\]()#+.,!:@#$%&~<>/-] {
    return getReturnGeneralized(MarkdownTokenTypes.TEXT);
  }

  // Backticks (code span)
  "`"+ {
    if (canInline()) {
      return MarkdownTokenTypes.BACKTICK;
    }
    return parseDelimited.returnType;
  }

  // Emphasis
  {WHITE_SPACE}+ ("*" | "_") {WHITE_SPACE}+ {
    return getReturnGeneralized(MarkdownTokenTypes.TEXT);
  }

  "*" {
    return getReturnGeneralized(MarkdownTokenTypes.EMPH);
  }

  "_" {
    return getReturnGeneralized(DiscordTokenTypes.UNDERLINE_EMPH);
  }

  "~" {
    return getReturnGeneralized(DiscordTokenTypes.TILDE);
  }

  "|" {
    return getReturnGeneralized(DiscordTokenTypes.PIPE);
  }

  {AUTOLINK} { return parseDelimited(MarkdownTokenTypes.AUTOLINK, false); }

}

<AFTER_LINE_START> {

  {WHITE_SPACE}+ {
    return MarkdownTokenTypes.WHITE_SPACE;
  }

  \" | "'" | "(" | ")" | "[" | "]" | "<" | ">" {
    return getDelimiterTokenType(yycharat(0));
  }
  ":" { return MarkdownTokenTypes.COLON; }
  "!" { return MarkdownTokenTypes.EXCLAMATION_MARK; }

  {WHITE_SPACE}* ({EOL} {WHITE_SPACE}*)+ {
    processEol();
    return MarkdownTokenTypes.EOL;
  }

  {DISCORD_AUTOLINK} {
    pushbackAutolink();
    return DiscordTokenTypes.DISCORD_AUTOLINK;
  }

  {ALPHANUM}+ (({WHITE_SPACE}+ | "_"+) {ALPHANUM}+)* / {WHITE_SPACE}+ {DISCORD_AUTOLINK} {
    return MarkdownTokenTypes.TEXT;
  }

  // optimize and eat underscores inside words
  {ALPHANUM}+ (({WHITE_SPACE}+ | "_"+) {ALPHANUM}+)* {
    return MarkdownTokenTypes.TEXT;
  }

  {ANY_CHAR} { return MarkdownTokenTypes.TEXT; }

}

<PARSE_DELIMITED> {
  {EOL} { resetState(); }

  {EOL} | {ANY_CHAR} {
    if (yycharat(0) == parseDelimited.exitChar) {
      yybegin(stateStack.pop());
      return getDelimiterTokenType(yycharat(0));
    }
    return parseDelimited.returnType;
  }

}

{ANY_CHAR} { return MarkdownTokenTypes.TEXT; }