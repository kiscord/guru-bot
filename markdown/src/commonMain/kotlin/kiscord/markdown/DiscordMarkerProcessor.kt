package kiscord.markdown

import kiscord.markdown.parser.*
import org.intellij.markdown.*
import org.intellij.markdown.flavours.commonmark.*
import org.intellij.markdown.lexer.*
import org.intellij.markdown.parser.*
import org.intellij.markdown.parser.constraints.*
import org.intellij.markdown.parser.markerblocks.*
import org.intellij.markdown.parser.markerblocks.providers.*
import org.intellij.markdown.parser.sequentialparsers.*
import kotlin.math.*

class DiscordMarkerProcessor(productionHolder: ProductionHolder, constraintsBase: MarkdownConstraints) :
    CommonMarkMarkerProcessor(productionHolder, constraintsBase) {

    private val markerBlockProviders = listOf(
        CodeBlockProvider(),
        CodeFenceProvider(),
        BlockQuoteProvider(),
    )

    override fun getMarkerBlockProviders() = markerBlockProviders

    override fun populateConstraintsTokens(
        pos: LookaheadText.Position,
        constraints: MarkdownConstraints,
        productionHolder: ProductionHolder
    ) {
        if (constraints.indent == 0) {
            return
        }

        val startOffset = pos.offset
        val endOffset = min(
            pos.offset - pos.offsetInCurrentLine + constraints.getCharsEaten(pos.currentLine),
            pos.nextLineOrEofOffset
        )

        val type = when (constraints.types.lastOrNull()) {
            '>' -> MarkdownTokenTypes.BLOCK_QUOTE
            else -> return
        }

        productionHolder.addProduction(listOf(SequentialParser.Node(startOffset..endOffset, type)))
    }

    private val interruptsParagraph: (LookaheadText.Position, MarkdownConstraints) -> Boolean =
        { position, constraints ->
            var result = false
            for (provider in getMarkerBlockProviders()) {
                if (provider.interruptsParagraph(position, constraints)) {
                    result = true
                    break
                }
            }
            result
        }

    override fun createNewMarkerBlocks(
        pos: LookaheadText.Position,
        productionHolder: ProductionHolder
    ): List<MarkerBlock> {
        if (pos.offsetInCurrentLine == -1) {
            return NO_BLOCKS
        }

        Compat.assert(MarkerBlockProvider.isStartOfLineWithConstraints(pos, stateInfo.currentConstraints))

        for (provider in getMarkerBlockProviders()) {
            val list = provider.createMarkerBlocks(pos, productionHolder, stateInfo)
            if (list.isNotEmpty()) {
                return list
            }
        }

        if (pos.offsetInCurrentLine >= stateInfo.nextConstraints.getCharsEaten(pos.currentLine)
            && pos.charsToNonWhitespace() != null
        ) {
            return listOf(
                DiscordParagraphMarkerBlock(
                    stateInfo.currentConstraints,
                    productionHolder.mark(),
                    interruptsParagraph
                )
            )
        }

        return emptyList()
    }

    object Factory : MarkerProcessorFactory {
        override fun createMarkerProcessor(productionHolder: ProductionHolder): MarkerProcessor<*> {
            return DiscordMarkerProcessor(productionHolder, DiscordConstraints.BASE)
        }
    }
}