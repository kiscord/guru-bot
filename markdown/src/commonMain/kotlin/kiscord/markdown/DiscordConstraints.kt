package kiscord.markdown

import org.intellij.markdown.parser.*
import org.intellij.markdown.parser.constraints.*
import org.intellij.markdown.parser.markerblocks.providers.*

class DiscordConstraints(
    private val indents: IntArray,
    types: CharArray,
    isExplicit: BooleanArray,
    charsEaten: Int
) : CommonMarkdownConstraints(indents, types, isExplicit, charsEaten) {
    override val base get() = BASE

    override fun createNewConstraints(
        indents: IntArray,
        types: CharArray,
        isExplicit: BooleanArray,
        charsEaten: Int
    ): DiscordConstraints {
        return DiscordConstraints(indents, types, isExplicit, charsEaten)
    }

    override fun addModifierIfNeeded(pos: LookaheadText.Position?): CommonMarkdownConstraints? {
        if (pos == null || pos.offsetInCurrentLine == -1)
            return null
        if (HorizontalRuleProvider.isHorizontalRule(pos.currentLine, pos.offsetInCurrentLine)) {
            return null
        }

        return tryAddBlockQuote(pos)
    }

    private fun tryAddBlockQuote(pos: LookaheadText.Position): CommonMarkdownConstraints? {
        val line = pos.currentLine

        var offset = pos.offsetInCurrentLine
        if (offset != 0) return null
        var count = 0

        while (offset != line.length && line[offset] == BQ_CHAR) {
            offset++
            count++
        }

        if (count != 1) return null

        var spacesAfter = 0
        if (offset >= line.length || line[offset] == ' ' || line[offset] == '\t') {
            spacesAfter = 1

            if (offset < line.length) {
                offset++
            }
        }

        if (spacesAfter == 0) return null

        return create(this, count + spacesAfter, BQ_CHAR, true, offset)
    }

    companion object {
        val BASE: DiscordConstraints = DiscordConstraints(IntArray(0), CharArray(0), BooleanArray(0), 0)

        private fun create(
            parent: DiscordConstraints,
            newIndentDelta: Int,
            newType: Char,
            newExplicit: Boolean,
            newOffset: Int
        ): DiscordConstraints {
            val n = parent.indents.size
            val indents = parent.indents.copyOf(n + 1)
            val types = parent.types.copyOf(n + 1)
            val isExplicit = parent.isExplicit.copyOf(n + 1)

            indents[n] = parent.indent + newIndentDelta
            types[n] = newType
            isExplicit[n] = newExplicit
            return parent.createNewConstraints(indents, types, isExplicit, newOffset)
        }
    }

}