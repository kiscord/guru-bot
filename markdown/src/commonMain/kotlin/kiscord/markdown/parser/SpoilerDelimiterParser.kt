package kiscord.markdown.parser

import kiscord.markdown.DiscordElementTypes
import kiscord.markdown.DiscordTokenTypes

class SpoilerDelimiterParser : DoubleTokenDelimiterParser(DiscordTokenTypes.PIPE, DiscordElementTypes.SPOILER)