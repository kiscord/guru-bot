package kiscord.markdown.parser

import kiscord.markdown.DiscordElementTypes
import kiscord.markdown.DiscordTokenTypes

class StrikeThroughDelimiterParser :
    DoubleTokenDelimiterParser(DiscordTokenTypes.TILDE, DiscordElementTypes.STRIKETHROUGH)