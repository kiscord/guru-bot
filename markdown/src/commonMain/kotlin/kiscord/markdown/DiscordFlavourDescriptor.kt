package kiscord.markdown

import org.intellij.markdown.IElementType
import org.intellij.markdown.MarkdownElementTypes
import org.intellij.markdown.MarkdownTokenTypes
import org.intellij.markdown.ast.ASTNode
import org.intellij.markdown.ast.getParentOfType
import org.intellij.markdown.ast.getTextInNode
import org.intellij.markdown.flavours.commonmark.CommonMarkFlavourDescriptor
import kiscord.markdown.lexer._DiscordLexer
import kiscord.markdown.parser.EmphStrongUnderlineDelimiterParser
import kiscord.markdown.parser.SpoilerDelimiterParser
import kiscord.markdown.parser.StrikeThroughDelimiterParser
import org.intellij.markdown.html.*
import org.intellij.markdown.html.entities.EntityConverter
import org.intellij.markdown.lexer.MarkdownLexer
import org.intellij.markdown.parser.LinkMap
import org.intellij.markdown.parser.MarkerProcessorFactory
import org.intellij.markdown.parser.sequentialparsers.EmphasisLikeParser
import org.intellij.markdown.parser.sequentialparsers.SequentialParser
import org.intellij.markdown.parser.sequentialparsers.SequentialParserManager
import org.intellij.markdown.parser.sequentialparsers.impl.*

open class DiscordFlavourDescriptor(
    useSafeLinks: Boolean = true, absolutizeAnchorLinks: Boolean = false
) : CommonMarkFlavourDescriptor(useSafeLinks, absolutizeAnchorLinks) {
    override val markerProcessorFactory: MarkerProcessorFactory get() = DiscordMarkerProcessor.Factory

    override fun createInlinesLexer(): MarkdownLexer {
        return MarkdownLexer(_DiscordLexer())
    }

    override val sequentialParserManager = object : SequentialParserManager() {
        override fun getParserSequence(): List<SequentialParser> {
            return listOf(
                AutolinkParser(listOf(MarkdownTokenTypes.AUTOLINK, DiscordTokenTypes.DISCORD_AUTOLINK)),
                BacktickParser(),
                InlineLinkParser(),
                EmphasisLikeParser(
                    EmphStrongUnderlineDelimiterParser(),
                    StrikeThroughDelimiterParser(),
                    SpoilerDelimiterParser()
                )
            )
        }
    }

    override fun createHtmlGeneratingProviders(linkMap: LinkMap, baseURI: URI?): Map<IElementType, GeneratingProvider> {
        return super.createHtmlGeneratingProviders(linkMap, baseURI) + hashMapOf(
            DiscordElementTypes.STRIKETHROUGH to SimpleInlineTagProvider("s", 2, -2),
            DiscordElementTypes.UNDERLINE to SimpleInlineTagProvider("u", 2, -2),
            DiscordElementTypes.SPOILER to object : SimpleInlineTagProvider("span", 2, -2) {
                override fun openTag(visitor: HtmlGenerator.HtmlGeneratingVisitor, text: String, node: ASTNode) {
                    visitor.consumeTagOpen(node, tagName, "class=\"spoiler\"")
                }
            },
            MarkdownTokenTypes.EOL to object : GeneratingProvider {
                override fun processNode(visitor: HtmlGenerator.HtmlGeneratingVisitor, text: String, node: ASTNode) {
                    visitor.consumeHtml("\n")
                }
            },
            DiscordTokenTypes.DISCORD_AUTOLINK to object : GeneratingProvider {
                override fun processNode(visitor: HtmlGenerator.HtmlGeneratingVisitor, text: String, node: ASTNode) {
                    val linkText = node.getTextInNode(text)

                    // #28: do not render GFM autolinks under link titles
                    // (though it's "OK" according to CommonMark spec)
                    if (node.getParentOfType(
                            MarkdownElementTypes.LINK_LABEL,
                            MarkdownElementTypes.LINK_TEXT
                        ) != null
                    ) {
                        visitor.consumeHtml(linkText)
                        return
                    }

                    // according to GFM_AUTOLINK rule in lexer, link either starts with scheme or with 'www.'
                    val absoluteLink = if (hasSchema(linkText)) linkText else "http://$linkText"

                    val link = EntityConverter.replaceEntities(linkText, true, false)
                    val normalizedDestination = LinkMap.normalizeDestination(absoluteLink, false).let {
                        if (useSafeLinks) makeXssSafeDestination(it) else it
                    }
                    visitor.consumeTagOpen(node, "a", "href=\"$normalizedDestination\"")
                    visitor.consumeHtml(link)
                    visitor.consumeTagClose("a")
                }

                private fun hasSchema(linkText: CharSequence): Boolean {
                    val index = linkText.indexOf('/')
                    if (index == -1) return false
                    return index != 0
                            && index + 1 < linkText.length
                            && linkText[index - 1] == ':'
                            && linkText[index + 1] == '/'
                }
            },
        )
    }
}