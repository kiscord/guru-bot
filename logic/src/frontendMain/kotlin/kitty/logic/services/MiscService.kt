package kitty.logic.services

import io.ktor.http.*
import kitty.*
import kitty.logic.*
import kitty.resources.*
import kotlinx.coroutines.flow.*

interface MiscService {
    val stats: FlushableStateFlow<StatsResponse?>
    val botProfile: FlushableStateFlow<UserProfile?>

    val botTitle: StateFlow<BotTitle>

    sealed interface BotTitle {
        val text: String

        object Default : BotTitle {
            override val text: String get() = BuildConfig.NAME
        }

        class Prefixed(val prefix: String) : BotTitle {
            val suffix: String get() = BuildConfig.NAME
            override val text: String
                get() = prefix + suffix
        }

        class Custom(override val text: String) : BotTitle
    }

    class Default(service: ServiceIO) : ServiceIO by service, MiscService {
        override val stats: FlushableStateFlow<StatsResponse?> =
            resource(ApiResource.Stats(), authenticatedOnly = false) {
                method = HttpMethod.Get
            }

        override val botProfile: FlushableStateFlow<UserProfile?> =
            resource(ApiResource.Users.Bot(), authenticatedOnly = false) {
                method = HttpMethod.Get
            }

        override val botTitle: StateFlow<BotTitle> by lazy {
            botProfile.map { botProfile ->
                if (botProfile == null) return@map BotTitle.Default
                val username = botProfile.username
                if (username.endsWith(BuildConfig.NAME)) {
                    val prefix = username.removeSuffix(BuildConfig.NAME)
                    return@map BotTitle.Prefixed(prefix)
                } else {
                    return@map BotTitle.Custom(username)
                }
            }.stateIn(coroutineScope, SharingStarted.Lazily, BotTitle.Default)
        }
    }
}
