package kitty.logic.services

import kitty.*
import kitty.logic.*
import kitty.resources.*
import io.ktor.http.*
import kiscord.api.*
import kotlinx.coroutines.flow.*

interface UserService {
    val userProfile: FlushableStateFlow<UserProfile?>
    val userGuilds: FlushableStateFlow<List<DiscordGuildDTO>?>
    fun user(userId: Snowflake): FlushableStateFlow<UserProfile?>

    class Default(service: ServiceIO) : ServiceIO by service, UserService {
        override val userProfile: FlushableStateFlow<UserProfile?> = resource(ApiResource.Users.Me()) {
            method = HttpMethod.Get
        }

        override val userGuilds: FlushableStateFlow<List<DiscordGuildDTO>?> = resource(ApiResource.Users.Me.Guilds()) {
            method = HttpMethod.Get
        }

        private val userMap = mutableMapOf<Snowflake, FlushableStateFlow<UserProfile?>>()

        override fun user(userId: Snowflake): FlushableStateFlow<UserProfile?> = userMap.getOrPut(userId) {
            resource(ApiResource.Users.Info(userId = userId))
        }
    }
}

