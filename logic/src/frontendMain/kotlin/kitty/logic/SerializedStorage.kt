package kitty.logic

import io.ktor.utils.io.core.*
import kotlinx.browser.*
import kotlinx.coroutines.flow.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import org.w3c.dom.*
import org.w3c.dom.events.*

actual class SerializedStorage<T : Any>(
    private val key: String,
    private val serializer: KSerializer<T>,
    private val format: StringFormat = Json,
    private val storage: Storage = window.localStorage,
) : Closeable {
    actual constructor(key: String, serializer: KSerializer<T>, format: StringFormat) :
            this(key, serializer, format, window.localStorage)

    private val _flow = MutableStateFlow(parse(format, storage.getItem(key)))
    actual val flow: StateFlow<T?> = _flow.asStateFlow()
    actual val value: T? get() = _flow.value

    actual fun clear() {
        storage.removeItem(key)
        _flow.value = null
    }

    actual fun set(value: T) {
        storage.setItem(key, format.encodeToString(serializer, value))
        _flow.value = value
    }

    private val eventListener = EventListener { event ->
        event as StorageEvent
        _flow.value = when {
            !event.isTrusted || event.storageArea !== storage -> return@EventListener
            event.key == null -> null
            event.key == key -> parse(format, event.newValue)
            else -> return@EventListener
        }
    }

    init {
        window.addEventListener("storage", eventListener)
    }

    override fun close() {
        window.removeEventListener("storage", eventListener)
    }

    private fun parse(format: StringFormat, value: String?): T? {
        return try {
            format.decodeFromString(serializer, value ?: return null)
        } catch (_: SerializationException) {
            null
        }
    }
}
