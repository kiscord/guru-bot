package kitty.logic

import io.ktor.client.*
import io.ktor.http.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.kodein.di.*
import kotlin.time.*
import kotlin.time.Duration.Companion.seconds

class RestServiceIO(override val di: DI) : ServiceIO, DIAware {
    override val defaultRequestContentType: ContentType get() = ContentType.Application.Cbor
    private val darkKittyAuth: StateFlow<DarkKittyAuth> by instance(tag = "auth-flow")
    override val httpClient: HttpClient by instance()

    override val coroutineScope = CoroutineScope(Dispatchers.Default + SupervisorJob())

    private sealed class Flush<out T> {
        object Empty : Flush<Nothing>()
        class With<out T>(val value: T) : Flush<T>()
    }

    private class FlushableFlowImpl<T>(
        stateFlow: StateFlow<T>,
        private val flushFlow: MutableStateFlow<in Flush<T>>
    ) : FlushableStateFlow<T>, StateFlow<T> by stateFlow {
        override fun flush() {
            flushFlow.value = Flush.Empty
        }

        override fun flush(with: T) {
            flushFlow.value = Flush.With(with)
        }
    }

    override fun <T : Any> http(authenticatedOnly: Boolean, block: suspend HttpClient.() -> T): FlushableStateFlow<T?> {
        val flushFlow = MutableStateFlow<Flush<T?>>(Flush.Empty)

        val flow = when {
            authenticatedOnly -> {
                flushFlow.combine(darkKittyAuth) { flush, auth ->
                    if (auth !is DarkKittyAuth.Authenticated) {
                        flushFlow.compareAndSet(flush, Flush.Empty)
                        return@combine null
                    }
                    if (flush is Flush.With<T?>) return@combine flush.value
                    val newValue = httpClient.block()
                    flushFlow.compareAndSet(Flush.Empty, Flush.With(newValue))
                    newValue
                }
            }

            else -> flushFlow.map { flush ->
                if (flush is Flush.With<T?>) return@map flush.value
                val newValue = httpClient.block()
                flushFlow.compareAndSet(Flush.Empty, Flush.With(newValue))
                newValue
            }
        }

        return flow.flushable(with = flushFlow)
    }

    private fun <T> Flow<T>.flushable(with: MutableStateFlow<Flush<T?>>): FlushableStateFlow<T?> {
        val stateFlow = stateIn(
            coroutineScope,
            SharingStarted.WhileSubscribed(stopTimeout = 5.seconds, replayExpiration = Duration.ZERO),
            null
        )

        return FlushableFlowImpl(stateFlow, with)
    }

    override suspend fun <T : Any> oneshotHttp(authenticatedOnly: Boolean, block: suspend HttpClient.() -> T): T? {
        if (authenticatedOnly && darkKittyAuth.value !is DarkKittyAuth.Authenticated) return null
        return httpClient.block()
    }
}
