package kitty.logic

import com.arkivanov.decompose.*
import com.arkivanov.decompose.router.stack.*
import com.arkivanov.decompose.router.stack.webhistory.*
import com.arkivanov.essenty.parcelable.*
import io.ktor.resources.*
import kiscord.api.*
import kotlinx.serialization.*
import org.kodein.di.*

@OptIn(ExperimentalDecomposeApi::class)
class KittyRootComponent(
    override val di: DI,
    componentContext: ComponentContext,
    deepLink: DeepLink = DeepLink.None,
    webHistoryController: WebHistoryController? = null,
) : ComponentContext by componentContext, DIAware, DarkKittyRoot {
    private val router: ResourcesRouter by instance()
    private val stackNavigation: StackNavigation<Config> by instance()
    private val deepLinkParser: (DeepLink) -> Config by factory(tag = "deep-link-parser")

    override val childStack = childStack(
        source = stackNavigation,
        initialStack = { listOf(deepLinkParser(deepLink)) },
        handleBackButton = true,
        childFactory = ::createChild
    )

    init {
        webHistoryController?.attach(
            navigator = stackNavigation,
            stack = childStack,
            getPath = { router.toDeepLink(it).path },
            getConfiguration = { deepLinkParser(DeepLink.Web(it)) as Config? ?: Config.Overview },
        )
    }

    private fun createChild(config: Config, componentContext: ComponentContext): DarkKittyRoot.Child {
        val subDI = subDI(di, copy = Copy.All) {
            bindProvider(tag = "child-component-context") { componentContext }
        }

        return config.createChild(subDI)
    }

    @Serializable
    @Resource("/")
    abstract class Config : Parcelable {
        open val parent: Config get() = this

        abstract fun createChild(di: DI): DarkKittyRoot.Child

        override fun toString() = this::class.simpleName ?: super.toString()

        @Parcelize
        @Serializable
        @Resource("/overview")
        object Overview : Config() {
            override fun createChild(di: DI) = DarkKittyRoot.Child.Overview(di)
        }

        @Parcelize
        @Serializable
        @Resource("/guilds")
        object GuildSelector : Config() {
            override fun createChild(di: DI) = DarkKittyRoot.Child.GuildSelector(di)
        }

        @Parcelize
        @Serializable
        @Resource("/{guildId}")
        data class GuildOverview(
            override val parent: GuildSelector = GuildSelector,
            val guildId: Snowflake
        ) : Config() {
            override fun createChild(di: DI) = DarkKittyRoot.Child.GuildOverview(di, guildId)
        }

        @Parcelize
        @Serializable
        @Resource("/login")
        data class Login(
            @SerialName("access_token")
            val accessToken: String? = null,
            @SerialName("refresh_token")
            val refreshToken: String? = null,
        ) : Config() {
            override fun createChild(di: DI) = DarkKittyRoot.Child.Login(di, accessToken, refreshToken)
        }
    }
}
