package kitty.logic

import kotlinx.serialization.Serializable

@Serializable
sealed interface DeepLink {
    @Serializable
    object None : DeepLink

    @Serializable
    data class Web(val path: String) : DeepLink
}