package kitty.logic

import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.resources.serialization.*
import kotlinx.serialization.*

class ResourcesRouter(
    private val resourcesFormat: ResourcesFormat,
    resources: Set<KSerializer<out Any>>,
) {
    private val rootNode = buildRoutingTree(resources)

    fun <R : Any> link(serializer: KSerializer<in R>, resource: R): String {
        val url = URLBuilder()
        @Suppress("INVISIBLE_MEMBER")
        href(resourcesFormat, serializer, resource, url)
        return url.build().fullPath
    }

    fun resource(link: String): Any? {
        val result = rootNode.resolveNode(link) ?: return null
        val resourceSerializer = result.node.resource ?: return null
        return resourcesFormat.decodeFromParameters(resourceSerializer, result.parameters)
    }

    fun fromDeepLink(deepLink: DeepLink): Any? = when (deepLink) {
        DeepLink.None -> null
        is DeepLink.Web -> resource(deepLink.path)
    }

    fun <R : Any> toDeepLink(resource: R): DeepLink.Web = DeepLink.Web(linkInRuntime(resource))

    private fun buildRoutingTree(resources: Set<KSerializer<out Any>>): RoutingNode {
        val root = MutableRoutingNode()

        resources.forEach { serializer ->
            val path = resourcesFormat.encodeToPathPattern(serializer)
            val segments = path.split('/').filter { it.isNotEmpty() }.map {
                if (!it.startsWith('{') || !it.endsWith('}')) return@map PathSelector.Constant(it)
                val name = it.substring(1, it.lastIndex)
                PathSelector.PathParameter(name)
            }
            val node = root.findOrCreateNode(segments)
            if (node.resource != null) {
                throw IllegalArgumentException("Conflicting routing nodes found: ${node.resource} and $serializer")
            }
            node.resource = serializer
        }

        return root.asReadOnly()
    }
}

inline fun <reified R : Any> ResourcesRouter.link(resource: R): String = link(serializer(), resource)

@Suppress("UNCHECKED_CAST")
@OptIn(InternalSerializationApi::class)
fun <R : Any> ResourcesRouter.linkInRuntime(resource: R): String {
    return link(resource::class.serializer() as KSerializer<R>, resource)
}

private sealed class PathSelector {
    data class Constant(val constant: String) : PathSelector()
    data class PathParameter(val parameterName: String) : PathSelector()
}

private interface RoutingNode {
    val resource: KSerializer<out Any>? get() = null
    val routingConstant: Map<PathSelector.Constant, RoutingNode> get() = emptyMap()
    val routingParameter: Map<PathSelector.PathParameter, RoutingNode> get() = emptyMap()
}

private data class ReadOnlyRoutingNode(
    override val resource: KSerializer<out Any>? = null,
    override val routingConstant: Map<PathSelector.Constant, RoutingNode> = emptyMap(),
    override val routingParameter: Map<PathSelector.PathParameter, RoutingNode> = emptyMap(),
) : RoutingNode

private class MutableRoutingNode : RoutingNode {
    override var resource: KSerializer<out Any>? = null
    override var routingConstant: MutableMap<PathSelector.Constant, MutableRoutingNode> = mutableMapOf()
    override var routingParameter: MutableMap<PathSelector.PathParameter, MutableRoutingNode> = mutableMapOf()

    fun findOrCreateNode(segments: List<PathSelector>): MutableRoutingNode {
        if (segments.isEmpty()) return this
        val segment = segments.first()
        val leftoverSegments = segments.subList(1, segments.size)
        return when (segment) {
            is PathSelector.Constant -> routingConstant.getOrPut(segment) { MutableRoutingNode() }
            is PathSelector.PathParameter -> routingParameter.getOrPut(segment) { MutableRoutingNode() }
        }.findOrCreateNode(leftoverSegments)
    }
}

private fun RoutingNode.asReadOnly(): ReadOnlyRoutingNode = ReadOnlyRoutingNode(
    resource = resource,
    routingConstant = routingConstant.mapValues { (_, value) -> value.asReadOnly() },
    routingParameter = routingParameter.mapValues { (_, value) -> value.asReadOnly() },
)

private fun RoutingNode.resolveNode(path: String): ResolveResult? {
    val paths = path.split('?', limit = 2)
    val parameters = if (paths.size > 1) parseQueryString(paths[1]) else Parameters.Empty
    return resolveNode(paths[0].split('/').filter { it.isNotEmpty() }, parameters)
}

private fun RoutingNode.resolveNode(segments: List<String>, parameters: Parameters): ResolveResult? {
    if (segments.isEmpty()) return ResolveResult(this, parameters)
    val segment = segments.first()
    val leftoverSegments = segments.subList(1, segments.size)
    routingConstant[PathSelector.Constant(segment)]?.resolveNode(leftoverSegments, parameters)?.let { return it }
    routingParameter.forEach { (selector, node) ->
        val updatedParams = ParametersBuilder().apply {
            appendAll(parameters)
            append(selector.parameterName, segment)
        }.build()
        node.resolveNode(leftoverSegments, updatedParams)?.let { return it }
    }
    return null
}

private data class ResolveResult(
    val node: RoutingNode,
    val parameters: Parameters
)
