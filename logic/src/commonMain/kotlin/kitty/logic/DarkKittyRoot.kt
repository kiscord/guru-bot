package kitty.logic

import com.arkivanov.decompose.*
import com.arkivanov.decompose.router.stack.*
import com.arkivanov.decompose.value.*
import kiscord.api.*
import org.kodein.di.*

interface DarkKittyRoot {
    val childStack: Value<ChildStack<*, Child>>

    abstract class Child(
        override val di: DI
    ) : ComponentContext by di.direct.instance(tag = "child-component-context"), DIAware {
        class Overview(di: DI) : Child(di)

        class GuildSelector(
            di: DI
        ) : Child(di)

        class GuildOverview(
            di: DI,
            val guildId: Snowflake,
        ) : Child(di)

        class Login(
            di: DI,
            val accessToken: String? = null,
            val refreshToken: String? = null
        ) : Child(di)
    }
}