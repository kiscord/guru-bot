package kitty.logic

import kotlinx.coroutines.flow.*

interface FlushableStateFlow<T> : StateFlow<T> {
    fun flush()
    fun flush(with: T)
}