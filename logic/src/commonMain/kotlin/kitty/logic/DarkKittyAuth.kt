package kitty.logic

import io.ktor.client.plugins.auth.providers.*

sealed interface DarkKittyAuth {
    val isPotentiallyAuthenticated: Boolean

    object None : DarkKittyAuth {
        override val isPotentiallyAuthenticated: Boolean get() = false
    }

    object Loading : DarkKittyAuth {
        override val isPotentiallyAuthenticated: Boolean get() = true
    }

    abstract class Authenticated(
        val bearerTokens: BearerTokens,
    ) : DarkKittyAuth {
        override val isPotentiallyAuthenticated: Boolean get() = true

        abstract fun logout()
    }

    class Expired(
        val bearerTokens: BearerTokens,
    ) : DarkKittyAuth {
        override val isPotentiallyAuthenticated: Boolean get() = true
    }

    class Error(
        val message: String,
    ) : DarkKittyAuth {
        override val isPotentiallyAuthenticated: Boolean get() = false
    }

    companion object
}
