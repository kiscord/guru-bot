package kitty.logic

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.*

interface ServiceIO {
    val httpClient: HttpClient
    val coroutineScope: CoroutineScope
    val defaultRequestContentType: ContentType

    fun <T : Any> http(authenticatedOnly: Boolean = true, block: suspend HttpClient.() -> T): FlushableStateFlow<T?>
    suspend fun <T : Any> oneshotHttp(authenticatedOnly: Boolean = true, block: suspend HttpClient.() -> T): T?
}

inline fun <reified T : Any, reified R : Any> ServiceIO.resource(
    resource: R,
    authenticatedOnly: Boolean = true,
    crossinline builder: HttpRequestBuilder.() -> Unit = {}
): FlushableStateFlow<T?> = http(authenticatedOnly = authenticatedOnly) {
    request(resource, builder).body()
}

suspend inline fun <reified T : Any, reified R : Any> ServiceIO.oneshotResource(
    resource: R,
    authenticatedOnly: Boolean = true,
    crossinline builder: HttpRequestBuilder.() -> Unit = {}
): T? = oneshotHttp(authenticatedOnly = authenticatedOnly) {
    request(resource, builder).body()
}
