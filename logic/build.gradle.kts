plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

kotlin {
    js("frontend") {
        useCommonJs()
        browser()
    }

    sourceSets.configureEach {
        languageSettings {
            optIn("kotlin.RequiresOptIn")
            progressiveMode = true
        }
    }
}

dependencies {
    commonMainApi(project(":shared"))
    commonMainApi(libs.decompose)
    commonMainApi("io.ktor:ktor-client-resources")
}
