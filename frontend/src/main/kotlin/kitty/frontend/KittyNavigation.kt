package kitty.frontend

import com.arkivanov.decompose.router.stack.*
import kitty.logic.*

class KittyNavigation(
    navigation: StackNavigation<KittyRootComponent.Config> = StackNavigation(),
    private val deepLinkParser: (DeepLink) -> KittyRootComponent.Config,
) : StackNavigation<KittyRootComponent.Config> by navigation {
    fun navigateTo(config: KittyRootComponent.Config, replace: Boolean = false) {
        if (replace) {
            val list = buildList {
                add(config)
                var parent = config.parent
                while (config != parent) {
                    add(parent)
                    parent = parent.parent
                }
            }.asReversed()

            navigate { list }
        } else {
            bringToFront(config)
        }
    }

    fun navigateTo(deepLink: DeepLink, replace: Boolean = false) {
        navigateTo(deepLinkParser(deepLink), replace)
    }
}