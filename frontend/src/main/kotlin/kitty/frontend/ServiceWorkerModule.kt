package kitty.frontend

import kotlinx.browser.*
import org.kodein.di.*

val ServiceWorkerModule = DI.Module("kitty-serviceworker") {
    if (js("process.env.NODE_ENV") === "production" && js("'serviceWorker' in window.navigator").unsafeCast<Boolean>()) {
        onReady {
            window.addEventListener("load", {
                window.navigator.serviceWorker.register("/service-worker.js").then {
                    console.log("SW registered: ", it)
                }.catch {
                    console.error("SW registration failed", it)
                }
            })
        }
    }
}