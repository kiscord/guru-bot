package kitty.frontend

import androidx.compose.runtime.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.plugins.resources.Resources
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.resources.serialization.*
import io.ktor.serialization.kotlinx.cbor.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.util.*
import io.ktor.util.logging.*
import kitty.*
import kitty.frontend.auth.*
import kitty.frontend.l10n.*
import kitty.http.*
import kitty.logic.*
import kitty.resources.*
import kotlinx.browser.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.serialization.*
import kotlinx.serialization.cbor.*
import kotlinx.serialization.json.*
import kotlinx.serialization.modules.*
import org.kodein.di.*
import org.w3c.dom.url.*

@OptIn(DelicateCoroutinesApi::class)
val HttpClientModule = DI.Module("kitty-httpclient") {
    bindSingleton(tag = "tokens") {
        SerializedStorage("tokens", BearerTokensSerializer, format = instance<Json>())
    }

    bindSingleton(tag = "auth-flow") {
        val tokensStorage: SerializedStorage<BearerTokens> = instance("tokens")
        val resourcesFormat: ResourcesFormat = instance()

        val urlBuilder = URLBuilder()
        href(resourcesFormat, WellKnownResource.JwksJson(), urlBuilder)
        val getKey = createRemoteJWKSet(URL(urlBuilder.buildString()))

        fun jwtVerify(token: String): Deferred<JWTVerifyResultWithResolvedKey> =
            jwtVerify(token, getKey, jwtVerifyOptions {
                audience = window.location.origin
                issuer = window.location.origin
                typ = "JWT"
            }).asDeferred()

        tokensStorage.flow.map { tokens ->
            if (tokens == null) {
                return@map DarkKittyAuth.None
            }

            try {
                val accessTokenResult = jwtVerify(tokens.accessToken)
                val refreshTokenResult = jwtVerify(tokens.refreshToken)
                joinAll(accessTokenResult, refreshTokenResult)
                if (accessTokenResult.isCompleted && refreshTokenResult.isCompleted) {
                    object : DarkKittyAuth.Authenticated(tokens) {
                        override fun logout() {
                            tokensStorage.clear()
                        }
                    }
                } else {
                    DarkKittyAuth.Error("JWTs failed verification")
                }
            } catch (e: JWTExpired) {
                DarkKittyAuth.Expired(tokens)
            } catch (e: Throwable) {
                console.error("Error during jwt verification", e)
                DarkKittyAuth.Error(e.message ?: "unknown error")
            } catch (e: dynamic) {
                console.error("Goddamn that js... Fatal error during jwt verification", e)
                DarkKittyAuth.Error("unknown error")
            }
        }.stateIn(GlobalScope, SharingStarted.Eagerly, DarkKittyAuth.Loading)
    }

    bindProvider {
        val authFlow: StateFlow<DarkKittyAuth> = instance(tag = "auth-flow")

        authFlow.value
    }

    bindSingleton {
        val tokensStorage: SerializedStorage<BearerTokens> = instance(tag = "tokens")
        val localizationProvider: State<LocalizationProvider> = instance()
        val contentNegotiationConfigurers: Set<ContentNegotiationConfigurer> = instance()

        HttpClient {
            followRedirects = false

            install(DefaultRequest) {
                headers {
                    if (!contains(HttpHeaders.AcceptLanguage)) {
                        val langs = mutableListOf<HeaderValueWithParameters>()
                        val currentLanguages = localizationProvider.value.languages
                        currentLanguages.mapIndexedTo(langs) { index, lang ->
                            AcceptLanguage(lang, 1.0f - index * 0.1f)
                        }
                        if (langs.isEmpty()) langs += AcceptLanguage("en", quality = 0.5f)
                        append(HttpHeaders.AcceptLanguage, langs.joinToString(separator = ", "))
                    }
                }
            }
            install(ContentNegotiation) {
                contentNegotiationConfigurers.forEach { configurer ->
                    with(configurer) {
                        configure()
                    }
                }
            }
            install(Resources) {
                serializersModule += instance()
            }
            install(Auth) {
                val originUrl = Url(URLBuilder.origin)
                bearer {
                    realm = KittyBuildConfig.NAME
                    loadTokens {
                        tokensStorage.value
                    }
                    refreshTokens {
                        val oldTokens = oldTokens ?: return@refreshTokens null

                        val resp = client.post(OAuth2Resource.Refresh()) {
                            expectSuccess = false
                            bearerAuth(oldTokens.refreshToken)
                            markAsRefreshTokenRequest()
                        }
                        if (resp.status == HttpStatusCode.Unauthorized || resp.status == HttpStatusCode.Forbidden) {
                            tokensStorage.clear()
                            return@refreshTokens null
                        } else if (!resp.status.isSuccess()) {
                            return@refreshTokens null
                        }
                        resp.body<BearerTokens>().also { newTokens ->
                            tokensStorage.set(newTokens)
                        }
                    }
                    sendWithoutRequest { request ->
                        request.url.build().protocolWithAuthority == originUrl.protocolWithAuthority
                                && !request.attributes.contains(Auth.AuthCircuitBreaker)
                    }
                }
            }
        }
    }
}
