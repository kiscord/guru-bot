package kitty.frontend

import com.arkivanov.essenty.lifecycle.*
import org.kodein.log.*

class LifecycleLogger(loggerFactory: LoggerFactory) : Lifecycle.Callbacks {
    private val logger = loggerFactory.newLogger<LifecycleLogger>()

    override fun onCreate() {
        logger.debug { "Lifecycle transitioned to Created state" }
    }

    override fun onStart() {
        logger.debug { "Lifecycle transitioned to Started state" }
    }

    override fun onResume() {
        logger.debug { "Lifecycle transitioned to Resumed state" }
    }

    override fun onPause() {
        logger.debug { "Lifecycle transitioned to Paused state" }
    }

    override fun onStop() {
        logger.debug { "Lifecycle transitioned to Stopped state" }
    }

    override fun onDestroy() {
        logger.debug { "Lifecycle transitioned to Destroyed state" }
    }
}