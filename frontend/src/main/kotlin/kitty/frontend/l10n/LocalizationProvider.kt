package kitty.frontend.l10n

import androidx.compose.runtime.*
import kitty.frontend.js.*
import org.jetbrains.compose.web.css.*

interface LocalizationProvider {
    val language: LanguageCode?
    val languages: List<LanguageCode>
    val supportedLanguages: Collection<LanguageCode>

    operator fun get(key: String, options: dynamic = undefined): String

    fun changeLanguage(language: LanguageCode)

    fun forContext(language: LanguageCode? = null, namespace: String? = null): LocalizationProvider
}


val l10n: LocalizationProvider
    @Composable
    @ReadOnlyComposable
    get() = LocalLocalizationProvider.current

@Suppress("NOTHING_TO_INLINE")
inline operator fun LocalizationProvider.invoke(key: String): String = get(key)
inline operator fun LocalizationProvider.invoke(key: String, options: dynamic.() -> Unit): String =
    get(key, jso(options))

private val LocalLocalizationProvider =
    staticCompositionLocalOf<LocalizationProvider> { throw IllegalStateException("No localization provider in the context") }

@Composable
fun ProvideLocalizationProvider(provider: LocalizationProvider, content: @Composable () -> Unit) {
    CompositionLocalProvider(LocalLocalizationProvider provides provider, content = content)
}

@OptIn(InternalComposeApi::class)
@Composable
fun <T> SwitchLocalizationContext(
    language: LanguageCode? = null,
    namespace: String? = null,
    content: @Composable () -> T
): T {
    if (language == null && namespace == null) {
        return content()
    }
    currentComposer.startProviders(
        arrayOf(
            LocalLocalizationProvider provides LocalLocalizationProvider.current.forContext(language, namespace)
        )
    )
    val result = content()
    currentComposer.endProviders()
    return result
}
