package kitty.frontend.l10n

import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.*
import kitty.frontend.js.*
import org.jetbrains.compose.web.css.*
import org.kodein.log.*

@NoLiveLiterals
class I18NextLocalizationProvider(private val context: Context, loggerFactory: LoggerFactory) {
    private val i18next = createInstance()

    companion object {
        private val localesRegex = Regex("\\./([-_a-z]+)_([-a-z]+)\\.json")
    }

    private val _state: MutableState<I18NextState>
    val state: State<LocalizationProvider> get() = _state

    private val supportedLanguages: SnapshotStateList<LanguageCode> = mutableStateListOf()
    private val logger = loggerFactory.newLogger<I18NextLocalizationProvider>()

    init {
        i18next.use(require("i18next-http-backend"))
        i18next.use(require("i18next-browser-languagedetector"))

        val supportedNamespacesSet = mutableSetOf<String>()
        val supportedLanguagesSet = mutableSetOf<LanguageCode>()

        context.keys().forEach {
            localesRegex.matchEntire(it)?.let { matchResult ->
                supportedNamespacesSet.add(matchResult.groupValues[1])
                supportedLanguagesSet.add(matchResult.groupValues[2])
            }
        }

        supportedLanguages.addAll(supportedLanguagesSet.sorted())

        logger.debug { "Supported languages: $supportedLanguagesSet" }
        logger.debug { "Supported namespaces: $supportedNamespacesSet" }

        _state = mutableStateOf(
            I18NextState(
                i18next = i18next,
                initialized = false,
                requestedLanguage = "en",
                supportedLanguages = supportedLanguages,
            ),
            referentialEqualityPolicy()
        )

        i18next.on("initialized") {
            _state.value = _state.value.copy(initialized = true)
        }
        i18next.on("languageChanged") {
            _state.value = _state.value.copy(requestedLanguage = it.unsafeCast<LanguageCode>())
        }

        i18next.init(jso {
            fallbackLng = "en"
            defaultNS = "translation"
            backend = jso {
                allowMultiLoading = false
                loadPath = { lngs: Array<String>, namespaces: Array<String> ->
                    val lng = lngs.single()
                    val namespace = namespaces.single()
                    val path = "./${namespace}_${lng}.json"
                    if (path in context.keys()) context(path) else null
                }
            }
            detection = jso {
                lookupQuerystring = "lang"
            }
            supportedLngs = supportedLanguages.toTypedArray()
            ns = supportedNamespacesSet.toTypedArray()
        })
    }

    private data class I18NextState(
        val i18next: I18Next,
        val initialized: Boolean,
        override val supportedLanguages: Collection<LanguageCode>,
        val requestedLanguage: LanguageCode,
    ) : LocalizationProvider {
        override val language: LanguageCode?
            get() = i18next.resolvedLanguage ?: i18next.language

        override val languages: List<LanguageCode>
            get() = (listOfNotNull(i18next.resolvedLanguage) + i18next.languages.orEmpty()).distinct()

        override fun get(key: String, options: dynamic): String {
            if (!initialized) return ""
            return i18next.t(key, options)
        }

        override fun changeLanguage(language: LanguageCode) {
            i18next.changeLanguage(language)
        }

        override fun forContext(language: LanguageCode?, namespace: String?): LocalizationProvider = when {
            language != null -> I18NextLanguageFixedT(
                provider = this,
                language = language,
                fixedT = i18next.getFixedT(language = language, namespace = namespace)
            )

            else -> I18NextFixedT(
                this,
                fixedT = i18next.getFixedT(namespace = namespace)
            )
        }
    }

    private class I18NextFixedT(
        private val provider: I18NextState,
        private val fixedT: JsFunction2<String, dynamic, String>
    ) : LocalizationProvider by provider {
        override fun get(key: String, options: dynamic): String {
            if (!provider.initialized) return ""
            return fixedT(key, options)
        }
    }

    private class I18NextLanguageFixedT(
        private val provider: I18NextState,
        override val language: LanguageCode,
        private val fixedT: JsFunction2<String, dynamic, String>
    ) : LocalizationProvider by provider {
        override val languages: List<LanguageCode> get() = listOf(language)

        override fun get(key: String, options: dynamic): String {
            if (!provider.initialized) return ""
            return fixedT(key, options)
        }
    }
}
