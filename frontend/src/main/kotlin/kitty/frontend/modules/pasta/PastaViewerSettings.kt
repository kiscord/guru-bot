package kitty.frontend.modules.pasta

import androidx.compose.runtime.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.frontend.ui.rich_text.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*

@Composable
fun PastaViewerSettings(controller: PastaViewerController) {
    var isActive by remember { mutableStateOf(false) }

    Div(attrs = {
        classes("pasta-viewer-settings")
    }) {
        Div(attrs = {
            classes("dropdown", "is-right")
            if (isActive) {
                classes("is-active")
            }
        }) {
            Div(attrs = {
                classes("dropdown-trigger")
            }) {
                Button(attrs = {
                    classes("button", "is-outlined", "is-info")
                    attr("aria-haspopup", "true")
                    attr("aria-controls", "pasta-viewer-settings")
                    onClick {
                        isActive = !isActive
                    }
                }) {
                    MDIIcon("cog", containerSize = IconSize.Small)
                }
            }
            Div(attrs = {
                id("pasta-viewer-settings")
                classes("dropdown-menu")
                attr("role", "menu")
            }) {
                Div(attrs = {
                    classes("dropdown-content")
                }) {
                    Div(attrs = {
                        classes("dropdown-item")
                    }) {
                        Field(l10n("settings.color_palette")) {
                            Control {
                                Div(attrs = {
                                    classes("select")
                                }) {
                                    Select(attrs = {
                                        onChange { event ->
                                            controller.palette =
                                                event.value?.let { enumValueOf<RichTextColorPalette>(it) }
                                                    ?: RichTextColorPalette.BrowserDefault
                                        }
                                    }) {
                                        RichTextColorPalette.values().forEach { palette ->
                                            Option(palette.name, attrs = {
                                                if (controller.palette == palette) {
                                                    selected()
                                                }
                                            }) {
                                                Text(palette.displayName)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Div(attrs = {
                        classes("dropdown-item")
                    }) {
                        Field(l10n("settings.renderer")) {
                            Control {
                                Div(attrs = {
                                    classes("select")
                                }) {
                                    Select(attrs = {
                                        onChange { event ->
                                            controller.renderer =
                                                event.value?.let { enumValueOf<PastaViewerController.Renderer>(it) }
                                                    ?: PastaViewerController.Renderer.Compose
                                        }
                                    }) {
                                        PastaViewerController.Renderer.values().forEach { renderer ->
                                            Option(renderer.name, attrs = {
                                                if (controller.renderer == renderer) {
                                                    selected()
                                                }
                                            }) {
                                                Text(renderer.displayName)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Div(attrs = {
                        classes("dropdown-item")
                    }) {
                        Div(attrs = {
                            classes("field")
                        }) {
                            var fontSize by remember { mutableStateOf(controller.fontSizeInPt) }
                            remember(controller.fontSizeInPt) {
                                fontSize = controller.fontSizeInPt
                            }

                            Label(attrs = {
                                classes("label", "font-size-output")
                            }) {
                                Span {
                                    Text(l10n("settings.font_size"))
                                }
                                Output(attrs = {
                                    classes("is-info")
                                    attr("for", "pasta-settings-font-size")
                                    style {
                                        width(4.ch + 1.6.cssRem)
                                    }
                                }) {
                                    Text(l10n("settings.font_size.label") {
                                        font_size = fontSize
                                    })
                                }
                            }

                            Control {
                                Input(InputType.Range) {
                                    id("pasta-settings-font-size")
                                    classes("slider", "is-info", "is-rounded")
                                    step(1)
                                    min("6")
                                    max("36")
                                    value(fontSize)
                                    onInput { event ->
                                        fontSize = event.value?.toInt() ?: 14
                                    }
                                    onChange { event ->
                                        fontSize = (event.value?.toInt() ?: 14).also {
                                            controller.fontSizeInPt = it
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

