package kitty.frontend.modules.pasta

import androidx.compose.runtime.*
import kitty.frontend.*
import kitty.frontend.js.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.*
import org.kodein.di.compose.*
import org.w3c.files.*

class PastaOverview(
    di: DI,
) : DarkKittyRoot.Child(di) {
    val service: PastaService by instance()
}

@Composable
fun PastaOverviewUi(context: PastaOverview) {
    require("@/css/pasta.scss")

    val navigation: KittyNavigation by rememberInstance()

    Section(attrs = { classes("section", "is-main-section", "is-flex", "is-justify-content-center") }) {
        Div(attrs = {
            classes("card", "pasta-overview-card")
        }) {
            Div(attrs = {
                classes("card-content")
            }) {
                P(attrs = {
                    classes("title")
                }) {
                    Text("Pasta йопта")
                }
                P(attrs = {
                    classes("subtitle")
                }) {
                    Text("Pasta, ну чё блять нипанятно?")
                }

                var activeKind by remember { mutableStateOf(PastaUploadKind.File) }
                var uploadInProgress by remember { mutableStateOf(false) }
                var fileToUpload: File? by remember { mutableStateOf(null) }
                val fileToUploadStable = fileToUpload
                var textToUpload: String by remember { mutableStateOf("") }
                val textToUploadStable = textToUpload
                var urlToUpload: String by remember { mutableStateOf("") }

                if (uploadInProgress) when (activeKind) {
                    PastaUploadKind.File -> {
                        if (fileToUploadStable != null) LaunchedEffect(fileToUploadStable) {
                            val pastaFile = context.service.uploadFile(fileToUploadStable)
                            navigation.navigateTo(PastaConfig.View(pastaFile))
                        }
                    }

                    PastaUploadKind.Text -> {
                        LaunchedEffect(textToUploadStable) {
                            val pastaFile = context.service.uploadText(textToUploadStable)
                            navigation.navigateTo(PastaConfig.View(pastaFile))
                        }
                    }

                    PastaUploadKind.URL -> {

                    }
                }

                Div(attrs = {
                    classes("tabs", "is-fullwidth")
                    if (uploadInProgress) {
                        classes("is-disabled")
                    }
                }) {
                    Ul {
                        PastaUploadKind.values().forEach { kind ->
                            Li(attrs = {
                                if (kind == activeKind) {
                                    classes("is-active")
                                }
                            }) {
                                A(attrs = {
                                    onClick { activeKind = kind }
                                }) {
                                    when (kind) {
                                        PastaUploadKind.File -> {
                                            MDIIcon("file")
                                            Span { Text(l10n("upload.kind.file")) }
                                        }

                                        PastaUploadKind.Text -> {
                                            MDIIcon("clipboard-text")
                                            Span { Text(l10n("upload.kind.text")) }
                                        }

                                        PastaUploadKind.URL -> {
                                            MDIIcon("link")
                                            Span { Text(l10n("upload.kind.url")) }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                when (activeKind) {
                    PastaUploadKind.File -> {
                        Div(attrs = {
                            classes("file", "is-fullwidth")
                            if (fileToUploadStable != null) {
                                classes("has-name")
                            }
                        }) {
                            Label(attrs = {
                                classes("file-label")
                            }) {
                                Input(InputType.File) {
                                    classes("file-input")
                                    name("file")
                                    accept("text/*,.txt,.log")
                                    onChange {
                                        fileToUpload = it.target.files?.get(0)
                                    }
                                }
                                Span(attrs = {
                                    classes("file-cta")
                                }) {
                                    Span(attrs = {
                                        classes("file-icon")
                                    }) {
                                        MDIIcon("file")
                                    }
                                    Span(attrs = {
                                        classes("file-label")
                                    }) {
                                        Text(l10n("upload.choose_a_file"))
                                    }
                                }
                                if (fileToUploadStable != null) Span(attrs = {
                                    classes("file-name")
                                }) {
                                    Text(fileToUploadStable.name)
                                }
                            }
                        }

                        Span { Text("тут кароч текст какой-то будет") }
                    }

                    PastaUploadKind.Text -> {
                        val placeholder = l10n("upload.text.placeholder")
                        TextArea {
                            classes("textarea")
                            rows(10)
                            placeholder(placeholder)
                            value(textToUpload)
                            onInput {
                                console.log(it.value)
                                textToUpload = it.value
                            }
                        }
                    }

                    PastaUploadKind.URL -> {
                        Input(InputType.Url) {
                            classes("input")
                            value(urlToUpload)
                            onInput {
                                urlToUpload = it.value
                            }
                        }
                    }
                }

                Field(attrs = {
                    classes("mt-3")
                }) {
                    Control {
                        Button(attrs = {
                            classes("button", "is-primary")
                            if (uploadInProgress) {
                                classes("is-loading")
                                disabled()
                            }
                            onClick {
                                uploadInProgress = true
                            }
                        }) {
                            Text("Submit")
                        }
                    }
                }
            }
        }
    }
}

private enum class PastaUploadKind {
    File, Text, URL,
}
