package kitty.frontend.modules.permissions

import com.arkivanov.essenty.parcelable.*
import io.ktor.resources.*
import kiscord.api.*
import kitty.logic.*
import kitty.resources.*
import kotlinx.serialization.*
import org.kodein.di.*

@Parcelize
@Serializable
@Resource("/permissions")
data class PermissionsConfig(
    override val parent: GuildOverview,
) : KittyRootComponent.Config(), GuildScoped {
    constructor(guildId: Snowflake) : this(parent = GuildOverview(guildId = guildId))

    override val guildId: Snowflake get() = parent.guildId

    override fun createChild(di: DI) = PermissionsGuildOverview(di, parent.guildId)
}
