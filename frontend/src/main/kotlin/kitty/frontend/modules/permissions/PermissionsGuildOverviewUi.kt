package kitty.frontend.modules.permissions

import androidx.compose.runtime.*
import kitty.*
import kitty.frontend.*
import kitty.frontend.js.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import kitty.logic.services.*
import kitty.modules.faq.*
import kitty.resources.*
import io.ktor.http.*
import io.ktor.utils.io.charsets.*
import kiscord.api.*
import kotlinx.coroutines.*
import kotlinx.serialization.*
import kotlinx.serialization.cbor.*
import kotlinx.serialization.json.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.dom.*
import org.khronos.webgl.*
import org.kodein.di.*
import org.kodein.di.compose.*
import org.w3c.files.*
import kitty.frontend.l10n.*
import org.jetbrains.compose.web.css.*

class PermissionsGuildOverview(
    di: DI,
    override val guildId: Snowflake,
) : DarkKittyRoot.Child(di), GuildScoped

@Composable
fun PermissionsGuildOverviewUi(context: PermissionsGuildOverview) {
    AuthPredicate { it.isPotentiallyAuthenticated }
}

