package kitty.frontend.modules.permissions

import kitty.frontend.*
import kitty.frontend.l10n.*
import kitty.modules.permissions.*
import org.kodein.di.*

private const val L10N_NAMESPACE = "module_permissions"

val PermissionsModule = DI.Module(PermissionsModuleName.name) {
    bindGuildEntrypoint {
        moduleName(PermissionsModuleName)
        l10nNamespace(L10N_NAMESPACE)
        config { guildId ->
            PermissionsConfig(guildId = guildId)
        }
    }

    bindNamespacedPage<PermissionsConfig, PermissionsGuildOverview>(L10N_NAMESPACE) {
        title { l10n("navigation.permissions") }
        renderer { child ->
            PermissionsGuildOverviewUi(child)
        }
    }
}
