package kitty.frontend.modules.faq

import androidx.compose.runtime.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.modules.faq.*
import io.ktor.http.*
import kiscord.api.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*

@Composable
fun FaqPageEditor(
    page: FaqPageDTO? = null,
    isSaveInProgress: Boolean,
    onSave: (FaqPageDTO) -> Unit,
): State<FaqPageDTO> {
    var text by remember(page) { mutableStateOf(page?.text ?: "") }
    val embed by remember(page) { mutableStateOf(EmbedState(page?.embed ?: Embed.builder())) }
    val aliases = remember(page) { page?.aliases.orEmpty().map { Tag(it) }.toMutableStateList() }

    return FaqPageEditor(
        isNew = page == null,
        text = text,
        onTextChanged = { text = it },
        embed = embed,
        aliases = aliases,
        mainAlias = aliases.firstOrNull(),
        onAliasCreated = { if (it !in aliases) aliases += it },
        onAliasDeleted = { aliases -= it },
        onMainAliasChanged = {
            aliases.remove(it)
            aliases.add(0, it)
        },
        isSaveInProgress = isSaveInProgress,
        onSave = onSave,
    )
}

@Composable
fun FaqPageEditor(
    isNew: Boolean,
    text: String,
    onTextChanged: (String) -> Unit,
    embed: EmbedState,
    aliases: Collection<Tag>,
    mainAlias: Tag?,
    onAliasCreated: (Tag) -> Unit,
    onAliasDeleted: (Tag) -> Unit,
    onMainAliasChanged: (Tag) -> Unit,
    isSaveInProgress: Boolean,
    onSave: (FaqPageDTO) -> Unit,
): State<FaqPageDTO> {
    val pageResult = derivedStateOf {
        FaqPageDTO(
            text = text,
            embed = Embed.Builder(embed).build(),
            aliases = aliases.map { it.value },
        )
    }

    HorizontalField(l10n("management.page.editor.field.title")) {
        Field {
            Control {
                val placeholder = l10n("management.page.editor.field.title.placeholder")
                Input(InputType.Text) {
                    classes("input", "is-rounded")
                    maxLength(256)
                    placeholder(placeholder)
                    value(embed.title ?: "")
                    onInput { embed.title = it.value }
                }
            }
        }
    }
    HorizontalField(l10n("management.page.editor.field.tags")) {
        Field {
            Control {
                TagsInput(
                    tags = aliases,
                    selected = mainAlias?.let { setOf(it) } ?: emptySet(),
                    placeholder = l10n("management.page.editor.field.tags.placeholder"),
                    onSelect = {
                        onMainAliasChanged(it)
                    },
                    onDelete = {
                        onAliasDeleted(it)
                    },
                    onCreate = {
                        onAliasCreated(it)
                    }
                )
            }
        }
    }
    HorizontalField(l10n("management.page.editor.field.text")) {
        Field {
            Control {
                val placeholder = l10n("management.page.editor.field.text.placeholder")
                TextArea {
                    classes("textarea", "is-rounded")
                    placeholder(placeholder)
                    value(text)
                    onInput { onTextChanged(it.value) }
                }
            }
        }
    }
    HorizontalField(l10n("management.page.editor.field.embed.description")) {
        Field {
            Control {
                val placeholder = l10n("management.page.editor.field.embed.description.placeholder")
                TextArea {
                    classes("textarea", "is-rounded")
                    maxLength(4096)
                    placeholder(placeholder)
                    value(embed.description ?: "")
                    onInput { embed.description = it.value }
                }
            }
        }
    }
    var colorEnabled by remember { mutableStateOf(embed.color != null) }
    HorizontalFieldWithSwitch(
        colorEnabled, { colorEnabled = it },
        l10n("management.page.editor.field.embed.color")
    ) {
        var editorColor by remember { mutableStateOf(embed.color ?: 0) }
        DisposableEffect(embed, colorEnabled, editorColor) {
            embed.color = if (colorEnabled) editorColor else null
            onDispose {}
        }
        Field {
            Control {
                Input(InputType.Color) {
                    classes("input", "is-rounded")
                    value(hexColor(editorColor).toString())
                    onInput {
                        editorColor = parseHexColor(Color(it.value))
                    }
                    if (!colorEnabled) {
                        disabled()
                    }
                }
            }
        }
    }

    var urlEnabled by remember { mutableStateOf(embed.url != null) }
    HorizontalFieldWithSwitch(
        urlEnabled, { urlEnabled = it },
        l10n("management.page.editor.field.embed.url")
    ) {
        var editorUrl by remember { mutableStateOf(embed.url?.toString()) }
        DisposableEffect(embed, urlEnabled, editorUrl) {
            embed.url = if (urlEnabled) editorUrl?.let { tryParseUrl(it) } else null
            onDispose {}
        }
        Field {
            Control {
                val placeholder = l10n("management.page.editor.field.embed.url.placeholder")
                Input(InputType.Url) {
                    classes("input", "is-rounded")
                    placeholder(placeholder)
                    value(editorUrl ?: "")
                    onInput { editorUrl = it.value }
                    if (!urlEnabled) {
                        disabled()
                    }
                }
            }
        }
    }

    HorizontalField(l10n("management.page.editor.field.fields")) {
        Field {
            embed.fields.forEach { field ->
                key(field) {
                    Control(attrs = {
                        classes("faq-page-field")
                    }) {
                        Field {
                            Control {
                                val placeholder = l10n("management.page.editor.field.fields.name.placeholder")
                                Input(InputType.Text) {
                                    classes("input", "is-rounded", "faq-page-field-name")
                                    maxLength(256)
                                    placeholder(placeholder)
                                    value(field.name)
                                    onInput { field.name = it.value }
                                }
                            }
                        }
                        Field {
                            Control {
                                val placeholder = l10n("management.page.editor.field.fields.value.placeholder")
                                TextArea {
                                    classes("textarea", "is-rounded", "faq-page-field-value")
                                    maxLength(1024)
                                    placeholder(placeholder)
                                    value(field.value)
                                    onInput { field.value = it.value }
                                }
                            }
                        }
                        Field(attrs = {
                            classes(
                                "is-flex",
                                "is-flex-direction-row",
                                "is-justify-content-space-between",
                                "mt-1"
                            )
                        }) {
                            Switch(
                                field.inline, { field.inline = it },
                                l10n("management.page.editor.field.fields.inline")
                            )
                            Button(attrs = {
                                classes("button", "is-danger", "is-rounded", "is-outlined")
                                onClick { embed.fields -= field }
                            }) {
                                MDIIcon("delete")
                                Span { Text(l10n("management.page.editor.field.fields.actions.delete")) }
                            }
                        }
                    }
                }
            }
            if (embed.fields.size < 25) Control {
                Button(attrs = {
                    classes("button", "is-primary", "is-rounded")
                    onClick {
                        embed.fields += EmbedState.Field(
                            Embed.Field(
                                name = "",
                                value = "",
                                inline = false,
                            )
                        )
                    }
                }) {
                    MDIIcon("plus")
                    Span { Text(l10n("management.page.editor.field.fields.actions.add")) }
                }
            }
        }
    }
    var footerEnabled by remember { mutableStateOf(embed.footer != null) }
    HorizontalFieldWithSwitch(footerEnabled, { footerEnabled = it }, l10n("management.page.editor.field.footer")) {
        val editorFooter by remember {
            mutableStateOf(embed.footer ?: EmbedState.Footer(Embed.Footer(text = "")))
        }
        DisposableEffect(embed, footerEnabled, editorFooter) {
            embed.footer = if (footerEnabled) editorFooter else null
            onDispose {}
        }
        Field {
            Control(attrs = {
                classes("faq-page-footer")
            }) {
                Field {
                    Control {
                        val placeholder = l10n("management.page.editor.field.footer.text.placeholder")
                        TextArea {
                            classes("textarea", "is-rounded", "faq-page-footer-text")
                            maxLength(2048)
                            placeholder(placeholder)
                            value(editorFooter.text)
                            onInput { editorFooter.text = it.value }
                            if (!footerEnabled) {
                                disabled()
                            }
                        }
                    }
                }
                Field {
                    Control {
                        var footerIconUrl by remember { mutableStateOf(editorFooter.iconUrl?.toString()) }
                        DisposableEffect(embed.footer, footerIconUrl) {
                            val footer = embed.footer ?: return@DisposableEffect onDispose {}
                            footer.iconUrl = footerIconUrl?.let { tryParseUrl(it) }
                            onDispose {}
                        }
                        val placeholder = l10n("management.page.editor.field.footer.icon_url.placeholder")
                        Input(InputType.Url) {
                            classes("input", "is-rounded", "faq-page-footer-icon-url")
                            placeholder(placeholder)
                            value(footerIconUrl ?: "")
                            onInput { footerIconUrl = it.value }
                            if (!footerEnabled) {
                                disabled()
                            }
                        }
                    }
                }
            }
        }
    }

    HorizontalField {
        Field(isGrouped = true) {
            Control {
                Button(attrs = {
                    classes("button", "is-info", "is-rounded")
                    type(ButtonType.Submit)
                    if (isSaveInProgress) {
                        classes("is-loading")
                    } else {
                        onClick {
                            onSave(pageResult.value)
                        }
                    }
                }) {
                    if (isNew) {
                        Text(l10n("management.page.editor.actions.create"))
                    } else {
                        Text(l10n("management.page.editor.actions.save"))
                    }
                }
            }
        }
    }

    return pageResult
}

private fun tryParseUrl(path: String): Url? {
    val url = try {
        Url(path)
    } catch (e: URLParserException) {
        return null
    }
    if (url.protocol != URLProtocol.HTTP && url.protocol != URLProtocol.HTTPS) return null
    if (url.host == "localhost") return null
    return url
}