package kitty.frontend.modules.faq

import androidx.compose.runtime.*
import io.ktor.http.*
import io.ktor.utils.io.charsets.*
import kiscord.api.*
import kitty.*
import kitty.frontend.*
import kitty.frontend.js.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import kitty.logic.services.*
import kitty.modules.faq.*
import kitty.resources.*
import kotlinx.coroutines.*
import kotlinx.serialization.*
import kotlinx.serialization.cbor.*
import kotlinx.serialization.json.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*
import org.khronos.webgl.*
import org.kodein.di.*
import org.kodein.di.compose.*
import org.w3c.files.*

class FaqGuildOverview(
    di: DI,
    override val guildId: Snowflake,
) : DarkKittyRoot.Child(di), GuildScoped

@OptIn(ExperimentalSerializationApi::class)
@Composable
fun FaqGuildOverviewUi(context: FaqGuildOverview) {
    AuthPredicate { it.isPotentiallyAuthenticated }

    val navigation: KittyNavigation by rememberInstance()
    val userService: UserService by rememberInstance()
    val faqService: FaqService by rememberInstance()
    val books = remember { mutableStateListOf<FaqBookDTO>() }
    var booksLoading by remember { mutableStateOf(true) }
    val cbor: Cbor by rememberInstance()
    val json: Json by rememberInstance(tag = "pretty-json")

    LaunchedEffect(faqService, context.guildId) {
        booksLoading = true
        val newBooks = faqService.books(context.guildId)
        newBooks.collect {
            if (it == null) {
                booksLoading = true
                return@collect
            }
            books.clear()
            books.addAll(it)

            booksLoading = false
        }
    }

    var bookCreateDialogVisisble by remember { mutableStateOf(false) }

    Section(attrs = { classes("section", "is-main-section") }) {
        Div(attrs = { classes("card", "has-table") }) {
            Header(attrs = { classes("card-header") }) {
                P(attrs = { classes("card-header-title") }) {
                    MDIIcon("book", iconSize = IconSize.Small, containerSize = IconSize.Default)
                    Text(l10n("management.book.title"))
                }
                val createLabel = l10n("management.book.table.actions.create")
                A(attrs = {
                    classes("card-header-icon")
                    attr("title", createLabel)
                    onClick {
                        bookCreateDialogVisisble = true
                    }
                }) {
                    MDIIcon("plus")
                }
            }
            Div(attrs = { classes("card-content") }) {
                val bookDeleter = DeletionModalDialog<FaqBookDTO> {
                    deletionHandler { book ->
                        faqService.deleteBook(context.guildId, book.id ?: return@deletionHandler false)
                    }
                    onDeleted { book ->
                        books.remove(book)
                    }
                    confirmationSlug { book -> book.name }
                    l10n(
                        title = { l10n("management.book.delete.title") },
                        header = { book ->
                            l10n("management.book.delete.subtitle") {
                                book_name = book.name
                            }
                        },
                        prompt = { l10n("management.book.delete.prompt") },
                        confirmButton = { l10n("management.actions.confirm") },
                        cancelButton = { l10n("management.actions.cancel") },
                        deletionSucceed = { book ->
                            l10n("management.book.deleted.success") {
                                book_name = book.name
                            }
                        },
                        deletionFailed = { book ->
                            l10n("management.book.deleted.failed") {
                                book_name = book.name
                            }
                        }
                    )
                }

                val pageDeleter = DeletionModalDialog<FaqPageDTO> {
                    deletionHandler { page ->
                        faqService.deletePage(context.guildId, page.id ?: return@deletionHandler false)
                    }
                    onDeleted { page ->
                        books.forEachIndexed { index, book ->
                            if (page !in book.pages) return@forEachIndexed
                            books[index] = book.copy(
                                pages = book.pages - page
                            )
                        }
                    }
                    confirmationSlug { page -> page.title }
                    l10n(
                        title = { l10n("management.page.delete.title") },
                        header = { page ->
                            l10n("management.page.delete.subtitle") {
                                page_title = page.title
                            }
                        },
                        prompt = { l10n("management.page.delete.prompt") },
                        confirmButton = { l10n("management.actions.confirm") },
                        cancelButton = { l10n("management.actions.cancel") },
                        deletionSucceed = { page ->
                            l10n("management.page.deleted.success") {
                                page_title = page.title
                            }
                        },
                        deletionFailed = { page ->
                            l10n("management.page.deleted.failed") {
                                page_title = page.title
                            }
                        }
                    )
                }

                ProvideDismissHandle({
                    bookCreateDialogVisisble = false
                }) {
                    var prepopulateWith by remember { mutableStateOf<FaqBookDTO?>(null) }
                    val prepopulateWithStable = prepopulateWith
                    var prepopulateWithFile by remember { mutableStateOf<File?>(null) }
                    val prepopulateWithFileStable = prepopulateWithFile

                    if (prepopulateWithFileStable != null) LaunchedEffect(prepopulateWithFileStable) {
                        val type = ContentType.parse(prepopulateWithFileStable.type)
                        prepopulateWith = try {
                            when {
                                type != ContentType.Any && ContentType.Application.Cbor.match(type)
                                        || prepopulateWithFileStable.name.endsWith(".cbor") -> {
                                    cbor.decodeFromBlob<FaqBookDTO>(prepopulateWithFileStable)
                                }

                                type != ContentType.Any && ContentType.Application.Json.match(type)
                                        || prepopulateWithFileStable.name.endsWith(".json") -> {
                                    json.decodeFromBlob<FaqBookDTO>(prepopulateWithFileStable)
                                }

                                else -> null
                            }?.sanitize()
                        } catch(e: SerializationException) {
                            null
                        }
                    }

                    var bookName by remember { mutableStateOf("") }

                    ModalCard(bookCreateDialogVisisble) {
                        ModalCardHeader(l10n("Book creation"))
                        ModalCardBody {
                            Subtitle(l10n("management.book.create.subtitle"))
                            Field(l10n("management.book.create.fields.book_name")) {
                                Control {
                                    val placeholder = l10n("management.book.create.fields.book_name.placeholder")
                                    Input(InputType.Text) {
                                        classes("input")
                                        placeholder(placeholder)
                                        value(bookName)
                                        onInput { bookName = it.value }
                                    }
                                }
                            }
                            Subtitle(l10n("management.book.create.backup.subtitle"))
                            Columns {
                                Column(attrs = {
                                    classes("is-4")
                                }) {
                                    Div(attrs = {
                                        classes("file", "is-boxed", "is-fullwidth", "is-fullheight")
                                        if (prepopulateWithStable != null) {
                                            classes("is-info")
                                        }
                                    }) {
                                        Label(attrs = {
                                            classes("file-label")
                                        }) {
                                            Input(InputType.File) {
                                                classes("file-input")
                                                accept(
                                                    listOf(
                                                        ".faq.json", ".faq.cbor",
                                                        ContentType.Application.Json, ContentType.Application.Cbor,
                                                    ).joinToString(",")
                                                )
                                                onInput { e ->
                                                    val files = e.target.files ?: return@onInput
                                                    prepopulateWithFile = files[0]
                                                }
                                            }
                                            Span(attrs = {
                                                classes("file-cta")
                                            }) {
                                                MDIIcon("upload", containerClass = "file-icon")
                                                Span(attrs = {
                                                    classes("file-label", "break-spaces")
                                                }) {
                                                    Text(l10n("management.book.create.backup.button"))
                                                }
                                            }
                                        }
                                    }
                                }
                                Column(attrs = {
                                    classes("is-align-self-center")
                                }) {
                                    if (prepopulateWithStable != null) {
                                        Button(attrs = {
                                            classes("delete")
                                            style {
                                                position(Position.Absolute)
                                                right(1.5.cssRem)
                                            }
                                            onClick {
                                                prepopulateWithFile = null
                                                prepopulateWith = null
                                            }
                                        })
                                        P {
                                            Text(l10n("management.book.details.name") {
                                                book_name = prepopulateWithStable.name
                                            })
                                        }

                                        P {
                                            Text(l10n("management.book.details.pages") {
                                                page_count = prepopulateWithStable.pages.size
                                            })
                                        }

                                        prepopulateWithStable.createdAt?.let { createdAt ->
                                            P {
                                                InlineText(l10n("management.book.details.created_at")) {
                                                    inline("{{created_at}}") {
                                                        DateTimeText(createdAt)
                                                    }
                                                }
                                            }
                                        }
                                        val author by remember(prepopulateWithStable.author) {
                                            userService.user(prepopulateWithStable.author)
                                        }.collectAsState()
                                        author?.let {
                                            P {
                                                Text(l10n("management.book.details.author") {
                                                    author_name = it.username
                                                })
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ModalCardFooter {
                            var bookToCreate by remember { mutableStateOf<FaqBookDTO?>(null) }
                            val bookToCreateStable = bookToCreate
                            val dismissHandle by rememberUpdatedState(DismissHandle.current)

                            if (bookToCreateStable != null) LaunchedEffect(bookToCreateStable) {
                                val book = faqService.createBook(context.guildId, bookToCreateStable)
                                books += book
                                dismissHandle.dismiss()
                            }

                            Button(attrs = {
                                classes("button", "is-primary", "is-outlined")
                                if (bookName.isBlank() && prepopulateWithStable == null) disabled()
                                onClick {
                                    var book = prepopulateWithStable ?: FaqBookDTO(name = bookName)
                                    if (bookName.isNotBlank()) {
                                        book = book.copy(name = bookName)
                                    }
                                    bookToCreate = book
                                }
                            }) {
                                Text(l10n("management.book.create.actions.create"))
                            }
                            Button(attrs = {
                                classes("button", "is-ghost")
                                onClick { dismissHandle.dismiss() }
                            }) {
                                Text(l10n("management.book.create.actions.cancel"))
                            }
                        }
                    }
                }

                var bookDetails by remember { mutableStateOf<Long?>(null) }

                ResponsiveTable(books, booksLoading, tableAttrs = {
                    classes("is-fullwidth", "is-striped", "is-hoverable")
                }) {
                    collapse(isExpanded = { it.id == bookDetails }, onToggle = { book ->
                        bookDetails = book.id.takeUnless { bookDetails == it }
                    }) { book ->
                        ResponsiveTable(book.pages.sortedBy { it.aliases.first() }, tableAttrs = {
                            classes("is-striped", "is-hoverable")
                        }) {
                            field({ l10n("management.page.table.alias") }) { page ->
                                Text(page.aliases.first())
                            }
                            field({ l10n("management.page.table.title") }) { page ->
                                Text(page.title)
                            }
                            actionButton({ l10n("management.page.table.actions.edit") }, attrs = { page ->
                                classes("is-small", "is-primary", "is-rounded")
                                onClick {
                                    val pageId = page.id ?: return@onClick
                                    navigation.navigateTo(FaqConfig.PageEdit(guildId = book.guildId, pageId = pageId))
                                }
                            }) {
                                MDIIcon("pencil", IconSize.Small)
                            }
                            actionButton({ l10n("management.page.table.actions.delete") }, attrs = { page ->
                                classes("is-small", "is-danger", "is-rounded")
                                onClick {
                                    pageDeleter.delete(page)
                                }
                            }) {
                                MDIIcon("delete", IconSize.Small)
                            }
                        }
                    }
                    field({ l10n("management.book.table.name") }) { book ->
                        Text(book.name)
                    }
                    field({ l10n("management.book.table.pages") }) { book ->
                        Text("${book.pages.size}")
                    }
                    field({ l10n("management.book.table.author") }) { book ->
                        val author by book.author.let { author ->
                            remember(userService, author) { userService.user(author) }
                        }.collectAsState()

                        val authorStable = author
                        if (authorStable == null) {
                            Text("...")
                        } else {
                            Text(authorStable.username)
                        }
                    }
                    field({ l10n("management.book.table.created_at") }) { book ->
                        book.createdAt?.let { createdAt ->
                            DateTimeText(createdAt, attrs = { classes("has-text-grey") })
                        }
                    }
                    actionButton({ l10n("management.page.table.actions.create") }, attrs = { book ->
                        classes("is-small", "is-primary", "is-rounded")
                        onClick {
                            navigation.navigateTo(
                                FaqConfig.PageCreate(guildId = context.guildId, bookId = book.id!!)
                            )
                        }
                    }) {
                        MDIIcon("plus", IconSize.Small)
                    }
                    actionButton({ l10n("management.book.table.actions.export") }, attrs = { book ->
                        classes("is-small", "is-info", "is-rounded")
                        onClick { e ->
                            if (e.shiftKey) {
                                val string = json.encodeToString(book.sanitize())
                                val blob = Blob(
                                    arrayOf(string), BlobPropertyBag(
                                        type = ContentType.Application.Json.withCharset(Charsets.UTF_8).toString()
                                    )
                                )
                                saveAs(blob, "${book.name}.faq.json")
                            } else {
                                val bytes = cbor.encodeToByteArray(book.sanitize())
                                val blob = Blob(
                                    arrayOf(bytes), BlobPropertyBag(
                                        type = ContentType.Application.Cbor.toString()
                                    )
                                )
                                saveAs(blob, "${book.name}.faq.cbor")
                            }
                        }
                    }) {
                        MDIIcon("export", IconSize.Small)
                    }
                    actionButton({ l10n("management.book.table.actions.delete") }, attrs = { book ->
                        classes("is-small", "is-danger", "is-rounded")
                        onClick { bookDeleter.delete(book) }
                    }) {
                        MDIIcon("delete", IconSize.Small)
                    }

                    listOf(
                        "Open-architected full-range migration",
                        "Decentralized 4th generation knowledge user",
                        "Upgradable intangible artificial intelligence",
                    ).forEachIndexed { index, s ->
                        fake(
                            FaqBookDTO(
                                id = index.toLong(),
                                name = s,
                                pages = listOf(),
                                author = Snowflake(123824611212263424UL)
                            )
                        )
                    }
                }
            }
        }
    }
}

private val FaqPageDTO.title: String
    get() = embed.title ?: "Untitled"
