package kitty.frontend.modules.faq

import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.*
import io.ktor.http.*
import kiscord.api.*
import kotlinx.datetime.*

@Stable
class EmbedState(spec: EmbedSpec) : EmbedSpec {
    override var author: Author? by mutableStateOf(spec.author?.let { Author(it) })
    override var color: Int? by mutableStateOf(spec.color)
    override var description: String? by mutableStateOf(spec.description)
    override val fields: SnapshotStateList<Field> = spec.fields.orEmpty().mapTo(SnapshotStateList()) { Field(it) }
    override var footer: Footer? by mutableStateOf(spec.footer?.let { Footer(it) })
    override var image: Image? by mutableStateOf(spec.image?.let { Image(it) })
    override var provider: Provider? by mutableStateOf(spec.provider?.let { Provider(it) })
    override var thumbnail: Embed.ThumbnailSpec? by mutableStateOf(spec.thumbnail?.let { Thumbnail(it) })
    override var timestamp: Instant? by mutableStateOf(spec.timestamp)
    override var title: String? by mutableStateOf(spec.title)
    override var type: String? by mutableStateOf(spec.type)
    override var url: Url? by mutableStateOf(spec.url)
    override var video: Video? by mutableStateOf(spec.video?.let { Video(it) })

    @Stable
    class Author(spec: Embed.AuthorSpec) : Embed.AuthorSpec {
        override var iconUrl: Url? by mutableStateOf(spec.iconUrl)
        override var name: String by mutableStateOf(spec.name)
        override var proxyIconUrl: Url? by mutableStateOf(spec.proxyIconUrl)
        override var url: Url? by mutableStateOf(spec.url)
    }

    @Stable
    class Field(spec: Embed.FieldSpec) : Embed.FieldSpec {
        override var inline: Boolean by mutableStateOf(spec.inline)
        override var name: String by mutableStateOf(spec.name)
        override var value: String by mutableStateOf(spec.value)
    }

    @Stable
    class Footer(spec: Embed.FooterSpec) : Embed.FooterSpec {
        override var iconUrl: Url? by mutableStateOf(spec.iconUrl)
        override var proxyIconUrl: Url? by mutableStateOf(spec.proxyIconUrl)
        override var text: String by mutableStateOf(spec.text)
    }

    @Stable
    class Image(spec: Embed.ImageSpec) : Embed.ImageSpec {
        override var height: Int? by mutableStateOf(spec.height)
        override var proxyUrl: Url? by mutableStateOf(spec.proxyUrl)
        override var url: Url by mutableStateOf(spec.url)
        override var width: Int? by mutableStateOf(spec.width)
    }

    @Stable
    class Thumbnail(spec: Embed.ThumbnailSpec) : Embed.ThumbnailSpec {
        override var height: Int? by mutableStateOf(spec.height)
        override var proxyUrl: Url? by mutableStateOf(spec.proxyUrl)
        override var url: Url by mutableStateOf(spec.url)
        override var width: Int? by mutableStateOf(spec.width)
    }

    @Stable
    class Video(spec: Embed.VideoSpec) : Embed.VideoSpec {
        override var height: Int? by mutableStateOf(spec.height)
        override var proxyUrl: Url? by mutableStateOf(spec.proxyUrl)
        override var url: Url? by mutableStateOf(spec.url)
        override var width: Int? by mutableStateOf(spec.width)
    }

    @Stable
    class Provider(spec: Embed.ProviderSpec) : Embed.ProviderSpec {
        override var name: String? by mutableStateOf(spec.name)
        override var url: Url? by mutableStateOf(spec.url)
    }
}