package kitty.frontend.modules.faq

import androidx.compose.runtime.*
import kitty.frontend.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import kitty.logic.services.*
import kitty.modules.faq.*
import kitty.resources.*
import kiscord.api.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.*
import org.kodein.di.compose.*

class FaqCreatePageChild(
    di: DI,
    override val guildId: Snowflake,
    val bookId: Long,
) : DarkKittyRoot.Child(di), GuildScoped

@Composable
fun FaqCreatePageUi(context: FaqCreatePageChild) {
    AuthPredicate { it.isPotentiallyAuthenticated }

    val navigation: KittyNavigation by rememberInstance()
    val miscService: MiscService by rememberInstance()
    val faqService: FaqService by rememberInstance()

    val botProfile by miscService.botProfile.collectAsState()

    var pagePreview by remember { mutableStateOf(FaqPageDTO(
        text = "",
        embed = Embed.builder().build(),
        aliases = emptyList(),
    )) }

    Section(attrs = {
        classes("section", "is-main-section")
    }) {
        Columns {
            Column {
                Div(attrs = {
                    classes("card")
                }) {
                    Header(attrs = {
                        classes("card-header")
                    }) {
                        P(attrs = {
                            classes("card-header-title")
                        }) {
                            MDIIcon("circle-edit-outline", iconSize = IconSize.Small, containerSize = IconSize.Default)
                            Text(l10n("management.page.editor.title"))
                        }
                    }
                    Div(attrs = {
                        classes("card-content")
                    }) {
                        var pageResult by remember { mutableStateOf<FaqPageDTO?>(null) }
                        val pageResultStable = pageResult

                        pagePreview = FaqPageEditor(
                            isSaveInProgress = pageResult != null,
                            onSave = { pageResult = it }).value

                        LaunchedEffect(pageResultStable) {
                            if (pageResultStable == null) return@LaunchedEffect
                            val newPage = faqService.createPage(context.guildId, context.bookId, pageResultStable)
                            pageResult = null
                            navigation.navigateTo(FaqConfig.PageEdit(guildId = context.guildId, pageId = newPage.id!!))
                        }
                    }
                }
            }

            Column(attrs = {
                classes("is-5")
            }) {
                DiscordScreen(isSticky = true) {
                    DiscordMessagePreview {
                        DiscordAvatar(botProfile)
                        DiscordMessage {
                            DiscordMessageName(botProfile)
                            DiscordMessageContent(pagePreview.text)
                            DiscordEmbed(pagePreview.embed)
                        }
                    }
                }
            }
        }
    }
}