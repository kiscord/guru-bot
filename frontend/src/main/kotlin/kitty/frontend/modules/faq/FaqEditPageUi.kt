package kitty.frontend.modules.faq

import androidx.compose.runtime.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import kitty.logic.services.*
import kitty.modules.faq.*
import kitty.resources.*
import kiscord.api.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.*
import org.kodein.di.compose.*

class FaqEditPageChild(
    di: DI,
    override val guildId: Snowflake,
    val pageId: Long,
) : DarkKittyRoot.Child(di), GuildScoped

@Composable
fun FaqEditPageUi(context: FaqEditPageChild) {
    AuthPredicate { it.isPotentiallyAuthenticated }

    val miscService: MiscService by rememberInstance()
    val faqService: FaqService by rememberInstance()

    val botProfile by miscService.botProfile.collectAsState()

    val pageFlow = remember(faqService) {
        faqService.page(guildId = context.guildId, pageId = context.pageId)
    }
    val page by pageFlow.collectAsState()
    val pageStable = page

    Loader(pageStable == null)
    if (pageStable == null) return

    var pagePreview by remember { mutableStateOf(pageStable) }

    Section(attrs = {
        classes("section", "is-main-section")
    }) {
        Columns {
            Column {
                Div(attrs = {
                    classes("card")
                }) {
                    Header(attrs = {
                        classes("card-header")
                    }) {
                        P(attrs = {
                            classes("card-header-title")
                        }) {
                            MDIIcon("circle-edit-outline", iconSize = IconSize.Small, containerSize = IconSize.Default)
                            Text(l10n("management.page.editor.title"))
                        }
                    }
                    Div(attrs = {
                        classes("card-content")
                    }) {
                        var pageResult by remember { mutableStateOf<FaqPageDTO?>(null) }
                        val pageResultStable = pageResult

                        pagePreview = FaqPageEditor(
                            pageStable,
                            isSaveInProgress = pageResult != null,
                            onSave = { pageResult = it }).value

                        LaunchedEffect(pageResultStable) {
                            if (pageResultStable == null) return@LaunchedEffect
                            val newPage = faqService.savePage(context.guildId, context.pageId, pageResultStable)
                            pageFlow.flush(with = newPage)
                            pageResult = null
                        }
                    }
                }
            }

            Column(attrs = {
                classes("is-5")
            }) {
                DiscordScreen(isSticky = true) {
                    DiscordMessagePreview {
                        DiscordAvatar(botProfile)
                        DiscordMessage {
                            DiscordMessageName(botProfile)
                            DiscordMessageContent(pagePreview.text)
                            DiscordEmbed(pagePreview.embed)
                        }
                    }
                }
            }
        }
    }
}

