package kitty.frontend

import androidx.compose.runtime.*
import com.arkivanov.decompose.*
import com.arkivanov.decompose.router.stack.*
import com.arkivanov.decompose.router.stack.webhistory.*
import com.arkivanov.essenty.lifecycle.*
import kitty.*
import kitty.frontend.js.*
import kitty.frontend.l10n.*
import kitty.frontend.modules.faq.*
import kitty.frontend.modules.pasta.*
import kitty.frontend.modules.permissions.*
import kitty.frontend.ui.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import kitty.logic.services.*
import kitty.modules.faq.*
import kitty.resources.*
import kotlinx.browser.*
import kotlinx.coroutines.flow.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import kotlinx.serialization.json.Json
import org.kodein.di.*
import org.kodein.di.compose.*
import org.w3c.dom.*
import kotlin.js.*
import kotlin.reflect.*

private val KittyModuleName = ModuleScoped.Name("kitty-frontend")

val KittyModule = DI.Module(KittyModuleName.name) {
    require("@/css/styles.scss")

    importOnce(KittyCommonModule)
    importOnce(HttpClientModule)
    importOnce(ServiceWorkerModule)

    bindSingleton {
        LifecycleRegistry().apply {
            subscribe(LifecycleLogger(instance()))

            attachToDocument()
        }
    }
    delegate<Lifecycle>().to<LifecycleRegistry>()

    bindSingleton {
        val pages =
            instance<Set<KittyDecomposedPage<out KittyRootComponent.Config, out DarkKittyRoot.Child>>>(tag = "frontend-pages")
        ResourcesRouter(instance(), pages.mapTo(mutableSetOf()) { it.configSerializer })
    }

    bindSingleton<ServiceIO> { RestServiceIO(di) }
    bindSingleton<UserService> { UserService.Default(instance()) }
    bindSingleton<MiscService> { MiscService.Default(instance()) }

    bindSet<KittyDecomposedPage<out KittyRootComponent.Config, out DarkKittyRoot.Child>>(tag = "frontend-pages")

    bindPage<KittyRootComponent.Config.Overview, DarkKittyRoot.Child.Overview> {
        title { l10n("navigation.overview") }
        renderer { child ->
            OverviewUi(child)
        }
    }
    bindPage<KittyRootComponent.Config.GuildSelector, DarkKittyRoot.Child.GuildSelector> {
        title { l10n("navigation.guild_selector") }
        renderer { child ->
            GuildSelectorUi(child)
        }
    }
    bindPage<KittyRootComponent.Config.Login, DarkKittyRoot.Child.Login> {
        title { l10n("navigation.login") }
        renderer { child ->
            LoginUi(child)
        }
    }
    bindPage<KittyRootComponent.Config.GuildOverview, DarkKittyRoot.Child.GuildOverview> {
        title { config ->
            val service: UserService by rememberInstance()
            val userGuilds by service.userGuilds.collectAsState()
            val guild = remember(userGuilds) { userGuilds.orEmpty().find { it.id == config.guildId } }
            l10n("navigation.guild_overview") { guild_name = guild?.name ?: "…" }
        }
        renderer { child ->
            GuildOverviewUi(child)
        }
    }

    bindFactory<DeepLink, KittyRootComponent.Config>(tag = "deep-link-parser") { deepLink ->
        val router: ResourcesRouter = instance()
        router.fromDeepLink(deepLink) as KittyRootComponent.Config? ?: KittyRootComponent.Config.Overview
    }

    bindSingleton {
        val deepLinkParser = factory<DeepLink, KittyRootComponent.Config>(tag = "deep-link-parser")
        KittyNavigation(deepLinkParser = deepLinkParser)
    }

    delegate<StackNavigation<KittyRootComponent.Config>>().to<KittyNavigation>()

    bindSingleton(tag = "root-component") {
        @OptIn(ExperimentalDecomposeApi::class)
        KittyRootComponent(
            di = di,
            componentContext = DefaultComponentContext(instance()),
            deepLink = DeepLink.Web(window.location.pathname + window.location.search),
            webHistoryController = DefaultWebHistoryController(),
        )
    }

    bindSingleton(tag = "login_redirect_url") {
        SerializedStorage(
            "login_redirect_url",
            DeepLink.serializer(),
            format = instance<Json>()
        )
    }

    bindSingleton<State<LocalizationProvider>> {
        I18NextLocalizationProvider(
            require.context("@/locales", false, RegExp("./(.+)_(.+)\\.json$")),
            loggerFactory = instance()
        ).state
    }

    bindSet<KittyEntrypoint>()

    bindGlobalEntrypoint {
        moduleName(KittyModuleName)
        l10nNamespace("translation")
        icon { MDIIcon("home") }
        config { KittyRootComponent.Config.Overview }
    }

    bindGlobalEntrypoint {
        moduleName(KittyModuleName)
        l10nNamespace("translation")
        icon { MDIIcon("server") }
        config { KittyRootComponent.Config.GuildSelector }
        predicate {
            val darkKittyAuthState: StateFlow<DarkKittyAuth> by rememberInstance(tag = "auth-flow")
            val darkKittyAuth by darkKittyAuthState.collectAsState()
            darkKittyAuth.isPotentiallyAuthenticated
        }
    }

    if (BuildConfig.MODULE_FAQ) importOnce(FaqModule)
    if (BuildConfig.MODULE_PASTA) importOnce(PastaModule)
    if (BuildConfig.MODULE_PERMISSIONS) importOnce(PermissionsModule)
}

private fun LifecycleRegistry.attachToDocument() {
    fun onVisibilityChanged() {
        if (document.visibilityState == "visible") {
            resume()
        } else {
            pause()
        }
    }

    onVisibilityChanged()

    document.addEventListener(type = "visibilitychange", callback = { onVisibilityChanged() })
}

private val Document.visibilityState: String get() = asDynamic().visibilityState.unsafeCast<String>()
