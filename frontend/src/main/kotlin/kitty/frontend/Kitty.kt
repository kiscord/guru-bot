package kitty.frontend

import kitty.frontend.ui.*
import kitty.logic.*
import org.jetbrains.compose.web.*
import org.kodein.di.*
import org.kodein.di.compose.*

fun main() {
    val di = DI {
        importOnce(KittyModule)
    }

    val rootComponent: KittyRootComponent by di.instance(tag = "root-component")

    renderComposableInBody {
        withDI(di) {
            KittyRootUi(rootComponent)
        }
    }
}
