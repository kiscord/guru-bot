package kitty.frontend.ui

import androidx.compose.runtime.*
import io.ktor.client.*
import io.ktor.client.plugins.resources.*
import io.ktor.resources.*
import io.ktor.resources.serialization.*
import kitty.*
import kitty.frontend.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.frontend.ui.blocks.navbar.*
import kitty.logic.*
import kitty.logic.services.*
import kitty.resources.*
import kotlinx.browser.*
import kotlinx.coroutines.flow.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.compose.*

@Composable
fun KittyNavigation(component: KittyRootComponent) {
    val httpClient: HttpClient by rememberInstance()
    val resourcesRouter: ResourcesRouter by rememberInstance()
    val resourcesFormat: ResourcesFormat by rememberInstance()
    val darkKittyAuthState: StateFlow<DarkKittyAuth> by rememberInstance(tag = "auth-flow")
    val darkKittyAuth by darkKittyAuthState.collectAsState()
    val loginRedirectUrl: SerializedStorage<DeepLink> by rememberInstance(tag = "login_redirect_url")
    val child by component.childStack.subscribeAsState()
    val userService: UserService by rememberInstance()
    val miscService: MiscService by rememberInstance()
    val botTitle by miscService.botTitle.collectAsState()
    val pages by rememberInstance<Set<KittyDecomposedPage<out KittyRootComponent.Config, out DarkKittyRoot.Child>>>(tag = "frontend-pages")

    @Composable
    fun title(config: KittyRootComponent.Config) = pages.title(config) ?: "<unknown>"

    fun startLogin(provider: String) {
        val redirectTo = when (val c = child.active.configuration) {
            KittyRootComponent.Config.Overview -> KittyRootComponent.Config.GuildSelector
            else -> c
        }
        loginRedirectUrl.set(resourcesRouter.toDeepLink(redirectTo))
        window.location.assign(href(resourcesFormat, OAuth2Resource.Login(provider = provider)))
    }

    val documentElement = document.documentElement
    var asideMenuOpened by remember { mutableStateOf(false) }

    DisposableEffect(documentElement, asideMenuOpened) {
        if (documentElement == null) return@DisposableEffect onDispose { }

        val classList = buildSet {
            add("has-aside-left")
            add("has-aside-mobile-transition")
            add("has-navbar-fixed-top")
            add("has-aside-expanded")
            if (asideMenuOpened) {
                add("has-aside-mobile-expanded")
            }
        }.toTypedArray()

        documentElement.classList.add(*classList)

        onDispose {
            documentElement.classList.remove(*classList)
        }
    }

    val pageTitle = "${botTitle.text} · ${title(child.active.configuration)}"
    remember(pageTitle) { document.title = pageTitle }

    NavBar(attrs = {
        classes("is-fixed-top")
    }) {
        var burgerOpen by remember { mutableStateOf(false) }

        NavBarBrand {
            NavBarItemAnchor(attrs = {
                classes("is-hidden-desktop", "jb-aside-mobile-toggle", "navbar-burger", "mr-auto", "ml-0")
                if (asideMenuOpened) classes("is-active")
                attr("role", "button")
                attr("aria-label", "menu")
                attr("aria-expanded", asideMenuOpened.toString())
                onClick {
                    asideMenuOpened = !asideMenuOpened
                }
            }) {
                repeat(3) {
                    Span(attrs = { attr("aria-hidden", "true") })
                }
            }
            NavBarBurger(burgerOpen, onToggle = { burgerOpen = !burgerOpen })
        }

        NavBarMenu(isOpen = burgerOpen) {
            NavBarEnd {
                Div(attrs = {
                    classes("navbar-item", "has-dropdown", "is-hoverable")
                }) {
                    NavBarLink(attrs = {
                        classes("is-arrowless")
                    }) {
                        IconWithText("translate", "Language", textAttrs = {
                            classes("is-hidden-desktop")
                        })
                    }

                    Div(attrs = {
                        classes("navbar-dropdown", "is-right")
                    }) {
                        val l10n = l10n

                        l10n.supportedLanguages.associateWith { language ->
                            Intl.DisplayNames(language, DisplayNamesOptions {
                                type = "language"
                                languageDisplay = "standard"
                            }).of(language).replaceFirstChar {
                                if (it.isLowerCase()) it.titlecase() else it.toString()
                            }
                        }.entries.sortedBy { (_, displayName) ->
                            displayName
                        }.forEach { (language, displayName) ->
                            NavBarItemAnchor(attrs = {
                                if (l10n.language == language) classes("is-active")
                                onClick { l10n.changeLanguage(language) }
                            }) {
                                Text(displayName)
                            }
                        }
                    }
                }

                when (val auth = darkKittyAuth) {
                    // TODO loading progress
                    DarkKittyAuth.None, DarkKittyAuth.Loading -> {
                        NavBarItemDiv {
                            Div(attrs = {
                                classes("buttons")
                            }) {
                                A(attrs = {
                                    classes("button", "is-primary")
                                    attr("rel", "opener")
                                    onClick { startLogin("discord") }
                                }) {
                                    Text(l10n("login"))
                                }
                            }
                        }
                    }

                    is DarkKittyAuth.Expired -> {
                        LaunchedEffect(auth) {
                            httpClient.get(OAuth2Resource.Check())
                        }

                        NavBarItemDiv {
                            Div(attrs = {
                                classes("buttons")
                            }) {
                                Button(attrs = {
                                    classes("button", "is-primary", "is-loading")
                                })
                            }
                        }
                    }

                    is DarkKittyAuth.Error -> {
                        NavBarItemDiv {
                            Div(attrs = {
                                classes("buttons")
                            }) {
                                A(attrs = {
                                    classes("button", "is-danger")
                                    attr("rel", "opener")
                                    onClick { startLogin("discord") }
                                }) {
                                    Text(l10n("login.error"))
                                }
                            }
                        }
                    }

                    is DarkKittyAuth.Authenticated -> {
                        Div(attrs = {
                            classes(
                                "navbar-item",
                                "has-dropdown",
                                "is-hoverable",
                                "has-dropdown-with-icons",
                                "has-divider",
                                "has-user-avatar"
                            )
                        }) {
                            val userProfile by userService.userProfile.collectAsState()

                            NavBarLink(attrs = {
                                classes("is-arrowless")
                            }) {
                                when (val userProfileStable = userProfile) {
                                    null -> MDIIcon("dots-horizontal")
                                    else -> {
                                        Div(attrs = {
                                            classes("is-user-avatar")
                                        }) {
                                            Img(src = userProfileStable.avatarUrl(32), alt = userProfileStable.username)
                                        }
                                        Div(attrs = {
                                            classes("is-user-name")
                                        }) {
                                            Span { Text(userProfileStable.username) }
                                        }
                                        MDIIcon("chevron-down")
                                    }
                                }
                            }

                            Div(attrs = {
                                classes("navbar-dropdown")
                            }) {
                                NavBarItemAnchor(attrs = {
                                    onClick { auth.logout() }
                                }) {
                                    IconWithText("logout", l10n("logout"))
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Aside(attrs = {
        classes("aside", "is-placed-left", "is-expanded")
    }) {
        Div(attrs = {
            classes("aside-tools")
        }) {
            Div(attrs = {
                classes("aside-tools-label", "is-flex", "is-flex-grow-1")
            }) {
                Span(attrs = {
                    classes("is-flex-grow-1")
                }) {
                    when (val botTitleStable = botTitle) {
                        is MiscService.BotTitle.Prefixed -> {
                            Text(botTitleStable.prefix)
                            B { Text(botTitleStable.suffix) }
                        }

                        else -> Text(botTitleStable.text)
                    }
                }
                Span(attrs = {
                    classes("is-flex-shrink-1", "version-label", "has-text-grey")
                }) { Text(BuildConfig.VERSION) }
            }
        }
        Div(attrs = {
            classes("menu", "is-menu-main")
        }) {
            P(attrs = {
                classes("menu-label")
            }) {
                Text(l10n("navigation.category.general"))
            }

            Ul(attrs = {
                classes("menu-list")
            }) {
                val entrypoints: Set<KittyEntrypoint> by rememberInstance()
                entrypoints.filterIsInstance<KittyEntrypoint.Global>().filter { it.predicate() }.forEach { entrypoint ->
                    key(entrypoint) {
                        val config = remember(entrypoint) { entrypoint.config() }
                        val isActive = config.isOneOfParentsFor(child.active.configuration)
                        val title = title(config)

                        Li(attrs = {
                            if (isActive) classes("is-active")
                        }) {
                            NavigationAnchor(config, attrs = {
                                if (isActive) classes("is-active", "router-link-active")
                                classes("has-icon")
                            }) {
                                ProvideIconSize(IconSize.Small) {
                                    entrypoint.icon()
                                }
                                Span(attrs = { classes("menu-item-label") }) { Text(title) }
                            }
                            if (isActive && entrypoint.nested.isNotEmpty()) {
                                Ul {
                                    entrypoint.nested.forEach { child ->
                                        key(child) {
                                            val childConfig = remember(child) { child.config() }
                                            val childTitle = title(childConfig)
                                            Li {
                                                NavigationAnchor(childConfig) {
                                                    Span(attrs = { classes("menu-item-label") }) { Text(childTitle) }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Section(attrs = {
        classes("section", "is-title-bar")
    }) {
        Div(attrs = {
            classes("level")
        }) {
            Div(attrs = {
                classes("level-left")
            }) {
                Div(attrs = {
                    classes("level-item")
                }) {
                    Ul(attrs = {
                        classes("navigation-breadcrumbs")
                    }) {
                        @Composable
                        fun navigationLevel(config: KittyRootComponent.Config) {
                            if (config.parent != config) navigationLevel(config.parent)
                            Li {
                                NavigationAnchor(config) {
                                    Text(title(config))
                                }
                            }
                        }
                        navigationLevel(child.active.configuration)
                    }
                }
            }
            Div(attrs = {
                classes("level-right")
            }) {
                Div(attrs = {
                    classes("level-item")
                }) {
                    Div(attrs = {
                        classes("buttons", "is-right")
                    }) {
                        A(href = KittyBuildConfig.VCS_URL, attrs = {
                            classes("button", "is-link")
                            target(ATarget.Blank)
                        }) {
                            MDIIcon("gitlab", IconSize.Small)
                            Span { Text("GitLab") }
                        }
                    }
                }
            }
        }
    }
}

private fun KittyRootComponent.Config.isOneOfParentsFor(active: KittyRootComponent.Config): Boolean {
    var node = active
    while (true) {
        if (this == node) return true
        val parent = node.parent
        if (parent === node) return false
        node = parent
    }
}

private external interface LanguageInfoMap

private operator fun LanguageInfoMap.get(language: LanguageCode): LanguageInfo? {
    return asDynamic()[language].unsafeCast<LanguageInfo?>()
}

private external interface LanguageInfo {
    val nativeName: String
    val englishName: String? get() = definedExternally
}

private external object Intl {
    class DisplayNames {
        constructor(locales: Array<LanguageCode>, options: DisplayNamesOptions = definedExternally)
        constructor(locale: LanguageCode, options: DisplayNamesOptions = definedExternally)

        fun of(value: String): String
    }
}

private external interface DisplayNamesOptions {
    var localeMatcher: String
    var style: String
    var type: String
    var languageDisplay: String
    var fallback: String
}

private inline fun DisplayNamesOptions(block: DisplayNamesOptions.() -> Unit): DisplayNamesOptions {
    return js("{}").unsafeCast<DisplayNamesOptions>().apply(block)
}

