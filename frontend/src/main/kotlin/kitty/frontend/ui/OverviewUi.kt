package kitty.frontend.ui

import androidx.compose.runtime.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import kitty.logic.services.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.compose.*

@Composable
fun OverviewUi(context: DarkKittyRoot.Child.Overview) {
    val miscService: MiscService by rememberInstance()
    val stats by miscService.stats.collectAsState()

    Section(attrs = {
        classes("hero", "is-hero-bar")
    }) {
        Div(attrs = {
            classes("hero-body")
        }) {
            Div(attrs = {
                classes("level")
            }) {
                Div(attrs = {
                    classes("level-left")
                }) {
                    Div(attrs = {
                        classes("level-item")
                    }) {
                        H1(attrs = {
                            classes("title")
                        }) {
                            Text(l10n("navigation.overview.title"))
                        }
                    }
                }
            }
        }
    }
    Section(attrs = {
        classes("section", "is-main-section")
    }) {
        Tile(isAncestor = true) {
            @Composable
            fun card(text: String, value: String, icon: String, iconClass: String) {
                Tile(isParent = true) {
                    Tile(isChild = true, attrs = {
                        classes("card")
                    }) {
                        Div(attrs = {
                            classes("card-content")
                        }) {
                            Div(attrs = {
                                classes("level", "is-mobile")
                            }) {
                                Div(attrs = {
                                    classes("level-item")
                                }) {
                                    Div(attrs = {
                                        classes("is-widget-label")
                                    }) {
                                        H3(attrs = {
                                            classes("subtitle", "is-spaced")
                                        }) {
                                            Text(text)
                                        }
                                        H1(attrs = {
                                            classes("title")
                                        }) {
                                            Text(value)
                                        }
                                    }
                                }
                                Div(attrs = {
                                    classes("level-item", "has-widget-icon")
                                }) {
                                    Div(attrs = {
                                        classes("is-widget-icon", iconClass)
                                    }) {
                                        MDIIcon(icon, IconSize.Large)
                                    }
                                }
                            }
                        }
                    }
                }
            }


            card(
                l10n("navigation.overview.guilds"),
                stats?.guilds?.toString() ?: "…", "server", "has-text-info"
            )
            card(
                l10n("navigation.overview.users"),
                stats?.users?.toString() ?: "…", "account-group", "has-text-primary"
            )
        }
    }
}