package kitty.frontend.ui

import androidx.compose.runtime.*
import kitty.frontend.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import kotlinx.browser.*
import org.kodein.di.compose.*

@Composable
fun KittyRootUi(component: KittyRootComponent) {
    val child by component.childStack.subscribeAsState()
    val pages by rememberInstance<Set<KittyDecomposedPage<out KittyRootComponent.Config, out DarkKittyRoot.Child>>>(tag = "frontend-pages")

    val localizationProviderState: State<LocalizationProvider> by rememberInstance()
    val localizationProvider by localizationProviderState
    val currentLanguage = localizationProvider.language

    DisposableEffect(currentLanguage) {
        val document = document.documentElement
        if (currentLanguage == null || document == null) return@DisposableEffect onDispose { }
        document.setAttribute("lang", currentLanguage)
        onDispose { document.removeAttribute("lang") }
    }

    ProvideLocalizationProvider(localizationProvider) {
        ToastController {
            KittyNavigation(component)

            val renderer by derivedStateOf {
                pages.renderer(child.active.instance)
            }

            renderer?.invoke()
        }
    }
}
