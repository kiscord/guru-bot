package kitty.frontend.ui

import androidx.compose.runtime.*
import kitty.frontend.*
import kitty.logic.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.compose.*
import org.w3c.dom.*

@Composable
fun NavigationAnchor(
    config: KittyRootComponent.Config,
    attrs: AttrBuilderContext<HTMLAnchorElement>? = null,
    content: ContentBuilder<HTMLAnchorElement>? = null,
) {
    val navigation: KittyNavigation by rememberInstance()
    val resourcesRouter: ResourcesRouter by rememberInstance()
    val href = remember(config, resourcesRouter) { resourcesRouter.toDeepLink(config) }

    A(href = href.path, attrs = {
        onClick { e ->
            navigation.navigateTo(config)
            e.preventDefault()
            e.stopPropagation()
        }
        attrs?.invoke(this)
    }, content = content)
}