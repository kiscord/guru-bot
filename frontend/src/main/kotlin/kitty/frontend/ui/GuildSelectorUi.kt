package kitty.frontend.ui

import androidx.compose.runtime.*
import kitty.*
import kitty.frontend.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import kitty.logic.services.*
import kitty.resources.*
import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.resources.serialization.*
import kotlinx.browser.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.compose.*

@Composable
fun GuildSelectorUi(context: DarkKittyRoot.Child.GuildSelector) {
    AuthPredicate { it.isPotentiallyAuthenticated }

    val navigation: KittyNavigation by rememberInstance()
    val resourcesFormat: ResourcesFormat by rememberInstance()

    val userService: UserService by rememberInstance()
    val userGuilds by userService.userGuilds.collectAsState()
    val (managedUserGuilds, unmanagedUserGuilds) = remember(userGuilds) {
        userGuilds.orEmpty().partition { it.managed }
    }
    val (allowedToManaged, disallowed) = remember(unmanagedUserGuilds) { unmanagedUserGuilds.partition { it.allowedToManage } }

    Loader(userGuilds == null)

    @Composable
    fun group(items: Collection<DiscordGuildDTO>, title: String) {
        if (items.isEmpty()) return
        Subtitle(title)

        Grid(items, GridSize.Big) { guild ->
            val isClickable = guild.managed || guild.allowedToManage
            Tile(isChild = true, attrs = {
                classes("card")
                when {
                    guild.managed -> classes("has-background-success")
                    guild.allowedToManage -> classes("has-background-info")
                }
                if (isClickable) {
                    classes("is-clickable")
                    onClick {
                        if (guild.managed) {
                            navigation.navigateTo(
                                KittyRootComponent.Config.GuildOverview(guildId = guild.id)
                            )
                        } else if (guild.allowedToManage) {
                            val url = URLBuilder()
                            href(resourcesFormat, OAuth2Resource.Login(provider = "discord"), url)
                            url.parameters["guildId"] = guild.id.toString()
                            window.location.assign(url.buildString())
                        }
                    }
                }
            }) {
                Div(attrs = { classes("card-content") }) {
                    Div(attrs = { classes("media") }) {
                        val guildIconUrl = guild.iconUrl
                        if (guildIconUrl != null) {
                            Div(attrs = { classes("media-left") }) {
                                Figure(attrs = { classes("image", "is-48x48") }) {
                                    Img(
                                        src = guildIconUrl.toString(),
                                        alt = "Icon of guild ${guild.name}",
                                        attrs = { classes("is-rounded") }
                                    )
                                }
                            }
                        }
                        Div(attrs = { classes("media-content", "is-align-self-center", "not-scrollable") }) {
                            P(attrs = { classes("title", "is-4") }) {
                                Text(guild.name)
                            }
                        }
                        if (isClickable) Div(attrs = {
                            classes("media-right", "is-align-self-center", "is-inline-flex")
                        }) {
                            when {
                                guild.managed ->
                                    MDIIcon("arrow-right", iconSize = IconSize.Medium)

                                guild.allowedToManage ->
                                    MDIIcon("server-plus", iconSize = IconSize.Medium)
                            }
                        }
                    }
                }
            }
        }
    }

    Section(attrs = { classes("section", "is-main-section") }) {
        group(managedUserGuilds, l10n("navigation.guild_selector.guilds.managed"))
        group(allowedToManaged, l10n("navigation.guild_selector.guilds.unmanaged"))
        group(disallowed, l10n("navigation.guild_selector.guilds.unavailable"))
    }
}
