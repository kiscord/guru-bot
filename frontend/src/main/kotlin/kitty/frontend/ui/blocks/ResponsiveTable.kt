package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kitty.frontend.l10n.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.dom.*
import org.jetbrains.compose.web.dom.Text
import org.w3c.dom.*
import kotlin.contracts.*

@OptIn(ExperimentalContracts::class)
@Composable
fun <T : Any> ResponsiveTable(
    items: Collection<T>,
    isLoading: Boolean,
    tableAttrs: AttrBuilderContext<HTMLTableElement>? = null,
    block: @DisallowComposableCalls ResponsiveTableBuilder<T>.() -> Unit
) {
    contract {
        callsInPlace(block, InvocationKind.AT_MOST_ONCE)
    }

    val builder = remember {
        ResponsiveTableBuilderImpl<T>().apply(block)
    }

    Div(attrs = {
        classes("b-table")
        if (isLoading) classes("is-loading")
    }) {
        Div(attrs = { classes("table-wrapper", "has-mobile-cards") }) {
            Table(attrs = {
                classes("table")
                tableAttrs?.invoke(this)
            }) {
                Thead {
                    Tr {
                        builder.cells.forEach { cell ->
                            with(cell) {
                                header()
                            }
                        }
                    }
                }
                Tbody {
                    when {
                        isLoading -> builder.fakeItems

                        items.isEmpty() -> {
                            Tr(attrs = {
                                classes("is-empty")
                            }) {
                                Td(attrs = {
                                    colspan(builder.cells.size)
                                }) {
                                    builder.onEmpty(this)
                                }
                            }

                            return@Tbody
                        }

                        else -> items
                    }.forEach { item ->
                        builder.keyWrapper(item) {
                            Tr {
                                builder.cells.forEach { cell ->
                                    with(cell) {
                                        cell(item)
                                    }
                                }
                            }
                            builder.cells.forEach { cell ->
                                with(cell) {
                                    afterRow(item, builder.cells)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalContracts::class)
@Composable
fun <T : Any> ResponsiveTable(
    items: Collection<T>?,
    tableAttrs: AttrBuilderContext<HTMLTableElement>? = null,
    block: @DisallowComposableCalls ResponsiveTableBuilder<T>.() -> Unit
) {
    contract {
        callsInPlace(block, InvocationKind.AT_MOST_ONCE)
    }

    ResponsiveTable(items.orEmpty(), items == null, tableAttrs, block)
}

interface ResponsiveTableBuilder<T : Any> {
    fun key(block: (T) -> Any)

    fun collapse(
        isExpanded: (T) -> Boolean,
        onToggle: (T) -> Unit,
        details: @Composable ElementScope<HTMLTableCellElement>.(T) -> Unit
    )

    fun field(
        label: @Composable () -> String,
        content: @Composable ElementScope<HTMLTableCellElement>.(T) -> Unit
    )

    fun actionButton(
        label: @Composable () -> String,
        attrs: (AttrsScope<HTMLButtonElement>.(T) -> Unit)? = null,
        content: (@Composable ElementScope<HTMLButtonElement>.(T) -> Unit)? = null,
    )

    fun onEmpty(content: ContentBuilder<HTMLTableCellElement>)

    fun fake(item: T)
}

private class ResponsiveTableBuilderImpl<T : Any> : ResponsiveTableBuilder<T> {
    val cells = mutableListOf<Cell<T>>()

    var keyWrapper: @Composable (T, @Composable () -> Unit) -> Unit = { _, content -> content() }

    override fun key(block: (T) -> Any) {
        keyWrapper = { item, content ->
            key(block(item)) {
                content()
            }
        }
    }

    override fun collapse(
        isExpanded: (T) -> Boolean,
        onToggle: (T) -> Unit,
        details: @Composable ElementScope<HTMLTableCellElement>.(T) -> Unit
    ) {
        cells += CollapseCell(isExpanded, onToggle, details)
    }

    override fun field(
        label: @Composable () -> String,
        content: @Composable ElementScope<HTMLTableCellElement>.(T) -> Unit
    ) {
        cells += FieldCell(
            label = label,
            content = content,
        )
    }

    override fun actionButton(
        label: @Composable () -> String,
        attrs: (AttrsScope<HTMLButtonElement>.(T) -> Unit)?,
        content: (@Composable ElementScope<HTMLButtonElement>.(T) -> Unit)?,
    ) {
        val cell = cells.lastOrNull() as? ActionsCell<T> ?: ActionsCell<T>().also { cells += it }
        cell.buttons += { item ->
            val title = label()
            Button(attrs = {
                classes("button")
                attr("title", title)
                attrs?.invoke(this, item)
            }) {
                content?.invoke(this, item)
            }
        }
    }

    var onEmpty: ContentBuilder<HTMLTableCellElement> = @Composable {
        Section(attrs = {
            classes("section")
        }) {
            Div(attrs = {
                classes("content", "has-text-grey", "has-text-centered")
            }) {
                P {
                    MDIIcon("emoticon-sad", IconSize.Large)
                }
                P {
                    SwitchLocalizationContext(namespace = "translation") {
                        Text("Nothing here :(")
                    }
                }
            }
        }
    }

    override fun onEmpty(content: ContentBuilder<HTMLTableCellElement>) {
        onEmpty = content
    }

    val fakeItems = mutableListOf<T>()

    override fun fake(item: T) {
        fakeItems += item
    }

    sealed interface Cell<T : Any> {
        @Composable
        fun ElementScope<HTMLTableRowElement>.header()

        @Composable
        fun ElementScope<HTMLTableRowElement>.cell(item: T)

        @Composable
        fun ElementScope<HTMLTableSectionElement>.afterRow(item: T, cells: Collection<Cell<T>>) {
        }
    }

    class FieldCell<T : Any>(
        private val label: @Composable () -> String,
        private val content: @Composable ElementScope<HTMLTableCellElement>.(T) -> Unit,
    ) : Cell<T> {
        @Composable
        override fun ElementScope<HTMLTableRowElement>.header() {
            Th {
                Text(label())
            }
        }

        @Composable
        override fun ElementScope<HTMLTableRowElement>.cell(item: T) {
            val label = label()
            Td(attrs = {
                attr("data-label", label)
            }) {
                content.invoke(this, item)
            }
        }
    }

    class ActionsCell<T : Any> : Cell<T> {
        val buttons = mutableListOf<@Composable ElementScope<HTMLDivElement>.(T) -> Unit>()

        @Composable
        override fun ElementScope<HTMLTableRowElement>.header() {
            Th()
        }

        @Composable
        override fun ElementScope<HTMLTableRowElement>.cell(item: T) {
            Td(attrs = {
                classes("is-actions-cell")
            }) {
                Div(attrs = { classes("buttons", "has-addons", "is-right") }) {
                    buttons.forEach { button ->
                        button(this, item)
                    }
                }
            }
        }
    }

    open class CollapseCell<T : Any>(
        private val isExpanded: (T) -> Boolean,
        private val onToggle: (T) -> Unit,
        private val details: @Composable ElementScope<HTMLTableCellElement>.(T) -> Unit
    ) : Cell<T> {
        @Composable
        override fun ElementScope<HTMLTableRowElement>.header() {
            Th(attrs = { classes("is-chevron-cell") })
        }

        @Composable
        override fun ElementScope<HTMLTableRowElement>.cell(item: T) {
            Td(attrs = {
                classes("is-chevron-cell")
            }) {
                A(attrs = {
                    attr("role", "button")
                    onClick { onToggle(item) }
                }) {
                    MDIIcon("chevron-right", containerAttrs = {
                        if (isExpanded(item)) classes("is-expanded")
                    })
                }
            }
        }

        @Composable
        override fun ElementScope<HTMLTableSectionElement>.afterRow(item: T, cells: Collection<Cell<T>>) {
            if (isExpanded(item)) {
                Tr(attrs = {
                    classes("detail")
                }) {
                    Td(attrs = {
                        colspan(cells.size)
                    }) {
                        details(item)
                    }
                }
            }
        }
    }
}