package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kitty.frontend.js.*
import kotlinx.browser.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import kotlin.math.*

@Immutable
private data class LazyListState(
    val scrollTop: Double = .0,
    val clientHeight: Int = 0,
    val offsetTop: Int = 0,
    val fontSize: Float = 0f,
    val lineHeight: Float = 0f,
)

@Composable
fun <T : Any> LazyListFixedHeight(
    items: List<T>,
    itemHeight: CSSSizeValue<out CSSUnitLengthOrPercentage>,
    overscanItems: Int = 15,
    attrs: AttrBuilderContext<out HTMLElement>? = null,
    itemAttrs: (AttrsScope<HTMLElement>.(T, Int) -> Unit) = { _, _ -> },
    container: ContentBuilder<HTMLDivElement>? = null,
    block: @Composable (T, Int) -> Unit
) {
    require("@/css/lazy-list.scss")

    var state: LazyListState by remember { mutableStateOf(LazyListState()) }

    fun takeValues(element: HTMLElement) {
        val style = window.getComputedStyle(element)
        state = state.copy(
            offsetTop = element.offsetTop,
            fontSize = style.fontSize.removeSuffix("px").toFloat(),
            lineHeight = style.lineHeight.removeSuffix("px").toFloat(),
        )
    }

    fun takeScrollValues(element: Element) {
        state = state.copy(
            scrollTop = element.scrollTop,
            clientHeight = element.clientHeight,
        )
    }

    val itemHeightPx = remember(itemHeight, state) {
        when (itemHeight.unit) {
            CSSUnit.px -> itemHeight.value
            CSSUnit.em -> itemHeight.value * state.lineHeight
            CSSUnit.percent -> itemHeight.value * state.lineHeight / 100f
            else -> throw UnsupportedOperationException("Only px, rem and percent units allowed for itemHeight!")
        }
    }

    val (startIndex, endIndex) = remember(state, itemHeightPx, items.size, overscanItems) {
        val start = (
                floor((state.scrollTop - state.offsetTop) / itemHeightPx).toInt() - overscanItems
                ).coerceIn(0, items.size)
        val end = (
                ceil((state.scrollTop - state.offsetTop + state.clientHeight) / itemHeightPx).toInt() + overscanItems
                ).coerceIn(0, items.size)
        start to end
    }

    Div(attrs = {
        onScroll {
            takeValues(it.target as HTMLElement)
        }

        style {
            height(itemHeightPx.px * items.size)
            variable("--lazy-list-item-height", itemHeightPx.px)
            paddingTop(itemHeightPx.px * startIndex)
        }

        classes("lazy-list")

        attrs?.invoke(this)
    }) {
        DisposableEffect(null) {
            val listener = EventListener {
                takeScrollValues(document.scrollingElementOrSelf)
            }

            document.addEventListener("resize", listener)
            document.addEventListener("scroll", listener)

            takeScrollValues(document.scrollingElementOrSelf)

            val observer = MutationObserver { _, _ ->
                takeValues(scopeElement)
            }

            observer.observe(
                scopeElement, MutationObserverInit(
                    attributes = true,
                    attributeFilter = arrayOf("style")
                )
            )

            takeValues(scopeElement)

            onDispose {
                observer.disconnect()
                document.removeEventListener("resize", listener)
                document.removeEventListener("scroll", listener)
            }
        }

        container?.invoke(this)

        for (index in startIndex until endIndex) {
            val item = items[index]
            key(index) {
                Div(attrs = {
                    classes("lazy-list-item", "list-item")
                    itemAttrs.invoke(this, item, index)
                }) {
                    block(item, index)
                }
            }
        }
    }
}

private val Document.scrollingElementOrSelf: Element
    get() = scrollingElement ?: documentElement ?: throw IllegalStateException("Unable to retrieve scrolling element")
