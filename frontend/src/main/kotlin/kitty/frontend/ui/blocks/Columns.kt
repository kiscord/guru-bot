package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Composable
fun Columns(attrs: AttrBuilderContext<HTMLDivElement>? = null, content: ContentBuilder<HTMLDivElement>) {
    Div(attrs = {
        classes("columns")
        attrs?.invoke(this)
    }, content = content)
}

@Composable
fun Column(attrs: AttrBuilderContext<HTMLDivElement>? = null, content: ContentBuilder<HTMLDivElement>) {
    Div(attrs = {
        classes("column")
        attrs?.invoke(this)
    }, content = content)
}