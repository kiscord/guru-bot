package kitty.frontend.ui.blocks.navbar

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Composable
fun NavBarItemAnchor(
    attrs: AttrBuilderContext<HTMLAnchorElement>? = null,
    content: ContentBuilder<HTMLAnchorElement>? = null
) {
    A(attrs = {
        classes("navbar-item")
        attrs?.invoke(this)
    }, content = content)
}


@Composable
fun NavBarItemDiv(
    attrs: AttrBuilderContext<HTMLDivElement>? = null,
    content: ContentBuilder<HTMLDivElement>? = null
) {
    Div(attrs = {
        classes("navbar-item")
        attrs?.invoke(this)
    }, content = content)
}