package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kiscord.markdown.*
import org.intellij.markdown.*
import org.intellij.markdown.ast.*
import org.intellij.markdown.flavours.*
import org.intellij.markdown.html.*
import org.intellij.markdown.parser.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

private object CustomDiscordFlavourDescriptor : DiscordFlavourDescriptor() {
    override fun createHtmlGeneratingProviders(linkMap: LinkMap, baseURI: URI?): Map<IElementType, GeneratingProvider> {
        return super.createHtmlGeneratingProviders(linkMap, baseURI) + hashMapOf(
            DiscordElementTypes.SPOILER to object : SimpleInlineTagProvider("span", 2, -2) {
                override fun openTag(visitor: HtmlGenerator.HtmlGeneratingVisitor, text: String, node: ASTNode) {
                    visitor.consumeTagOpen(node, tagName, "class=\"spoiler\"", "tabindex=\"0\"")
                }
            }
        )
    }
}

private val discordAttributesCustomizer: AttributesCustomizer = { _, tagName, attributes ->
    when (tagName) {
        "a" -> when {
            attributes.none { it == "class=\"spoiler\"" } -> {
                val newAttirbutes = attributes.toMutableList()
                if (attributes.none { it?.startsWith("target=") == true }) {
                    newAttirbutes += "target=\"_blank\""
                }
                if (attributes.none { it?.startsWith("rel=") == true }) {
                    newAttirbutes += "rel=\"noopener noreferrer\""
                }
                newAttirbutes
            }

            else -> attributes
        }

        else -> attributes
    }
}

private class DiscordTagRenderer : HtmlGenerator.DefaultTagRenderer(discordAttributesCustomizer, false) {
    override fun openTag(
        node: ASTNode,
        tagName: CharSequence,
        vararg attributes: CharSequence?,
        autoClose: Boolean
    ): CharSequence {
        val tag = super.openTag(node, tagName, *attributes, autoClose = autoClose)
        if (tagName == "blockquote") {
            return "<div class=\"quote-container\"><div class=\"quote-marker\"></div>$tag"
        }
        return tag
    }

    override fun closeTag(tagName: CharSequence): CharSequence {
        val tag = super.closeTag(tagName)
        if (tagName == "blockquote") {
            return "$tag</div>"
        }
        return tag
    }
}

@Composable
fun ElementScope<HTMLElement>.MarkdownText(
    text: String,
    flavour: MarkdownFlavourDescriptor = CustomDiscordFlavourDescriptor
) {
    val sanitizedText = remember(text) { text.trim() }
    val parser = remember(flavour) { MarkdownParser(flavour) }
    val parsed by derivedStateOf { parser.buildMarkdownTreeFromString(sanitizedText) }
    val generator by derivedStateOf { HtmlGenerator(sanitizedText, parsed, flavour) }
    val html by derivedStateOf { generator.generateHtml(DiscordTagRenderer()) }

    DisposableEffect(html) {
        scopeElement.innerHTML = html
        onDispose { scopeElement.innerHTML = "" }
    }
}