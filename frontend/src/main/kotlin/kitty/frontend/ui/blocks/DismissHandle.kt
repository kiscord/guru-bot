package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kotlinx.datetime.*

fun interface DismissHandle {
    val dismissTimestamps: TimeStamps? get() = null
    fun dismiss()

    data class TimeStamps(
        val start: Instant,
        val end: Instant,
    )

    object None : DismissHandle {
        override fun dismiss() {}
    }

    companion object {
        val current: DismissHandle
            @Composable
            @ReadOnlyComposable
            get() = LocalDismissHandle.current
    }
}

private val LocalDismissHandle = staticCompositionLocalOf<DismissHandle> { DismissHandle.None }

@Composable
fun ProvideDismissHandle(dismissHandle: DismissHandle, content: @Composable () -> Unit) {
    CompositionLocalProvider(LocalDismissHandle provides dismissHandle, content = content)
}