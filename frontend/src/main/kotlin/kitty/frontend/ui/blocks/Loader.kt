package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*

@Composable
fun Loader(isActive: Boolean) {
    Div(attrs = {
        classes("loader-wrapper")
        if (isActive) classes("is-active")
    }) {
        Div(attrs = {
            classes("loader", "is-loading")
        })
    }
}