package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import kotlin.contracts.*

@OptIn(ExperimentalContracts::class)
@Composable
fun InlineText(text: String, block: @DisallowComposableCalls InlineTextScope.() -> Unit) {
    contract {
        callsInPlace(block, InvocationKind.AT_MOST_ONCE)
    }

    val scope = remember { InlineTextScopeImpl().apply(block) }
    scope.render(text)
}

interface InlineTextScope {
    fun inline(token: String, block: @Composable () -> Unit)
}

private class InlineTextScopeImpl : InlineTextScope {
    private val tokens = mutableMapOf<String, @Composable () -> Unit>()

    override fun inline(token: String, block: @Composable () -> Unit) {
        tokens[token] = block
    }

    @Composable
    fun render(text: String) {
        val inlineTokens = remember(text) {
            val inlineTokens = mutableListOf<InlineToken>()
            var i = 0
            while (true) {
                val newToken = text.indexOfAny(tokens.keys, startIndex = i)
                if (newToken < 0) {
                    if (i < text.length) {
                        inlineTokens += InlineToken.RawText(text.substring(i))
                    }
                    break
                }
                if (newToken > i) {
                    inlineTokens += InlineToken.RawText(text.substring(i, newToken))
                    i = newToken
                }
                val (token, block) = tokens.entries.first { (token, _) ->
                    text.startsWith(token, startIndex = newToken)
                }
                inlineTokens += InlineToken.Block(block)
                i += token.length
            }
            inlineTokens.toList()
        }

        inlineTokens.forEach { token ->
            token.render()
        }
    }

    private sealed interface InlineToken {
        @Composable
        fun render()

        class RawText(val text: String) : InlineToken {
            @Composable
            override fun render() = Text(text)
        }

        class Block(val block: @Composable () -> Unit) : InlineToken {
            @Composable
            override fun render() = block()
        }
    }
}