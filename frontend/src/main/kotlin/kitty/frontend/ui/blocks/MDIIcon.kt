package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Composable
fun MDIIcon(
    icon: String,
    iconSize: IconSize? = null,
    containerSize: IconSize? = null,
    iconAlignment: IconAlignment? = null,
    containerClass: String = "icon",
    containerAttrs: AttrBuilderContext<HTMLSpanElement>? = null,
    iconAttrs: AttrBuilderContext<HTMLSpanElement>? = null,
) {
    val realIconSize = iconSize ?: IconSize.current
    val realContainerSize = containerSize ?: realIconSize
    val realIconAlignment = iconAlignment ?: IconAlignment.current

    Span(attrs = {
        classes(containerClass)
        when (realContainerSize) {
            IconSize.Small -> classes("is-small")
            IconSize.Default -> {}
            IconSize.Medium -> classes("is-medium")
            IconSize.Large -> classes("is-large")
        }
        when(realIconAlignment) {
            IconAlignment.Default -> {}
            IconAlignment.Left -> classes("is-left")
            IconAlignment.Right -> classes("is-right")
        }
        containerAttrs?.invoke(this)
    }) {
        Span(attrs = {
            classes("mdi", "mdi-$icon")
            when (realIconSize) {
                IconSize.Small -> {}
                IconSize.Default -> classes("mdi-24px")
                IconSize.Medium -> classes("mdi-36px")
                IconSize.Large -> classes("mdi-48px")
            }
            iconAttrs?.invoke(this)
        })
    }
}
