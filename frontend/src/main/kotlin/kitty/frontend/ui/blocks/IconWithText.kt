package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.jetbrains.compose.web.dom.Text
import org.w3c.dom.*

@Composable
fun IconWithText(
    icon: String,
    text: String,
    attrs: AttrBuilderContext<HTMLSpanElement>? = null,
    textAttrs: AttrBuilderContext<HTMLSpanElement>? = null,
) {
    Span(attrs = {
        classes("icon-text")
        attrs?.invoke(this)
    }) {
        MDIIcon(icon)
        Span(attrs = {
            textAttrs?.invoke(this)
        }) {
            Text(text)
        }
    }
}