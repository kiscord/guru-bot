package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kiscord.builder.*
import kitty.frontend.js.*
import kitty.frontend.l10n.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.dom.*
import kotlin.contracts.*
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalContracts::class)
@Composable
fun <T : Any> DeletionModalDialog(block: @DisallowComposableCalls DeletionModalDialogScope<T>.() -> Unit): DeletionModalDialogHandle<T> {
    contract {
        callsInPlace(block, InvocationKind.AT_MOST_ONCE)
    }

    val scope = remember {
        DeletionModalDialogScopeImpl.Builder<T>().apply(block).build()
    }

    val toastHost = ToastHost.current

    ProvideDismissHandle({
        scope.itemAboutToDelete = null
        scope.itemAboutToDeleteConfirm = ""
        scope.itemDeleteInProgress = null
    }) {
        val dismissHandle = DismissHandle.current

        val itemDeleteInProgress = scope.itemDeleteInProgress

        if (itemDeleteInProgress != null) LaunchedEffect(itemDeleteInProgress) {
            try {
                if (scope.deletionHandler(itemDeleteInProgress)) {
                    toastHost.submit(ToastAlignment.TopCenter, 5.seconds.inWholeMilliseconds) {
                        Notification(attrs = {
                            classes("is-primary")
                        }) {
                            Text(scope.l10n.deletionSucceed(itemDeleteInProgress))
                        }
                    }
                    scope.onDeleted(itemDeleteInProgress)
                } else {
                    toastHost.submit(ToastAlignment.TopCenter, 5.seconds.inWholeMilliseconds) {
                        Notification(attrs = {
                            classes("is-danger")
                        }) {
                            Text(scope.l10n.deletionFailed(itemDeleteInProgress))
                        }
                    }
                }
            } finally {
                dismissHandle.dismiss()
            }
        }

        val itemAboutToDelete = scope.itemAboutToDelete

        val confirmationSlug = remember(itemAboutToDelete) {
            if (itemAboutToDelete == null) return@remember ""
            scope.confirmationSlug(itemAboutToDelete)
        }

        ModalCard(itemAboutToDelete != null) {
            ModalCardHeader(itemAboutToDelete?.let { scope.l10n.title(it) } ?: "")
            ModalCardBody {
                Subtitle(itemAboutToDelete?.let { scope.l10n.header(it) } ?: "")
                Field(itemAboutToDelete?.let { scope.l10n.prompt(it) } ?: "") {
                    Control(
                        isExpanded = true,
                    ) {
                        Input(InputType.Text) {
                            classes("input")
                            placeholder(confirmationSlug)
                            value(scope.itemAboutToDeleteConfirm)
                            onInput { event ->
                                scope.itemAboutToDeleteConfirm = event.value
                            }
                        }
                    }
                }
            }
            ModalCardFooter {
                Button(attrs = {
                    classes("button", "is-danger", "is-outlined")
                    if (!scope.itemAboutToDeleteConfirm.equals(confirmationSlug, ignoreCase = true)) {
                        disabled()
                    } else {
                        onClick {
                            scope.itemDeleteInProgress = itemAboutToDelete
                        }
                    }
                }) {
                    Text(itemAboutToDelete?.let { scope.l10n.confirmButton(it) } ?: "")
                }
                Button(attrs = {
                    classes("button", "is-ghost")
                    onClick { dismissHandle.dismiss() }
                }) {
                    Text(itemAboutToDelete?.let { scope.l10n.cancelButton(it) } ?: "")
                }
            }
        }
    }

    return scope.handle
}

interface DeletionModalDialogScope<T : Any> {
    fun confirmationSlug(block: (T) -> String)
    fun deletionHandler(block: suspend (T) -> Boolean)
    fun onDeleted(block: (T) -> Unit)
    fun l10n(l10n: DeletionModalDialogLocalization<T>)
    fun l10n(
        title: L10NText<T>,
        header: L10NText<T>,
        prompt: L10NText<T>,
        confirmButton: L10NText<T>,
        cancelButton: L10NText<T>,
        deletionSucceed: L10NText<T>,
        deletionFailed: L10NText<T>,
    ) = l10n(
        DeletionModalDialogLocalization(
            title = title,
            header = header,
            prompt = prompt,
            confirmButton = confirmButton,
            cancelButton = cancelButton,
            deletionSucceed = deletionSucceed,
            deletionFailed = deletionFailed,
        )
    )
}

interface DeletionModalDialogHandle<T : Any> {
    fun delete(item: T)
}

typealias L10NText<T> = @Composable (T) -> String

@Stable
data class DeletionModalDialogLocalization<in T>(
    val title: L10NText<T>,
    val header: L10NText<T>,
    val prompt: L10NText<T>,
    val confirmButton: L10NText<T>,
    val cancelButton: L10NText<T>,
    val deletionSucceed: L10NText<T>,
    val deletionFailed: L10NText<T>,
)

private class DeletionModalDialogScopeImpl<T : Any>(
    private val _confirmationSlug: (T) -> String,
    private val _deletionHandler: suspend (T) -> Boolean,
    private val _onDeleted: (T) -> Unit,
    val l10n: DeletionModalDialogLocalization<T>,
) {
    val handle = Handle()

    var itemAboutToDelete by mutableStateOf<T?>(null)
    var itemAboutToDeleteConfirm by mutableStateOf("")
    var itemDeleteInProgress by mutableStateOf<T?>(null)

    fun confirmationSlug(value: T): String = _confirmationSlug(value)
    suspend fun deletionHandler(value: T): Boolean = _deletionHandler(value)
    fun onDeleted(value: T) = _onDeleted(value)

    class Builder<T : Any> : DeletionModalDialogScope<T>, BuilderFor<DeletionModalDialogScopeImpl<T>> {
        private lateinit var confirmationSlug: (T) -> String
        private lateinit var deletionHandler: suspend (T) -> Boolean
        private lateinit var l10n: DeletionModalDialogLocalization<T>
        private var onDeleted: (T) -> Unit = {}

        override fun confirmationSlug(block: (T) -> String) {
            confirmationSlug = block
        }

        override fun deletionHandler(block: suspend (T) -> Boolean) {
            deletionHandler = block
        }

        override fun onDeleted(block: (T) -> Unit) {
            onDeleted = block
        }

        override fun l10n(l10n: DeletionModalDialogLocalization<T>) {
            this.l10n = l10n
        }

        override fun build() = DeletionModalDialogScopeImpl(
            _confirmationSlug = confirmationSlug,
            _deletionHandler = deletionHandler,
            _onDeleted = onDeleted,
            l10n = l10n,
        )
    }

    inner class Handle : DeletionModalDialogHandle<T> {
        override fun delete(item: T) {
            itemAboutToDelete = item
        }
    }
}
