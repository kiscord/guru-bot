package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*

@Composable
fun Title(text: String) {
    H1(attrs = { classes("title")}) {
        Text(text)
    }
}

@Composable
fun Subtitle(text: String) {
    H2(attrs = { classes("subtitle")}) {
        Text(text)
    }
}