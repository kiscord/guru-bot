package kitty.frontend.ui.blocks.navbar

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*

@Composable
fun NavBarBurger(isOpen: Boolean, onToggle: () -> Unit) {
    A(attrs = {
        classes("navbar-burger")
        if (isOpen) classes("is-active")
        attr("role", "button")
        attr("aria-label", "menu")
        attr("aria-expanded", isOpen.toString())
        onClick { onToggle() }
    }) {
        repeat(3) {
            Span(attrs = { attr("aria-hidden", "true") })
        }
    }
}