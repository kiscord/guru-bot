package kitty.frontend.ui.blocks.navbar

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Composable
fun NavBarLink(
    attrs: AttrBuilderContext<HTMLAnchorElement>? = null,
    content: ContentBuilder<HTMLAnchorElement>? = null
) {
    A(attrs = {
        classes("navbar-link")
        attrs?.invoke(this)
    }, content = content)
}