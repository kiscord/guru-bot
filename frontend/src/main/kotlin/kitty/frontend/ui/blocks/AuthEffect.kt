package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kitty.frontend.*
import kitty.logic.*
import kotlinx.coroutines.flow.*
import org.kodein.di.compose.*

@Composable
fun AuthEffect(block: DisposableEffectScope.(DarkKittyAuth) -> DisposableEffectResult) {
    val authFlow: StateFlow<DarkKittyAuth> by rememberInstance(tag = "auth-flow")
    val auth by authFlow.collectAsState()
    DisposableEffect(auth) {
        block(auth)
    }
}

@Composable
fun AuthPredicate(
    fallback: () -> KittyRootComponent.Config = { KittyRootComponent.Config.Overview },
    block: (DarkKittyAuth) -> Boolean
) {
    val navigation: KittyNavigation by rememberInstance()

    AuthEffect { auth ->
        if (!block(auth)) {
            navigation.navigateTo(fallback())
        }
        onDispose { }
    }
}