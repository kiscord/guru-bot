package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Composable
fun Figure(
    attrs: AttrBuilderContext<HTMLElement>? = null,
    content: ContentBuilder<HTMLElement>? = null
) {
    TagElement(
        elementBuilder = Figure,
        applyAttrs = attrs,
        content = content
    )
}

private val Figure = ElementBuilder.createBuilder<HTMLElement>("figure")