package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kitty.*
import kitty.frontend.js.*
import kitty.frontend.modules.faq.*
import kiscord.api.*
import org.jetbrains.compose.web.attributes.*
import org.jetbrains.compose.web.css.*
import org.jetbrains.compose.web.dom.*
import org.jetbrains.compose.web.dom.Text
import org.w3c.dom.*

@Composable
fun DiscordScreen(isSticky: Boolean = false, content: ContentBuilder<HTMLDivElement>) {
    require("@/css/discord-embed.scss")

    Div(attrs = {
        classes("discord-screen")
        if (isSticky) classes("is-sticky")
    }) {
        content(this)
    }
}

@Composable
fun DiscordMessagePreview(content: ContentBuilder<HTMLDivElement>) {
    require("@/css/discord-embed.scss")

    Div(attrs = {
        classes("discord-message-preview")
    }) {
        content(this)
    }
}

@Composable
fun DiscordAvatar(url: String) {
    require("@/css/discord-embed.scss")

    Div(attrs = {
        classes("message-avatar")
    }) {
        Img(src = url)
    }
}

@Composable
fun DiscordAvatar(userProfile: UserProfile?) {
    DiscordAvatar(userProfile?.avatarUrl(80) ?: "https://cdn.discordapp.com/embed/avatars/1.png")
}

@Composable
fun DiscordMessage(content: ContentBuilder<HTMLDivElement>) {
    require("@/css/discord-embed.scss")

    Div(attrs = {
        classes("message-message")
    }) {
        content()
    }
}

@Composable
fun DiscordMessageName(name: String, bot: Boolean = false) {
    require("@/css/discord-embed.scss")

    Div(attrs = {
        classes("message-name")
    }) {
        key("username") {
            Span(attrs = {
                classes("message-username")
            }) { Text(name) }
        }
        key("tag") {
            if (bot) Span(attrs = {
                classes("message-tag")
            }) {
                Span(attrs = {
                    classes("message-tag-text")
                }) {
                    Text("BOT")
                }
            }
        }
        key("date") {
            Span(attrs = {
                classes("message-date")
            }) { Text("Today at 1:23 PM") }
        }
    }
}

@Composable
fun DiscordMessageName(userProfile: UserProfile?) {
    DiscordMessageName(userProfile?.username ?: BuildConfig.NAME, userProfile?.bot == true)
}


@Composable
fun DiscordMessageContent(text: String) {
    require("@/css/discord-embed.scss")

    Div(attrs = {
        classes("message-content", "markdown")
    }) {
        MarkdownText(text)
    }
}

@Composable
fun DiscordEmbed(embed: EmbedSpec) {
    Article(attrs = {
        classes("message-embed")
        val color = embed.color
        if (color != null && color != 0) style {
            variable("--message-embed-color", hexColor(color))
        }
    }) {
        key("title") {
            embed.title?.let { title ->
                Div(attrs = {
                    classes("embed-title")
                }) {
                    val url = embed.url
                    if (url != null) {
                        A(href = url.toString(), attrs = {
                            target(ATarget.Blank)
                        }) {
                            Text(title)
                        }
                    } else {
                        Text(title)
                    }
                }
            }
        }
        key("description") {
            embed.description?.let { description ->
                Div(attrs = {
                    classes("embed-description", "markdown")
                }) {
                    MarkdownText(description)
                }
            }
        }
        key("fields") {
            val rawFields = embed.fields.orEmpty()
            val fields = remember(rawFields) {
                rawFields.windowedWithMax(3) { field ->
                    if (!field.inline) return@windowedWithMax field
                    field.inline
                }
            }
            if (fields.isNotEmpty()) Div(attrs = {
                classes("embed-fields")
            }) {
                fields.forEachIndexed { rowIndex, l ->
                    val inline = l.all { it.inline }
                    val region = 12 / l.size
                    l.forEachIndexed { index, field ->
                        key("field-$rowIndex-$index") {
                            Div(attrs = {
                                classes("embed-field")
                                style {
                                    if (inline) {
                                        gridColumn(index * region + 1, (index + 1) * region + 1)
                                    } else {
                                        gridColumn(1, 13)
                                    }
                                }
                            }) {
                                Div(attrs = {
                                    classes("embed-field-name")
                                }) {
                                    Text(field.name)
                                }
                                Div(attrs = {
                                    classes("embed-field-value", "markdown")
                                }) {
                                    MarkdownText(field.value)
                                }
                            }
                        }
                    }
                }
            }
        }
        key("footer") {
            embed.footer?.let { footer ->
                Div(attrs = {
                    classes("embed-footer")
                }) {
                    footer.iconUrl?.let { iconUrl ->
                        Img(src = iconUrl.toString(), attrs = {
                            classes("embed-footer-icon")
                        })
                    }
                    Span(attrs = {
                        classes("embed-footer-text")
                    }) {
                        Text(footer.text)
                    }
                }
            }
        }
    }
}

fun hexColor(color: Int): CSSColorValue = Color('#' + color.toString(16).padStart(6, '0'))
fun parseHexColor(color: CSSColorValue): Int = color.toString().removePrefix("#").toInt(16)

private fun <T, Z> Iterable<T>?.windowedWithMax(max: Int, block: (T) -> Z): List<List<T>> {
    if (this == null) return emptyList()
    val iter = iterator()
    if (!iter.hasNext()) return emptyList()
    val first = iter.next()
    var currentPredicate = block(first)
    var current = mutableListOf(first)
    val lists = mutableListOf<List<T>>(current)
    while (iter.hasNext()) {
        val next = iter.next()
        val nextPredicate = block(next)
        if (currentPredicate != nextPredicate || current.size >= max) {
            current = mutableListOf<T>().also { lists += it }
            currentPredicate = nextPredicate
        }
        current += next
    }
    return lists
}