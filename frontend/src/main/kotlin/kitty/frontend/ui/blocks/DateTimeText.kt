package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kitty.frontend.l10n.*
import kotlinx.datetime.*
import org.jetbrains.compose.web.dom.*
import org.jetbrains.compose.web.dom.Text
import org.w3c.dom.*
import kotlin.js.Date

@Composable
fun DateTimeText(
    instant: Instant, attrs: AttrBuilderContext<HTMLElement>? = null,
    shortOptions: Date.LocaleOptions = js("{}").unsafeCast<Date.LocaleOptions>(),
    fullOptions: Date.LocaleOptions = js("{}").unsafeCast<Date.LocaleOptions>(),
) {
    val l10n = l10n
    val languages = remember(l10n) { l10n.languages.toTypedArray() }
    val fullText = remember(instant, languages, fullOptions) {
        instant.toJSDate().toLocaleString(languages, fullOptions)
    }
    val shortText = remember(instant, languages, shortOptions) {
        instant.toJSDate().toLocaleDateString(languages, shortOptions)
    }

    Small(attrs = {
        classes("is-abbr-like")
        attr("title", fullText)
        attrs?.invoke(this)
    }) {
        Text(shortText)
    }
}