package kitty.frontend.ui.blocks.navbar

import androidx.compose.runtime.*
import org.jetbrains.compose.web.dom.*
import org.w3c.dom.*

@Composable
fun NavBarBrand(
    attrs: AttrBuilderContext<HTMLDivElement>? = null,
    content: ContentBuilder<HTMLDivElement>? = null,
) {
    Div(attrs = {
        classes("navbar-brand")
        attrs?.invoke(this)
    }, content = content)
}