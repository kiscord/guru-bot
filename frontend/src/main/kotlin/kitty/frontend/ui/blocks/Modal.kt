package kitty.frontend.ui.blocks

import androidx.compose.runtime.*
import kotlinx.browser.*
import kotlinx.dom.*
import org.jetbrains.compose.web.dom.*
import org.jetbrains.compose.web.dom.Text
import org.w3c.dom.*
import org.w3c.dom.events.*

@Composable
private fun ModalBase(isActive: Boolean, content: ContentBuilder<HTMLDivElement>) {
    val dismissHandle = DismissHandle.current

    DisposableEffect(isActive) {
        if (!isActive) return@DisposableEffect onDispose { }
        val doc = document.documentElement ?: return@DisposableEffect onDispose { }
        val keyListener = handler@{ e: Event ->
            if (e !is KeyboardEvent) return@handler
            if (e.keyCode == 27) dismissHandle.dismiss()
        }
        doc.addEventListener("keydown", keyListener)
        doc.addClass("is-clipped")
        onDispose {
            doc.removeClass("is-clipped")
            doc.removeEventListener("keydowm", keyListener)
        }
    }

    Div(attrs = {
        classes("modal")
        if (isActive) {
            classes("is-active")
        }
    }) {
        Div(attrs = {
            classes("modal-background")
            onClick { dismissHandle.dismiss() }
        })
        content()
    }
}

@Composable
fun Modal(isActive: Boolean, content: ContentBuilder<HTMLDivElement>) {
    val dismissHandle = DismissHandle.current

    ModalBase(isActive) {
        Div(attrs = {
            classes("modal-content")
        }, content = content)
        Button(attrs = {
            classes("modal-close", "is-large")
            attr("aria-label", "close")
            onClick { dismissHandle.dismiss() }
        })
    }
}

@Composable
fun ModalCard(
    isActive: Boolean,
    content: ContentBuilder<HTMLElement>
) {
    ModalBase(isActive) {
        Div(attrs = {
            classes("modal-card")
        }) {
            content()
        }
    }
}

@Composable
fun ModalCardHeader(title: String) {
    val dismissHandle = DismissHandle.current

    Header(attrs = {
        classes("modal-card-head")
    }) {
        P(attrs = {
            classes("modal-card-title")
        }) {
            Text(title)
        }
        Button(attrs = {
            classes("delete")
            attr("aria-label", "close")
            onClick { dismissHandle.dismiss() }
        })
    }
}

@Composable
fun ModalCardBody(content: ContentBuilder<HTMLElement>) {
    Section(attrs = {
        classes("modal-card-body")
    }, content = content)
}

@Composable
fun ModalCardFooter(content: ContentBuilder<HTMLElement>) {
    Footer(attrs = {
        classes("modal-card-foot")
    }, content = content)
}