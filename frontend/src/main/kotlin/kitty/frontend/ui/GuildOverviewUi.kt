package kitty.frontend.ui

import androidx.compose.runtime.*
import kitty.frontend.*
import kitty.frontend.l10n.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import org.jetbrains.compose.web.dom.*
import org.kodein.di.compose.*

@Composable
fun GuildOverviewUi(context: DarkKittyRoot.Child.GuildOverview) {
    val navigation: KittyNavigation by rememberInstance()
    val entrypoints: Set<KittyEntrypoint> by rememberInstance()

    Section(attrs = { classes("section", "is-main-section") }) {
        Subtitle(l10n("navigation.guild_overview.available_modules"))

        val validEntrypoints = LinkedHashMap<KittyEntrypoint, KittyRootComponent.Config>()
        entrypoints.filterIsInstance<KittyEntrypoint.Guild>().forEach { entrypoint ->
            if (!entrypoint.predicate()) return@forEach
            validEntrypoints[entrypoint] = entrypoint.config(context.guildId)
        }

        Grid(validEntrypoints.entries, GridSize.Big) { (entrypoint, config) ->
            SwitchLocalizationContext(namespace = entrypoint.l10nNamespace) {
                Tile(isChild = true, attrs = { classes("box") }) {
                    H1(attrs = { classes("title") }) { Text(l10n("module.title")) }
                    H2(attrs = { classes("subtitle") }) { Text(l10n("module.subtitle")) }
                    Div(attrs = {
                        classes("is-flex", "is-flex-direction-row", "is-justify-content-end")
                    }) {
                        Button(attrs = {
                            classes("button")
                            onClick {
                                navigation.navigateTo(config)
                            }
                        }) { MDIIcon("arrow-right") }
                    }
                }
            }
        }
    }
}
