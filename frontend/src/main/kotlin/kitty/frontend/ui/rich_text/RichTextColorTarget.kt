package kitty.frontend.ui.rich_text

enum class RichTextColorTarget {
    Foreground,
    Background,
    Underline,
    ;
}
