package kitty.frontend.ui.rich_text

import androidx.compose.runtime.*
import kitty.frontend.l10n.*

enum class RichTextColorPalette(val cssClassName: String?, private val displayNameKey: String) {
    BrowserDefault(null, "ansi.palette.default"),
    VGA("vga-palette", "ansi.palette.vga"),
    WindowsXP("windows-xp-palette", "ansi.palette.windows_xp"),
    WindowsPowerShell("windows-powershell-palette", "ansi.palette.windows_powershell"),
    VSCode("vscode-palette", "ansi.palette.vscode"),
    Windows10("windows-10-palette", "ansi.palette.windows_10"),
    MacOSTerminal("macos-terminal-palette", "ansi.palette.macos_terminal"),
    PuTTY("putty-palette", "ansi.palette.putty"),
    MIRC("mirc-palette", "ansi.palette.mirc"),
    XTerm("xterm-palette", "ansi.palette.xterm"),
    Ubuntu("ubuntu-palette", "ansi.palette.ubuntu"),
    Eclipse("eclipse-palette", "ansi.palette.eclipse"),
    ;

    val displayName: String
        @Composable
        @ReadOnlyComposable
        get() = l10n.forContext(namespace = "module_pasta")[displayNameKey]
}
