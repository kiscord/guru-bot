package kitty.frontend.ui

import androidx.compose.runtime.*
import kitty.frontend.*
import kitty.frontend.ui.blocks.*
import kitty.logic.*
import io.ktor.client.plugins.auth.providers.*
import org.kodein.di.compose.*

@Composable
fun LoginUi(context: DarkKittyRoot.Child.Login) {
    val navigation: KittyNavigation by rememberInstance()
    val tokensStorage: SerializedStorage<BearerTokens> by rememberInstance(tag = "tokens")
    val loginRedirectUrl: SerializedStorage<DeepLink> by rememberInstance(tag = "login_redirect_url")

    Loader(true)

    LaunchedEffect(context) {
        val accessToken = context.accessToken
        val refreshToken = context.refreshToken
        if (accessToken != null && refreshToken != null) {
            tokensStorage.set(BearerTokens(accessToken = accessToken, refreshToken = refreshToken))
        }
        val redirectUrl = loginRedirectUrl.value
        loginRedirectUrl.clear()
        navigation.navigateTo(redirectUrl ?: DeepLink.None, replace = true)
    }
}