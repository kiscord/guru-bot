package kitty.frontend.js

import kotlin.js.RegExp

external interface Context : JsFunction1<String, dynamic> {
    fun resolve(module: String): String
    fun keys(): Array<String>
    val id: Int
}

external object require {
    fun resolve(module: String): String

    fun ensure(
        dependencies: Array<String>,
        callback: (JsFunction<String, dynamic>) -> Unit = definedExternally,
        errorCallback: (Error) -> Unit = definedExternally,
        chunkName: String = definedExternally,
    )

    fun context(
        directory: String,
        useSubdirectories: Boolean,
        regExp: RegExp,
        mode: String? = definedExternally
    ): Context
}

external fun require(module: String): dynamic