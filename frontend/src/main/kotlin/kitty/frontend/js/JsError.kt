package kitty.frontend.js

@JsName("Error")
open external class JsError(message: String = definedExternally, options: JsErrorOptions = definedExternally) : Throwable {
    var name: String
        get() = definedExternally
        set(value) = definedExternally
}

external interface JsErrorOptions {
    val cause: Any?
        get() = definedExternally
}