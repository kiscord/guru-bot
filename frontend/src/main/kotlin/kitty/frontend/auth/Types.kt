@file:JsModule("jose")

package kitty.frontend.auth

import kotlin.js.Date

external interface FlattenedJWSInput {
    var header: JWSHeaderParameters?
        get() = definedExternally
        set(value) = definedExternally
    var payload: dynamic /* String | Uint8Array */
        get() = definedExternally
        set(value) = definedExternally
    var protected: String?
        get() = definedExternally
        set(value) = definedExternally
    var signature: String
}


external interface JoseHeaderParameters {
    var kid: String?
        get() = definedExternally
        set(value) = definedExternally
    var x5t: String?
        get() = definedExternally
        set(value) = definedExternally
    var x5c: Array<String>?
        get() = definedExternally
        set(value) = definedExternally
    var x5u: String?
        get() = definedExternally
        set(value) = definedExternally
    var jku: String?
        get() = definedExternally
        set(value) = definedExternally
    var jwk: dynamic /* { "kty" | "crv" | "x" | "y" | "e" | "n": string? } */
        get() = definedExternally
        set(value) = definedExternally
    var typ: String?
        get() = definedExternally
        set(value) = definedExternally
    var cty: String?
        get() = definedExternally
        set(value) = definedExternally
}

external interface JSONWebKeySet {
    var keys: Array<JWK>
}

external interface JWK {
    var alg: String?
        get() = definedExternally
        set(value) = definedExternally
    var crv: String?
        get() = definedExternally
        set(value) = definedExternally
    var d: String?
        get() = definedExternally
        set(value) = definedExternally
    var dp: String?
        get() = definedExternally
        set(value) = definedExternally
    var dq: String?
        get() = definedExternally
        set(value) = definedExternally
    var e: String?
        get() = definedExternally
        set(value) = definedExternally
    var ext: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var k: String?
        get() = definedExternally
        set(value) = definedExternally
    var key_ops: Array<String>?
        get() = definedExternally
        set(value) = definedExternally
    var kid: String?
        get() = definedExternally
        set(value) = definedExternally
    var kty: String?
        get() = definedExternally
        set(value) = definedExternally
    var n: String?
        get() = definedExternally
        set(value) = definedExternally
    var p: String?
        get() = definedExternally
        set(value) = definedExternally
    var q: String?
        get() = definedExternally
        set(value) = definedExternally
    var qi: String?
        get() = definedExternally
        set(value) = definedExternally
    var use: String?
        get() = definedExternally
        set(value) = definedExternally
    var x: String?
        get() = definedExternally
        set(value) = definedExternally
    var y: String?
        get() = definedExternally
        set(value) = definedExternally
}

external interface JWSHeaderParameters : JoseHeaderParameters {
    var alg: String?
        get() = definedExternally
        set(value) = definedExternally
    var b64: Boolean?
        get() = definedExternally
        set(value) = definedExternally
    var crit: Array<String>?
        get() = definedExternally
        set(value) = definedExternally
}

external interface KeyLike {
    var type: String
}

external interface VerifyOptions : CritOption {
    var algorithms: Array<String>?
        get() = definedExternally
        set(value) = definedExternally
}

external interface CritOption {
    var crit: Crit?
        get() = definedExternally
        set(value) = definedExternally
}

external interface Crit

external interface JWTClaimVerificationOptions {
    var audience: dynamic /* String? | Array<String>? */
        get() = definedExternally
        set(value) = definedExternally
    var clockTolerance: dynamic /* String? | Number? */
        get() = definedExternally
        set(value) = definedExternally
    var issuer: dynamic /* String? | Array<String>? */
        get() = definedExternally
        set(value) = definedExternally
    var maxTokenAge: dynamic /* String? | Number? */
        get() = definedExternally
        set(value) = definedExternally
    var subject: String?
        get() = definedExternally
        set(value) = definedExternally
    var typ: String?
        get() = definedExternally
        set(value) = definedExternally
    var currentDate: Date?
        get() = definedExternally
        set(value) = definedExternally
}

external interface CompactJWSHeaderParameters : JWSHeaderParameters

external interface JWTHeaderParameters : CompactJWSHeaderParameters {
    override var b64: Boolean?
        get() = definedExternally
        set(value) = definedExternally
}

external interface JWTVerifyResult {
    var payload: JWTPayload
    var protectedHeader: JWTHeaderParameters
}

external interface JWTPayload {
    var iss: String?
        get() = definedExternally
        set(value) = definedExternally
    var sub: String?
        get() = definedExternally
        set(value) = definedExternally
    var aud: dynamic /* String? | Array<String>? */
        get() = definedExternally
        set(value) = definedExternally
    var jti: String?
        get() = definedExternally
        set(value) = definedExternally
    var nbf: Number?
        get() = definedExternally
        set(value) = definedExternally
    var exp: Number?
        get() = definedExternally
        set(value) = definedExternally
    var iat: Number?
        get() = definedExternally
        set(value) = definedExternally
}

external interface JWTVerifyOptions : VerifyOptions, JWTClaimVerificationOptions

external interface RemoteJWKSetOptions {
    var timeoutDuration: Number?
        get() = definedExternally
        set(value) = definedExternally
    var cooldownDuration: Number?
        get() = definedExternally
        set(value) = definedExternally
    var cacheMaxAge: dynamic /* Number? | Any? */
        get() = definedExternally
        set(value) = definedExternally
    var agent: Any?
        get() = definedExternally
        set(value) = definedExternally
    var headers: dynamic
        get() = definedExternally
        set(value) = definedExternally
}

external interface ResolvedKey {
    val key: KeyLike
}

external interface JWTVerifyResultWithResolvedKey : JWTVerifyResult, ResolvedKey