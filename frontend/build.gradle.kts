import org.jetbrains.kotlin.gradle.targets.js.webpack.*

plugins {
    kotlin("js")
    kotlin("plugin.serialization")
    alias(libs.plugins.compose)
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

kotlin {
    js {
        useCommonJs()
        browser()
        binaries.executable()
    }
}

repositories {
    google()
}

dependencies {
    implementation(project(":shared"))
    implementation(project(":logic"))
    implementation(project(":markdown"))

    implementation(compose.web.core)
    implementation(compose.web.svg)
    implementation(libs.bundles.kodein.frontend)

    implementation(npm("@creativebulma/bulma-tooltip", "^1.2.0"))
    implementation(npm("@deckar01/bulma-pageloader", "^2.2.1"))
    implementation(npm("@mdi/font", "^7.1.96"))
    implementation(npm("bulma", "^0.9.4"))
    implementation(npm("bulma-responsive-tables", "^1.2.5"))
    implementation(npm("bulma-switch-control", "^1.2.2"))
    implementation(npm("i18next", "^22.4.9"))
    implementation(npm("i18next-browser-languagedetector", "^7.0.1"))
    implementation(npm("i18next-chained-backend", "^4.2.0"))
    implementation(npm("i18next-http-backend", "^2.1.1"))
    implementation(npm("i18next-localstorage-backend", "^4.1.0"))
    implementation(npm("jose", "^4.11.2"))

    implementation(devNpm("clean-webpack-plugin", "^4.0.0"))
    implementation(devNpm("compression-webpack-plugin", "^10.0.0"))
    implementation(devNpm("css-loader", "^6.7.1"))
    implementation(devNpm("css-minimizer-webpack-plugin", "^4.0.0"))
    implementation(devNpm("html-webpack-plugin", "^5.5.0"))
    implementation(devNpm("json-minimizer-webpack-plugin", "^4.0.0"))
    implementation(devNpm("mini-css-extract-plugin", "^2.6.1"))
    implementation(devNpm("sass", "^1.54.5"))
    implementation(devNpm("sass-loader", "^13.0.2"))
    implementation(devNpm("style-loader", "^3.3.1"))
    implementation(devNpm("terser-webpack-plugin", "^5.3.5"))
    implementation(devNpm("webpack-bundle-analyzer", "^4.6.1"))
    implementation(devNpm("workbox-webpack-plugin", "^6.5.4"))
}

tasks.withType<KotlinWebpack>().configureEach {
    inputs.dir(file("src/main/web"))
    outputs.dir(destinationDirectory)

    args += listOf("--stats", "verbose")

    when (mode) {
        KotlinWebpackConfig.Mode.PRODUCTION -> {
            report = true
        }

        KotlinWebpackConfig.Mode.DEVELOPMENT -> {
            val backendUrl = "http://localhost:${ext["kitty.backend.port"]}"
            devServer?.apply {
                proxy = mutableMapOf(
                    "/api" to backendUrl,
                    "/.well-known" to backendUrl,
                    "/favicon.png" to backendUrl,
                )
                port = ext["kitty.frontend.port"].toString().toInt()
            }
        }
    }
}

tasks.named("browserDevelopmentRun").configure {
    dependsOn("developmentExecutableCompileSync")
}

tasks.named("browserProductionRun").configure {
    dependsOn("productionExecutableCompileSync")
}
