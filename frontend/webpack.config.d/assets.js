(function (config) {
    config.output.assetModuleFilename = "assets/[contenthash][ext][query]";

    config.module.rules.push({
        test: /\.(png|jpg|gif|ico|svg)$/i,
        type: 'asset/resource',
        generator: {
            filename: "assets/images/[contenthash][ext][query]",
        },
    });

    config.module.rules.push({
        test: /\.(woff2|woff|ttf|eot)$/i,
        type: 'asset/resource',
        generator: {
            filename: "assets/fonts/[contenthash][ext][query]",
        },
    });

    config.module.rules.push({
        test: /\/locales\/.+_.+\.json$/i,
        type: 'asset/resource',
        generator: {
            filename: "assets/locales/[contenthash][ext][query]",
        },
    });

    config.module.rules.push({
        test: /favicon\.ico$/i,
        type: 'asset/resource',
        generator: {
            filename: "[name][ext][query]",
        },
    });

    config.module.rules.push({
        test: /\.webmanifest$/i,
        type: 'asset/resource',
        generator: {
            filename: "[name][ext][query]",
        },
    });
})(config);
