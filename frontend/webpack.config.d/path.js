(function (config) {
    const path = require('path');

    config.resolve.modules.push(path.resolve('../../node_modules'));
    config.resolve.alias = {
        ...config.resolve.alias,
        '@': path.resolve('../../../../frontend/src/main/web'),
        'node_modules': path.resolve('../../../../build/js/node_modules'),
    };
})(config);
