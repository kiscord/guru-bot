(function (config) {
    const MiniCssExtractPlugin = require("mini-css-extract-plugin");
    const devMode = config.mode !== "production";

    if (!devMode) {
        config.plugins.push(new MiniCssExtractPlugin({
            filename: "css/[contenthash].css",
            chunkFilename: "css/[id].css",
        }));
    }

    config.module.rules.push({
        test: /\.css$/,
        use: [devMode ? 'style-loader' : MiniCssExtractPlugin.loader, 'css-loader']
    });

    config.module.rules.push({
        test: /\.s[ac]ss$/,
        use: [devMode ? 'style-loader' : MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
    });
})(config);