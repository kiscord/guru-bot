(function (config) {
    const WorkboxPlugin = require("workbox-webpack-plugin");
    const devMode = config.mode !== "production";

    if (!devMode) config.plugins.push(new WorkboxPlugin.GenerateSW({
        clientsClaim: true,
        skipWaiting: true,
        maximumFileSizeToCacheInBytes: 10485760,
        runtimeCaching: [{
            urlPattern: ({request, url}) => {
                return url.pathname === "/.well-known/jwks.json"
            },
            handler: 'NetworkFirst',
            options: {
                cacheName: 'jwks',
            },
        }],
    }));
})(config);
