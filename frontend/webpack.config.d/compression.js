(function (config) {
    const CompressionPlugin = require("compression-webpack-plugin");
    const zlib = require("zlib");
    const devMode = config.mode !== "production";

    if (!devMode) {
        const commonOptions = {
            test: /\.(js|css|html|json)$/,
            minRatio: 0.9,
        };

        config.plugins.push(new CompressionPlugin({
            ...commonOptions,
            filename: "[path][base].gz",
            algorithm: "gzip",
            compressionOptions: {
                params: {
                    level: zlib.constants.Z_BEST_COMPRESSION,
                },
            },
        }));

        config.plugins.push(new CompressionPlugin({
            ...commonOptions,
            filename: "[path][base].br",
            algorithm: "brotliCompress",
            compressionOptions: {
                params: {
                    [zlib.constants.BROTLI_PARAM_QUALITY]: zlib.constants.BROTLI_MAX_QUALITY,
                },
            },
        }));
    }
})(config);