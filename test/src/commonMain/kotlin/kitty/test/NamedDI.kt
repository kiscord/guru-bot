package kitty.test

import kitty.*
import org.kodein.di.*

private class NamedDI(private val name: String, val originalDi: DI) : DI by originalDi, AutoCloseable {
    override fun close() {
        if (originalDi is AutoCloseableDI) {
            originalDi.close()
        }
    }

    override fun toString(): String = name
}

infix fun DI.named(name: String): DI = when (this) {
    is NamedDI -> NamedDI(name, originalDi)
    else -> NamedDI(name, this)
}

