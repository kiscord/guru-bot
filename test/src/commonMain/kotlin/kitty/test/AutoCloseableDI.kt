package kitty.test

import kitty.*
import org.kodein.di.*

class AutoCloseableDI(di: DI) : DI by di, AutoCloseable {
    override fun close() {
        (this as DI).close()
    }

    companion object {
        operator fun invoke(
            allowSilentOverride: Boolean = false,
            init: DI.MainBuilder.() -> Unit
        ): AutoCloseableDI = AutoCloseableDI(DI.invoke(allowSilentOverride, init))
    }
}
