pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}

plugins {
    id("com.mooltiverse.oss.nyx") version "2.3.0"
}

rootProject.name = "kitty"

include(":backend")
include(":frontend")
include(":logic")
include(":markdown")
include(":shared")
include(":test")

dependencyResolutionManagement {
    repositories {
        mavenCentral()
        maven("https://gitlab.com/api/v4/projects/12953147/packages/maven") {
            name = "kiscord-gitlab"
            content {
                includeGroup("rocks.aur.kiscord")
            }
        }
    }

    versionCatalogs {
        create("kiscord") {
            from("rocks.aur.kiscord:kiscord:0.7.0-develop.1")
        }
    }
}
