val platforms: Configuration by configurations.creating {
    isCanBeResolved = false
    isCanBeConsumed = false
}

configurations.all {
    if (isCanBeResolved && !name.endsWith("DependenciesMetadata")) {
        extendsFrom(platforms)
    }
}

dependencies {
    platforms(platform(kiscord.kiscord.platform))
    platforms(platform(kiscord.kotlin.bom))
}
